//
//  FragmentModifyCodes.java
//  Caran Demo App
//
//  Copyright © 2017 Datacolor. All rights reserved.
//

package com.datacolor.caran;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class FragmentModifyCodes extends FragmentBase {
    interface FragmentModifyCodesDelegate {
        String fragmentModifyCodesOemCode(FragmentModifyCodes sender);
        String fragmentModifyCodesRegistrationCode(FragmentModifyCodes sender);
        void fragmentModifyCodesDidRequestDismiss(FragmentModifyCodes sender);
        void fragmentModifyCodesDidRequestSaveCodes(FragmentModifyCodes sender, String oemCode, String regCode);
    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_modify_codes;
    }


    @Override
    protected void init() {
        this.setTitle("Codes");
        this.enableCloseButton();

        ((Button) getView().findViewById(R.id.button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String oemCode = ((EditText) getView().findViewById(R.id.edit_oem_code)).getText().toString();
                String regCode = ((EditText) getView().findViewById(R.id.edit_reg_code)).getText().toString();
                getMainActivity().fragmentModifyCodesDidRequestSaveCodes((FragmentModifyCodes) self, oemCode, regCode);
            }
        });

        this.reload();
    }


    @Override
    public void reload() {
        ((EditText) getView().findViewById(R.id.edit_oem_code)).setText(getMainActivity().fragmentModifyCodesOemCode(this));
        ((EditText) getView().findViewById(R.id.edit_reg_code)).setText(getMainActivity().fragmentModifyCodesRegistrationCode(this));
    }


    @Override
    protected void dismiss() {
        getMainActivity().fragmentModifyCodesDidRequestDismiss(this);
    }
}