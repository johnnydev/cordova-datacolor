//
//  OctagonView.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 06/10/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import UIKit

class DecagonView: UIView {
    
    override func draw(_ rect: CGRect) {
        
        // create the polygon mask
        let polylayer = Polygons.instance.drawPolygonLayer(x: rect.midX, y: rect.midY, radius: rect.width/2, sides: 10, color: UIColor.blue, offset: 0)
        
        // apply the mask
        self.layer.mask = polylayer
    }
    
}
