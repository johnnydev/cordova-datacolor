//
//  MeasuringAnimated.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 13/10/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable

class MeasuringAnimated: UIActivityIndicatorView {

    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: PROPERTIES
    // MARK:
    //--------------------------------------------------------------------------------
    
    var contentView : UIView?
    var message: String = ""
    
    
    @IBOutlet weak var rotatingView: DecagonView!
    
    
    private func rotateView(){
    
        
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear, animations: { () -> Void in
            self.rotatingView.transform = self.rotatingView.transform.rotated(by: CGFloat(Double.pi / 2))
            }, completion:{
                (finished) -> Void in
                self.rotateView()
            }
        )

        
    }
    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: INIT
    // MARK:
    //--------------------------------------------------------------------------------
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        contentView = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        contentView!.frame = bounds
        
        // Make the view stretch with containing view
        contentView!.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView!)
    }
    
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        // initial setup
        rotateView()
        
        // return the view
        return view
    }
    
    


}
