//
//  MeasureViewController.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 29/09/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import UIKit
import MessageUI

class MeasurementsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate{
    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: PROPERTIES
    // MARK:
    //--------------------------------------------------------------------------------
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var btnExport: UIBarButtonItem!    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: OVERRIDES
    // MARK:
    //--------------------------------------------------------------------------------
    
    
    // Remove the observers
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        // Receive measure
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didReceiveMeasure),
            name: NSNotification.Name(rawValue: "MeasureFromDeviceNotification"),
            object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: ACTIONS
    // MARK:
    //--------------------------------------------------------------------------------
    
    @IBAction func CloseView(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func ClearList(_ sender: AnyObject) {
        
        let alert = UIAlertController(title: "Are you sure?", message:
            "This will delete all the measurements stored", preferredStyle: .alert)
        
        self.present(alert, animated: false, completion: nil)
        
        alert.addAction(UIAlertAction(title: "No",
                                      style: UIAlertActionStyle.default,
                                      handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler:
            {action in
                // Handler code
                CaranController.instance.clearMeasurementsList()
                CaranController.instance.lastMeasure = nil // clear also the latest measure
                self.tableView.reloadData()
        }))
        
    }
    
    @IBAction func ExportList(_ sender: AnyObject) {
        
        if(DCUtils.instance.exportCSV(aList: CaranController.instance.measurementsList)){        
            // Send by email
            self.sendFileByEmail()
        }
    }
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: EMAIL COMPOSER
    // MARK:
    //--------------------------------------------------------------------------------
    
    // Generating the email controller.
    func configuredMailComposeViewController() -> MFMailComposeViewController {
        let emailController = MFMailComposeViewController()
        emailController.mailComposeDelegate = self
        emailController.setSubject("Caran CSV File")
        emailController.setMessageBody("Measurements list export file", isHTML: false)
        
        // Attaching the .CSV file to the email.
        var fileData : Data?
        
        do{
            fileData = try Data(contentsOf: DCUtils.CSVExportfileDestinationUrl)
        }
        catch{
            print("failed to get data from file")
        }
        
        if(fileData != nil){
            emailController.addAttachmentData(fileData!, mimeType: "text/csv", fileName: "caran_export.csv")
        }
        
        
        return emailController
    }

    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        // Dismiss the mail composer view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: METHODS
    // MARK:
    //--------------------------------------------------------------------------------
    
    
    func sendFileByEmail(){
        let emailViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.present(emailViewController, animated: true, completion: nil)
        }
    }
    
    
    func didReceiveMeasure(notification: NSNotification) {
        
        print("MeasuresListViewController was notified of a measure")
        
        tableView.reloadData()
        
    }
    
    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: TABLE PROTOCOL
    // MARK:
    //--------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(CaranController.instance.measurementsList.count > 0){
            btnExport.isEnabled = true
        }
        else{
            btnExport.isEnabled = false
        }
        
        return CaranController.instance.measurementsList.count
    }
    
    @available(iOS 2.0, *)
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   
        let cell = tableView.dequeueReusableCell(withIdentifier: "measurementViewCell", for: indexPath) as! MeasurementViewCell
        
        let measure = CaranController.instance.measurementsList[indexPath.row]
        let measureUtils = LABMeasureUtils(_labMeasure: measure)        
        
        cell.MeasureLabel.text = measureUtils.lab_string
        cell.MeasureView.backgroundColor = measureUtils.uicolor
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
        // set as latestMeasure
        CaranController.instance.lastMeasure = CaranController.instance.measurementsList[indexPath.row]
        
        // dismiss
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    // Override to support editing the table view.
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            
            if( CaranController.instance.lastMeasure == CaranController.instance.measurementsList[indexPath.row]){
                CaranController.instance.lastMeasure = nil
            }
            
            CaranController.instance.deleteFromMeasurementsListAt(_index: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}
