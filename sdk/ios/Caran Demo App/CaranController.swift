//
//  CaranController.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 29/09/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import Foundation
import CoreBluetooth
import UIKit

// Debugging
let printDebugMessages = true


class CaranController : NSObject, CaranProtocol
{

    // Singleton Pattern
    // Caran Controller can be accessed from CaranController.sharedInstance
    class var instance: CaranController {
        struct Singleton {
            static let instance = CaranController()
        }
        return Singleton.instance
    }
    
    // Properties
    private var caran : DCICaranLib!            // Our Caran SDK property
    var devices: [CBPeripheral] = []            // Our list of found Caran device(s) property
    var connectedDevice: CBPeripheral?          // Connected Device
    var wasManuallyDisconnected:Bool = false    // Should be set to true to notify the app that the user manually disconnected the instrument
    var lastMeasure:LABMeasure?
    var calibrationWarning:Bool = false         // Will be refreshed after a "successful" calibration; and then used to update the text on the Calibration Success view.
    
    private let averageDownloadTimeForChunk = 1.744186
    
    
    private var measures = [LABMeasure]()
    
    
    public var needsCalibration : Bool {
        get { return self.caran.needsCalibration()}
    }
    
    public var measurementsList : [LABMeasure] {    
        get{ return measures }
    }
   
    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: INITIALIZATION
    // MARK:
    //--------------------------------------------------------------------------------
    
    public func initialize()
    {
        caran = DCICaranLib.init(delegate: self) // Initialize the SDK
        
        let loadedMeasurements = loadMeasurements() // load the stored measurements
        if(loadedMeasurements != nil) {
            measures = loadedMeasurements!
        }
        
        // set the MeasurementMode on the SDK based on a local setting
        let measurementModeRawValue = DCUtils.instance.loadLocalSetting(_withKey: "MeasurementMode")
        if(measurementModeRawValue != nil){
            let measurementMode = CrnMeasurementMode(rawValue: measurementModeRawValue as! UInt32)
            setMeasurementModeAs(_mode: measurementMode)
        }
        
        
    }
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: PUBLIC INTERFACE
    // MARK:
    //--------------------------------------------------------------------------------
    
    public func searchForDevices()
    {
        caran.getListOfVisibleDevices()
    }
    
    public func connectToDevice(device:CBPeripheral)
    {
        caran.connect(toDevice: device)
    }

    public func calibrateDevice()
    {
        caran.calibrate()
    }

    public func takeMeasurement()
    {
        caran.takeMeasurement()
    }

    public func disconnectFromDevice()
    {
        self.wasManuallyDisconnected = true
        if(caran != nil){
            caran.disconnectFromDevice()
        }
        
    }
    
    public func clearMeasurementsList(){
        measures.removeAll()
        saveMeasurements()
    }
    
    public func deleteFromMeasurementsListAt(_index : Int){
        measures.remove(at: _index)
    }
    
    public func getDeviceSerialFromName(aName : String) -> String{
        
        var name = aName
        let range = aName.range(of: "DC_CARAN_", options: .caseInsensitive, range: nil, locale: nil)
        
        if (range != nil){
            name.removeSubrange(range!)
            return name.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        else{
            return ""
        }
    }
    
    public func getDeviceSerial() -> String{
        
        var name = ""
        
        if(self.connectedDevice != nil){
            name = getDeviceSerialFromName(aName: self.connectedDevice!.name!)
        }
        
        return name
    }
    
    public func getFirwareVersion() -> String {
        
        var fwVersion = ""
        
        if(connectedDevice != nil){
            
            fwVersion = String(self.caran.firmwareRev())
            
        }
        
        return fwVersion
    }
    
    // SDK
    public func getSdkVersion() -> String {    
        var sdkversion = ""
        if(caran != nil){
            sdkversion = String(caran.sdkRev())
        }
        return sdkversion
    }
    
    // MODE
    public func isModeLabPlusMatch() -> Bool {
        var measureMode = false
        if(caran != nil){
            if(caran.getMeasurementMode() == modeMeasureLabPlusMatch){
                measureMode = true
            }
        }
        return measureMode
    }
    
    public func setMeasurementModeAs(_mode:CrnMeasurementMode){
    
        if(caran != nil){
            caran.setMeasurementModeAs(_mode)
            DCUtils.instance.saveLocalSetting(_settingValue: _mode.rawValue, _withKey: "MeasurementMode")
        }
    }
    
    // Fandeck Info
    
    public func getFandeckNames()->[String] {
        
        var fandeckNames = [String]()
                
        if(caran != nil){
            let sdkFandeckNames = caran.fandeckNames()
            if(sdkFandeckNames != nil){
                for element in sdkFandeckNames! {
                    fandeckNames.append(String(element as! NSMutableString))
                }
            }            
        }
        return fandeckNames;
    }
    
    
    public func getFandeckCreationDate()->String {
        
        var creationDate = "Not Available"
        if(caran != nil){
            creationDate = String(caran.fandeckCreationDate())
        }
        return creationDate;
    }
    
    public func getFandeckIlluminant()->String {
        
        var illuminant = "Not Available"
        if(caran != nil){
            illuminant = String(caran.fandeckIlluminant())
        }
        return illuminant;
    }
    
    public func getFandeckCount()->Int {
        
        var fandeckCount = 0
        if(caran != nil){
            fandeckCount = Int(caran.fandeckCount())
        }
        return fandeckCount;
    }
    
    public func getFandeckRecords()->Int {
        
        var fandeckRecords = 0
        if(caran != nil){
            fandeckRecords = Int(caran.fandeckRecords())
        }
        return fandeckRecords;
    }
    
    
    public func uploadFandeckCsv(array:[String]){
        if(caran != nil){
            caran.downloadFandeck(withCSVs: array)
        }
    }
    
    public func maxNumFandecks()->Int{
       return Int(self.caran.maxNumFandecks())
    }

    public func maxNumColors()->Int{
       return Int(self.caran.maxNumColors())
    }

    public func getDownloadTimeForChunk()->Double{
    
        let time = DCUtils.instance.loadLocalSetting(_withKey: "DownloadTimeForChunk")
        
        if(time == nil){
            return averageDownloadTimeForChunk
        }else{
            return time as! Double
        }
        
    }
    
    public func setDownloadTimeForChunk(time:Double){
        DCUtils.instance.saveLocalSetting(_settingValue: time, _withKey: "DownloadTimeForChunk")
    }
    
    
    public func getChunksFormNumberOfColors(colors:Int)->Int{

        var totalBytes:UInt32 = 0
        var totalChunks:UInt32 = 0
    
        caran.getFandeckDownloadStats(fromTotalColors: UInt32(colors), asTotalBytes: &totalBytes, andTotalChunks: &totalChunks)
        
        return Int(totalChunks)
    }
    
    
    
    // Registration
    
    public func getCodes(){
        if(caran != nil){
            caran.getCodes()
        }
    }
    
    public func writeCodes(_oemCode:String,_registrationCode:String){
        if(caran != nil){
            caran.setCodesWithOEMCode(_oemCode, andRegistrationCode: _registrationCode)
        }
    }
    
    
    
    
    
    // BT status
    public func isBlueToothPoweredOn()->Bool{
    
        if(caran != nil){
            return caran.isBluetoothPoweredOn()
        }else{
            return false
        }
    
    }
    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: PRIVATE METHODS
    // MARK:
    //--------------------------------------------------------------------------------
    
    private func storeMeasure(_labMeasure: LABMeasure){
        measures.insert(_labMeasure, at: 0)
        saveMeasurements()
    }
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: NSCODING
    // MARK:
    //--------------------------------------------------------------------------------
    
    func saveMeasurements() {
        let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(measures, toFile: LABMeasure.ArchiveURL.path)
        
        if !isSuccessfulSave {
            print("Failed to save measurements...")
        }
    }
    
    func loadMeasurements() -> [LABMeasure]? {
        
        return NSKeyedUnarchiver.unarchiveObject(withFile: LABMeasure.ArchiveURL.path) as? [LABMeasure]
    }
    
    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: CARAN PROTOCOL
    // MARK:
    //--------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------
    //
    // didGetButtonPress
    //
    // Called when the button is pushed on the device
    //
    //--------------------------------------------------------------------------------
    
    
    public func didGetButtonPress() {
        if printDebugMessages { print("APP: didGetButtonPress") }
        
        // Send a notification
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ButtonPushedNotification"), object: self, userInfo: nil)
        
    }
    
    
    //--------------------------------------------------------------------------------
    //
    // didReceiveListOfVisibileDevices
    //
    // Called after scanning for devices, when a list has been returned.
    // If no Caran(s) are found, the list will be empty).
    //
    //--------------------------------------------------------------------------------
    
    public func didReceiveList(ofVisibileDevices peripheralList: NSMutableArray!) {
    
        
        if printDebugMessages {
            print("APP: didReceiveListOfVisibileDevices")
        }
        
        // Clear our device list.
        devices = []
        
        
        // Populate our device list with what came back from the scan.
        for element in peripheralList {
            devices.append(element as! CBPeripheral)
        }
        
        
        // Send a notification
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CaranListOfVisibleDevicesNotification"), object: self, userInfo: ["devices" : devices])
    }
    
    
    //--------------------------------------------------------------------------------
    //
    // didConnectToDevice
    //
    // Called after successful connection to a Caran.
    //
    //--------------------------------------------------------------------------------
    
    @available(iOS 5.0, *)
    public func didConnect(toDevice _device: CBPeripheral!) {
        
        if printDebugMessages {
            print("APP: didConnectToDevice")
        }
        
        connectedDevice = _device

        // Send a notification        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DeviceConnectedNotification"), object: self, userInfo: ["device" : connectedDevice as Any])
        
        
        
        // lblDeviceName.text = _device.name
        // lblDeviceCode.text = _device.identifier.UUIDString
        
    }
    
    
    //--------------------------------------------------------------------------------
    //
    // didFailToConnectToDevice
    //
    // Called after attempted, but failed, connection to a Caran.
    //
    //--------------------------------------------------------------------------------
    @available(iOS 5.0, *)
    public func didFailToConnect(toDevice _device: CBPeripheral!) {
        
        if printDebugMessages {
            print("APP: didFailToConnectToDevice")
        }
        
        connectedDevice = nil
        
        // Send a notification
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FailToConnectNotification"), object: self)
        
    }
    
    
    //--------------------------------------------------------------------------------
    //
    // didDisconnectFromDevice
    //
    // Called after successful disconnection.
    //
    //--------------------------------------------------------------------------------
    @available(iOS 5.0, *)
    func didDisconnectFromDevice() {
        
        if printDebugMessages {
            print("APP: didDisconnectFromDevice")
        }
        connectedDevice = nil
        
        // Send a notification
        print("Send: MeasureNotification")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DisconnectFromDeviceNotification"), object: self)
        
        
    }
    
    
    //--------------------------------------------------------------------------------
    //
    // didReceiveMeasurement
    //
    // Called after successful measurement.
    //
    // Should perform whatever additional operations are needed in the app after
    // getting the measurement data.
    //
    //--------------------------------------------------------------------------------
    
    public func didReceiveMeasurement(withL l: NSNumber!,
                                      andA a: NSNumber!,
                                      andB b: NSNumber!,
                                      andMatch1 m1: FandeckMatch, // Added these to the protocol.
                                      andMatch2 m2: FandeckMatch,
                                      andMatch3 m3: FandeckMatch) {
        
        if printDebugMessages {
            print("APP: didReceiveMeasurement Lab + Match")
        }
        
        var fandeckMatchArr = [MeasureMatch]()
                
        let name1 = MeasureMatch.cnameToString(cObj: m1.name)
        let name2 = MeasureMatch.cnameToString(cObj: m2.name)
        let name3 = MeasureMatch.cnameToString(cObj: m3.name)
        
        let fandeck1 = MeasureMatch.cnameToString(cObj: m1.fdname)
        let fandeck2 = MeasureMatch.cnameToString(cObj: m2.fdname)
        let fandeck3 = MeasureMatch.cnameToString(cObj: m3.fdname)
        
        let match_1 = MeasureMatch(_name: name1, _l: m1.l, _a: m1.a, _b: m1.b, _dE: m1.deltaE, _fandeck:fandeck1)
        let match_2 = MeasureMatch(_name: name2, _l: m2.l, _a: m2.a, _b: m2.b, _dE: m2.deltaE, _fandeck:fandeck2)
        let match_3 = MeasureMatch(_name: name3, _l: m3.l, _a: m3.a, _b: m3.b, _dE: m3.deltaE, _fandeck:fandeck3)

        fandeckMatchArr.append(match_1)
        fandeckMatchArr.append(match_2)
        fandeckMatchArr.append(match_3)
        
        lastMeasure = LABMeasure(_l: Float(l),_a: Float(a),_b: Float(b),_matchArr: fandeckMatchArr)

        // store the measurement in the list
        self.storeMeasure(_labMeasure: lastMeasure!)
        
        print("number of measurements: " + measures.count.description)
        
        
        // Send the notification
        print("Send: MeasureNotification")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MeasureFromDeviceNotification"), object: self, userInfo: [
            "LABMeasure" : lastMeasure as Any
            ])
       
    }
    
    
    public func didReceiveMeasurement(withL l: NSNumber!,
                                      andA a: NSNumber!,
                                      andB b: NSNumber!) {
    
    
        if printDebugMessages {
            print("APP: didReceiveMeasurement Lab only")
        }
        
        
        let fandeckMatchArr = [MeasureMatch]()
        
        lastMeasure = LABMeasure(_l: Float(l),_a: Float(a),_b: Float(b),_matchArr: fandeckMatchArr)
        
        // store the measurement in the list
        self.storeMeasure(_labMeasure: lastMeasure!)
        
        print("number of measurements: " + measures.count.description)
        
        
        // Send the notification
        print("Send: MeasureNotification")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MeasureFromDeviceNotification"), object: self, userInfo: [
            "LABMeasure" : lastMeasure as Any
            ])
        
        
    }

    
    //--------------------------------------------------------------------------------
    //
    // didBecomeUncalibrated
    //
    // Called ONCE at most, during a connection session, if the SDK says that Caran needs
    // calibration. This may be called after a measurement is taken, and also
    // after connection, so it's useful in case Caran goes from a calibrated
    // to an uncalibrated state during the connection session.
    //
    // Note: host app can call needsCalibration at any time while connected to check
    // the calibration status in addition to observing this warning.
    //
    //--------------------------------------------------------------------------------
    
    func didBecomeUncalibrated() {
        
        if printDebugMessages {
            print("APP: didBecomeUncalibrated")
        }

        print("Send: UncalibratedNotification")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DeviceUncalibratedNotification"), object: self)
        
    }
   
    
    delegate
    //--------------------------------------------------------------------------------
    //
    // didPassCalibrationWithWarning:(BOOL)warning
    //
    // Called when the SDK reports that a calibration attempt has completed successfully.
    // If warning = YES, the user should be warned that they may not be calibrating on the tile.
    //
    //--------------------------------------------------------------------------------
    
    func didPassCalibration(withWarning warning: Bool) {
        
        if printDebugMessages {
            print("APP: didPassCalibration")
        }
        
        // Set our flag, to later modify the text on the "success" screen, depending on whether there's a warning, or not.
        calibrationWarning = warning;

        // Should transition into the "success" screen; Should also clear the badge.
        print("Send: CalibratedNotification")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DeviceCalibratedNotification"), object: self)
           
    }
    
    
    
    //--------------------------------------------------------------------------------
    //
    // didFailCalibration
    //
    // Called when the SDK reports that a calibration attempt has failed.
    //
    //--------------------------------------------------------------------------------
    
    func didFailCalibration() {
        
        if printDebugMessages {
            print("APP: didFailCalibration.")
        }
        // 5.18.17: Clear our warning flag. (We have a full failure, we're beyond "warning")
        calibrationWarning = false;
      
        print("Send: Calibration failed!")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DeviceCalibrationFailedNotification"), object: self)
        
    }
    
    
    
    //--------------------------------------------------------------------------------
    //
    // didStartFandeckDownloadForTotalChunks
    //
    //
    //
    //--------------------------------------------------------------------------------
    
    func didStartFandeckDownload(forTotalChunks totalChunks: UInt32) {
        
        if printDebugMessages {
            print("APP: didStartFandeckDownload")
        }
        
        // Send a notification
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DidStartFandeckDownloadNotification"), object: self, userInfo: ["totalChunks" : Int(totalChunks) as Int])
        
        
    }

    
    //--------------------------------------------------------------------------------
    //
    // didFandeckDownloadForChunk
    //
    //
    //
    //--------------------------------------------------------------------------------

    public func didFandeckDownload(forChunk thisChunk: UInt32, ofTotalChunks totalChunks: UInt32, withColorsTransferred numColors: UInt32, ofTotalColors totalColors: UInt32) {
        
        if printDebugMessages {
            print("APP: didFandeckDownload")
        }
        
        // Send a notification
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DidFandeckDownloadNotification"), object: self, userInfo: [
            "totalChunks" : Int(totalChunks) as Int,
            "thisChunk" : Int(thisChunk) as Int,
            "transferredColors" : Int(numColors) as Int,
            "totalColors" : Int(totalColors) as Int
            ])
        
    }
    

    
    //--------------------------------------------------------------------------------
    //
    // didCompleteFandeckDownload
    //
    //
    //
    //--------------------------------------------------------------------------------
    
    func didCompleteFandeckDownload(withBytesTransferred totalBytesTransferred: UInt, withSuccess success: Bool) {
       
        
        if printDebugMessages {
            print("APP: didCompleteFandeckDownload")
        }
        
        // Send a notification
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DidCompleteFandeckDownloadNotification"), object: self, userInfo: [
            "totalBytesTransferred" : Int(totalBytesTransferred) as Int,
            "success" : success as Bool
            ])
    }
    
    //--------------------------------------------------------------------------------
    //
    // didGetCodesOEM:(NSString *)oemCode andRegistration:(NSString *)registrationCode withSuccess:(BOOL)success;
    //
    //
    //
    //--------------------------------------------------------------------------------
    func didGetCodesOEM(_ oemCode: String!, andRegistration registrationCode: String!, withSuccess success: Bool) {
        if printDebugMessages {
            print("APP: didGetCodesOEM")
        }
        
        // Send a notification
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DidGetCodes"), object: self, userInfo: [
            "oemCode" : String(oemCode) as String,
            "registrationCode" : String(registrationCode) as String,
            "success" : success as Bool
            ])
    }
    
    //--------------------------------------------------------------------------------
    //
    // didSetCodesWithSuccess:(BOOL)success;
    //
    //
    //
    //--------------------------------------------------------------------------------
    func didSetCodes(withSuccess success: Bool) {
        if printDebugMessages {
            print("APP: didSetCodes")
        }
        
        // Send a notification
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DidSetCodes"), object: self, userInfo: [
            "success" : success as Bool
        ])
    }
    
    
}
