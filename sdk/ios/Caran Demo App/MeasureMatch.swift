//
//  MeasureMatch.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 11/10/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import Foundation
import UIKit

class MeasureMatch : NSObject, NSCoding {

    // MARK: Properties
    private(set) var name : String!
    private(set) var lab_l : Float!
    private(set) var lab_a : Float!
    private(set) var lab_b : Float!
    private(set) var deltaE : Float!
    private(set) var fandeck : String!
    
    // MARK: Types
    struct PropertyKey {
        static let nameKey = "name"
        static let lab_lKey = "lab_l"
        static let lab_aKey = "lab_a"
        static let lab_bKey = "lab_b"
        static let deltaEKey = "deltaE"
        static let fandeckKey = "fandeck"
    }

    
    init(_name:String, _l:Float,_a:Float,_b:Float,_dE:Float,_fandeck:String){
    
        name = _name
        lab_l = _l
        lab_a = _a
        lab_b = _b
        deltaE = _dE
        fandeck = _fandeck
        super.init()
    }
    
    // MARK: Methods
    func getDeltaEString()->String{    
        return String(format: "%.2f", deltaE)
    }

    static func cnameToString(cObj : (UInt8,UInt8,UInt8,UInt8,UInt8,UInt8,UInt8,UInt8,UInt8,UInt8,UInt8,UInt8))->String{
        
        var str : String = ""
 
        let mirror:Mirror = Mirror(reflecting: cObj)
        var index = mirror.children.startIndex
        
        for _ in 0..<mirror.children.count {
           
            // get the char value
            let myCharValue = mirror.children[index].value as! UInt8
            
            if(myCharValue == 0){
                break
            }else{
                str.append(Character(UnicodeScalar(myCharValue)))
            }
            
            index = mirror.children.index(after: index)
        }
        
        return str
    }
    
    
    // MARK: NSCoding
    func encode(with aCoder:NSCoder) {
        
        aCoder.encode(name, forKey: PropertyKey.nameKey)
        
        aCoder.encode(lab_l, forKey: PropertyKey.lab_lKey)
        aCoder.encode(lab_a, forKey: PropertyKey.lab_aKey)
        aCoder.encode(lab_b, forKey: PropertyKey.lab_bKey)
        
        aCoder.encode(deltaE, forKey: PropertyKey.deltaEKey)
        
        aCoder.encode(fandeck, forKey: PropertyKey.fandeckKey)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        let name = aDecoder.decodeObject(forKey: PropertyKey.nameKey) as! String
        
        var l = aDecoder.decodeObject(forKey: PropertyKey.lab_lKey) as? Float
        var a = aDecoder.decodeObject(forKey: PropertyKey.lab_aKey) as? Float
        var b = aDecoder.decodeObject(forKey: PropertyKey.lab_bKey) as? Float
        
        if(l == nil){
            l = 0
        }
        if(a == nil){
            a = 0
        }

        if(b == nil){
            b = 0
        }

        
        var fandeckName = aDecoder.decodeObject(forKey: PropertyKey.fandeckKey) as? String
        if(fandeckName == nil){
            fandeckName = ""
        }
        
        let dE = aDecoder.decodeObject(forKey: PropertyKey.deltaEKey) as! Float
        
        self.init(_name: name,_l: l!,_a: a!,_b: b!,_dE: dE, _fandeck: fandeckName!)
    }
    
}
