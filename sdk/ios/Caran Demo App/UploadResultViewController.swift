//
//  UploadResultViewController.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 04/11/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import UIKit

class UploadResultViewController: UIViewController {

    // MARK: Properties
    public var uploadSucceded:Bool = false
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    // Remove the observers
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("UploadResultViewController viewDidLoad")
        
        
        
        // Disconnect Notification
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didDisconnect),
            name: NSNotification.Name(rawValue: "DisconnectFromDeviceNotification"),
            object: nil)
        
        // Enable sleep mode
        UIApplication.shared.isIdleTimerDisabled = false
        
        
        if(uploadSucceded){
            titleLabel.text = "Download completed"
            descLabel.text = "The fandeck was successfully downloaded into your device"
        }else{
            titleLabel.text = "Download failed"
            descLabel.text = "Your device will now work in LAB only mode"
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    

    @IBAction func doneButtonPressed(_ sender: Any) {
        CSVFandecksListViewController.FinishedDownload = true
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func didDisconnect(notification: NSNotification) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
}
