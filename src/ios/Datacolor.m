/********* Datacolor.m Cordova Plugin Implementation *******/

#import <Cordova/CDV.h>
#import <Cordova/CDVPlugin.h>
#import "DCICaranLib.h"

@interface CaranController:NSObject<CaranProtocol> {

}
-(void) didReceiveListOfVisibileDevices:(NSMutableArray<CBPeripheral*>*)peripheralList;

@end

@interface Datacolor : <CDVPlugin, CaranProtocol> {
  // Member variables go here.
}

- (void)listDevices:(CDVInvokeUrlCommand*)command;

@end

@implementation CaranController

- (void) didReceiveListofVisibileDevices:(NSMutableArray<CBPeripheral*>*)peripheralList {

  print("APP: didReceiveListOfVisibileDevices")
  
  devices = []
  
  
  // Populate our device list with what came back from the scan.
  for element in peripheralList {
    devices.append(element as! CBPeripheral)
  }
  
  // Send a notification
  NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CaranListOfVisibleDevicesNotification"), object: self, userInfo: ["devices" : devices])
}
    
    
@end

@implementation Datacolor

- (void)listDevices:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSString* echo = [command.arguments objectAtIndex:0];

    if (echo != nil && [echo length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:echo];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }

    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

@end
