//
//  MatchTableViewCell.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 05/10/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import UIKit

class MatchTableViewCell: UITableViewCell {

    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: PROPERTIES
    // MARK:
    //--------------------------------------------------------------------------------
    
    @IBOutlet weak var MatchPreview: UIView!
    
    @IBOutlet weak var MeasureView: UIView!
    
    @IBOutlet weak var ColorName: LabelAndValueView!
        
    @IBOutlet weak var Fandeck: LabelAndValueView!
    
    @IBOutlet weak var deltaE: LabelAndValueView!
    
    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: OVERRIDES
    // MARK:
    //--------------------------------------------------------------------------------
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
                
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
