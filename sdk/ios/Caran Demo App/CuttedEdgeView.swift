//
//  CuttedEdgeView.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 06/10/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import Foundation
import UIKit

class CuttedEdgeView: UIView {
    
    override func draw(_ rect: CGRect) {
        
        // Create the  mask
        
        let shape = CAShapeLayer()  // define the shape
       
        let cPath:UIBezierPath = UIBezierPath(); // define the path

        let cutSize = CGFloat(20) // size of the cutted edge

        // define the points
        let point1 = CGPoint(x: rect.minX, y: rect.minY)
        let point2 = CGPoint(x: (rect.maxX - cutSize), y: rect.minY)
        let point3 = CGPoint(x: rect.maxX, y: (rect.minY+cutSize))
        let point4 = CGPoint(x: rect.maxX, y: rect.maxY)
        let point5 = CGPoint(x: rect.minX, y: rect.maxY)
        
        // draw the path
        cPath.move(to: point1)
        cPath.addLine(to: point2)
        cPath.addLine(to: point3)
        cPath.addLine(to: point4)
        cPath.addLine(to: point5)
        cPath.addLine(to: point1)
        cPath.close()
        
        // assign the path to the shaep
        shape.path = cPath.cgPath
        
        // apply the mask
        self.layer.mask = shape
    }
    
}
