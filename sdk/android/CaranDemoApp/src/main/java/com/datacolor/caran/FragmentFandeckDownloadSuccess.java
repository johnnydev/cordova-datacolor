//
//  FragmentDownloadSuccess.java
//  Caran Demo App
//
//  Copyright © 2017 Datacolor. All rights reserved.
//

package com.datacolor.caran;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class FragmentFandeckDownloadSuccess extends FragmentBase {
    interface FragmentFandeckDownloadSuccessDelegate {
        void fragmentFandeckDownloadSuccessDidRequestDismiss(FragmentFandeckDownloadSuccess sender);
    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_fandeck_download_success;
    }


    @Override
    protected void init() {
        this.setTitle("Success");

        ((Button) getView().findViewById(R.id.button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }


    @Override
    protected void dismiss() {
        getMainActivity().fragmentFandeckDownloadSuccessDidRequestDismiss(this);
    }
}