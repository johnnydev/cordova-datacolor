//
//  FragmentFandeckDownload.java
//  Caran Demo App
//
//  Copyright © 2017 Datacolor. All rights reserved.
//

package com.datacolor.caran;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class FragmentFandeckDownload extends FragmentBase {
    interface FragmentFandeckDownloadDelegate {
        String fragmentFandeckDownloadFandeckName(FragmentFandeckDownload sender);
        int fragmentFandeckDownloadFandeckProgress(FragmentFandeckDownload sender);
        String fragmentFandeckDownloadTimeRemaining(FragmentFandeckDownload sender);
        String fragmentFandeckDownloadColorsDownloaded(FragmentFandeckDownload sender);
    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_fandeck_download;
    }


    @Override
    protected void init() {
        this.reload();
    }


    @Override
    public void  reload() {
        ((TextView) (getView().findViewById(R.id.fandeck_name))).setText(getMainActivity().fragmentFandeckDownloadFandeckName(this));
        ((ProgressBar) (getView().findViewById(R.id.progress_bar))).setProgress(getMainActivity().fragmentFandeckDownloadFandeckProgress(this));
        ((TextView) (getView().findViewById(R.id.remaining_time))).setText(getMainActivity().fragmentFandeckDownloadTimeRemaining(this));
        ((TextView) (getView().findViewById(R.id.colors_loaded))).setText(getMainActivity().fragmentFandeckDownloadColorsDownloaded(this));
    }
}