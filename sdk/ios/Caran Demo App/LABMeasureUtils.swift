//
//  LABMeasureUtils.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 07/10/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//


import Foundation
import UIKit

class LABMeasureUtils {

    private(set) var labMeasure : LABMeasure
    private(set) var rgb_r : Int
    private(set) var rgb_g : Int
    private(set) var rgb_b : Int
    private(set) var uicolor : UIColor
    private(set) var hex : String    
    
    public var lab_l : Float { get { return labMeasure.lab_l } }
    public var lab_a : Float { get { return labMeasure.lab_a } }
    public var lab_b : Float { get { return labMeasure.lab_b } }
    public var name : String { get { return labMeasure.name } }
    
    public var lab_string : String { get { return String(format: "L: %@ A: %@ B: %@", lab_l_string, lab_a_string, lab_b_string) } }
    
    public var lab_l_string : String { get { return String(format: "%.2f", labMeasure.lab_l) } }
    public var lab_a_string : String { get { return String(format: "%.2f", labMeasure.lab_a) } }
    public var lab_b_string : String { get { return String(format: "%.2f", labMeasure.lab_b) } }
    
    public var rgb_r_string : String { get { return String(rgb_r) } }
    public var rgb_g_string : String { get { return String(rgb_g) } }
    public var rgb_b_string : String { get { return String(rgb_b) } }

    
    
    
    init(_labMeasure : LABMeasure) {
    
        labMeasure = _labMeasure
        
        let labArray = [CGFloat(_labMeasure.lab_l),CGFloat(_labMeasure.lab_a),CGFloat(_labMeasure.lab_b),1.0]
        
        uicolor = UIColor.init(fromCIE_LabArray: labArray)
        var rgbArr = uicolor.rgbaArray()
        
        var r = rgbArr?[0] as! Float
        var g = rgbArr?[1] as! Float
        var b = rgbArr?[2] as! Float
        
        if(r > 1) {r = 1}
        if(g > 1) {g = 1}
        if(b > 1) {b = 1}
        
        if(r < 0) {r = 0}
        if(g < 0) {g = 0}
        if(b < 0) {b = 0}
        
        rgb_r = Int(r * 255)
        rgb_g = Int(g * 255)
        rgb_b = Int(b * 255)
        
        hex = ""
        hex = LABMeasureUtils.toHexString(r: rgb_r, g: rgb_g, b: rgb_b)
        
    }

    
    // MARK: Class Methods
    static func toHexString(r: Int,g: Int,b: Int) -> String {
        
        let r:CGFloat = CGFloat(r)
        let g:CGFloat = CGFloat(g)
        let b:CGFloat = CGFloat(b)
        
        let rgb:Int = (Int)(r)<<16 | (Int)(g)<<8 | (Int)(b)<<0
        
        return NSString(format:"#%06x", rgb) as String
    }
    
}
