//
//  FragmentTabs.java
//  Caran Demo App
//
//  Copyright © 2017 Datacolor. All rights reserved.
//

package com.datacolor.caran;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TabHost;
import android.widget.TextView;

import java.util.HashMap;
import java.util.StringTokenizer;

public class FragmentTabs extends FragmentBase {
    interface FragmentTabsDelegate {
    }

    TabHost tabHost = null;
    private final String TAG_TAB_MEASURE = "measure";
    private final String TAG_TAB_MATCH = "match";
    private final String TAG_TAB_DEVICE = "device";
    private String activeTabId = null;

    private HashMap<String, FragmentBase> fragmentsMap = new HashMap<String, FragmentBase>();
    private HashMap<String, String> indicesMap = new HashMap<String, String>();
//    private enum FragmentTabsTab {FragmentTabsTabMeasure, FragmentTabsTabMatch, FragmentTabsTabDevice};


    @Override
    protected int layoutId() {
        return R.layout.fragment_tabs;
    }


    private void addTab(String tag, int viewId, CharSequence label, int drawableResourceId, FragmentBase fragment) {
        TabHost.TabSpec spec = tabHost.newTabSpec(tag);
        spec.setContent(viewId);
        View tabItemView = getLayoutInflater().inflate(R.layout.tab_item, null);
        ((TextView) tabItemView.findViewById(R.id.title)).setText(label);
        ((ImageView) tabItemView.findViewById(R.id.icon)).setImageResource(drawableResourceId);
        spec.setIndicator(tabItemView);
        tabHost.addTab(spec);
        fragmentsMap.put(tag, fragment);
        indicesMap.put(tag, Integer.toString(tabHost.getTabWidget().getTabCount() - 1));
    }


    private void setTabBadge(String tag, String badge) {
        String tabIndexString = indicesMap.get(tag);
        if (tabIndexString != null) {
            int tabIndex = Integer.valueOf(tabIndexString);
            View tabView = tabHost.getTabWidget().getChildAt(tabIndex);
            if (tabView != null) {
                ViewGroup container = (ViewGroup) tabView.findViewById(R.id.badge_cotainer);
                if ((badge != null) && (badge.length() > 0)) {
                    ((TextView) tabView.findViewById(R.id.badge)).setText(badge);
                    container.setVisibility(View.VISIBLE);
                } else {
                    container.setVisibility(View.GONE);
                }
            }
        }
    }


    @Override
    protected void init() {
        tabHost = (TabHost) getView().findViewById(R.id.device_tabhost);
        tabHost.setup();

        this.addTab(TAG_TAB_MEASURE, R.id.device_tab_measure, "Measure", R.drawable.ic_tab_measure, new FragmentMeasure().init(getMainActivity(), (ViewGroup) getView().findViewById(R.id.device_tab_measure)));
        this.addTab(TAG_TAB_MATCH, R.id.device_tab_match, "Match", R.drawable.ic_tab_match, new FragmentMatch().init(getMainActivity(), (ViewGroup) getView().findViewById(R.id.device_tab_match)));
        this.addTab(TAG_TAB_DEVICE, R.id.device_tab_device, "Device", R.drawable.ic_tab_device, new FragmentDevice().init(getMainActivity(), (ViewGroup) getView().findViewById(R.id.device_tab_device)));

        activeTabId = TAG_TAB_MEASURE;
        fragmentsMap.get(TAG_TAB_MEASURE).reload();

        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener(){
            @Override
            public void onTabChanged(String tabId) {
                /*if (TAG_TAB_MEASURE.equals(tabId)) {
                } else if (TAG_TAB_MATCH.equals(tabId)) {
                } else if (TAG_TAB_DEVICE.equals(tabId)) {
                }*/
                activeTabId = tabId;
                FragmentBase fragment = fragmentsMap.get(tabId);
                fragment.reload();
            }});

        this.reload();
    }


    @Override
    public void reload() {
        String deviceBadge = null;
        if (getDciCaranLib().needsCalibration())
            deviceBadge = "!";
        this.setTabBadge(TAG_TAB_DEVICE, deviceBadge);
    }


    public void reloadActiveTab() {
        if (activeTabId != null) {
            fragmentsMap.get(activeTabId).reload();
        }
    }
}