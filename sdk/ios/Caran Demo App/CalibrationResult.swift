//
//  CalibrationResult.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 04/10/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import UIKit

class CalibrationResult: UIViewController {

    // Mark properties:
    
    @IBOutlet weak var Congratulations2: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.barTintColor = DCUtils.MainColor
        self.navigationController!.navigationBar.topItem!.title = "Back"
        
        // 5.18.17: Change the text shown in case we need to warn them about the extended range.
        if (CaranController.instance.calibrationWarning) {
            Congratulations2.text = "Your device is now calibrated and ready to measure (however: please make sure that your device is on the calibration tile)"
        }
        else {
            Congratulations2.text = "Your device is now calibrated and ready to measure"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func closeWizard(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: {});
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
