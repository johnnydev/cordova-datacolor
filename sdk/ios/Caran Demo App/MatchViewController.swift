//
//  MatchViewController.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 04/10/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import UIKit

class MatchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{

    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: PROPERTIES
    // MARK:
    //--------------------------------------------------------------------------------
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var emptyViewSubTitle: UILabel!
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: OVERRIDES
    // MARK:
    //--------------------------------------------------------------------------------
    
    
    // Remove the observers
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        // Receive measure
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didReceiveMeasure),
            name: NSNotification.Name(rawValue: "MeasureFromDeviceNotification"),
            object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBarController?.title = "Match"
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "List", style: .plain, target: self, action: #selector(listTapped))
        
        // restore the navigation title
        self.tabBarController?.navigationItem.titleView = nil
        self.tabBarController?.navigationItem.title = "Match"
        
        tableView.reloadData()
        
        
        // empty view
        setEmptyView()
    }

    
    
    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: METHODS
    // MARK:
    //--------------------------------------------------------------------------------
    
    func setEmptyView(){
        emptyView.isHidden = true
        
        let measure = CaranController.instance.lastMeasure
        if((measure == nil) || (measure!.matchArr.count == 0)){
            emptyView.isHidden = false
        }
        
        if(CaranController.instance.isModeLabPlusMatch()){
            emptyViewSubTitle.isHidden = true
        }else{
            emptyViewSubTitle.isHidden = false
        }
    }
    
    func didReceiveMeasure(notification: NSNotification) {
        
        print("MatchViewController was notified of a measure")

        tableView.reloadData()
        
        setEmptyView()
    }
    
    func listTapped() {
        
        let modalViewController = self.storyboard!.instantiateViewController(withIdentifier: "MeasurementsView")
        modalViewController.modalPresentationStyle = .fullScreen
        self.present(modalViewController, animated: true, completion: nil)
        
    }
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: TABLE PROTOCOL
    // MARK:
    //--------------------------------------------------------------------------------
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(CaranController.instance.lastMeasure == nil){
            return 3
        }
        else if(CaranController.instance.lastMeasure!.matchArr.count == 0){
            return 3
        }
        else{
            return CaranController.instance.lastMeasure!.matchArr.count
        }
        
    }
    
    @available(iOS 2.0, *)
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "matchTableViewCellProto", for: indexPath) as! MatchTableViewCell
        
        let measure = CaranController.instance.lastMeasure
        
        if((measure == nil) || (measure!.matchArr.count == 0)){
        
            // empty row
            
            cell.ColorName.isHidden = true
            cell.Fandeck.isHidden = true
            cell.MeasureView.isHidden = true
            cell.MatchPreview.backgroundColor = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1) //#EEEEEE
            cell.deltaE.isHidden = true
        
        }
        else {

            // standard row
            
            cell.ColorName.isHidden = false
            cell.Fandeck.isHidden = false
            cell.MeasureView.isHidden = false
            cell.deltaE.isHidden = false
            
            let match = measure!.matchArr[indexPath.row]
            
            let measureUtils = LABMeasureUtils(_labMeasure: measure!)
            cell.MeasureView.backgroundColor = measureUtils.uicolor
            Animations.ZoomIn(viewToAnimate: cell.MeasureView)
            
            // Color Name
            cell.ColorName.setValue(_value: match.name)
            
            // Delta E
            cell.deltaE.setLabelAndValue(_label: "DeltaE", _value: match.getDeltaEString())
            cell.deltaE.setTransparent()
            cell.deltaE.setLabelColorWith(l: match.lab_l)
            
            // Fandeck
            cell.Fandeck.setLabelAndValue(_label: "FANDECK", _value: match.fandeck)
            
            let l = CGFloat(match.lab_l)
            let a = CGFloat(match.lab_a)
            let b = CGFloat(match.lab_b)
            
            
            let labArray = [CGFloat(l),CGFloat(a),CGFloat(b),1.0]
            let uicolor = UIColor.init(fromCIE_LabArray: labArray)
            cell.MatchPreview.backgroundColor = uicolor
        
        }
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    
}
