//
//  FragmentFandecksList.java
//  Caran Demo App
//
//  Copyright © 2017 Datacolor. All rights reserved.
//

package com.datacolor.caran;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.datacolor.DCICaranLib;
import com.datacolor.caran.model.Fandeck;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.max;
import static java.lang.Math.round;

public class FragmentFandecksList extends FragmentBase {
    interface FragmentFandecksListDelegate {
        void fragmentFandecksListDidRequestDismiss(FragmentFandecksList sender);
        void fragmentFandecksListDidRequestDownloadFandecks(FragmentFandecksList sender, ArrayList<Fandeck> fandecks);
    }

    private int maxFandecks = 0;
    private int maxColors = 0;
    private int numFandecks = 0;
    private int numColors = 0;

    private ListView fandecksListView = null;

    private class FandeckItem {
        public Fandeck fandeck;
        private boolean isChecked = false;

        public FandeckItem(Fandeck fandeck) {
            this.fandeck = fandeck;
        }

        public void setChecked(boolean checked) {
            if (isChecked != checked) {
                this.isChecked = checked;
                if (checked) {
                    numFandecks += 1;
                    numColors += fandeck.colors_number;
                } else {
                    numFandecks -= 1;
                    numColors -= fandeck.colors_number;
                }
            }
        }

        public boolean isChecked() {
            return isChecked;
        }
    }

    private class ItemsAdapter extends ArrayAdapter<FandeckItem> {
        public View view;
        public ItemsAdapter(Context context, List<FandeckItem> objects) {
            super(context, R.layout.list_item_fandeck, objects);
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup parent) {
            this.view = convertView;
            if (view == null) {
                LayoutInflater layoutInflater = getLayoutInflater();
                view = layoutInflater.inflate(R.layout.list_item_fandeck, null);

                ViewGroup accessory = (ViewGroup) view.findViewById(R.id.accessory);
                layoutInflater.inflate(R.layout.list_item_accessory_checkbox, accessory, true);
            }

            final FandeckItem item = getItem(pos);

            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    item.setChecked(buttonView.isChecked());
                    reload();
                }
            });

            ((TextView) (view.findViewById(R.id.title))).setText(item.fandeck.name);
            ((TextView) (view.findViewById(R.id.num_colors))).setText("Number of colors: " + item.fandeck.colors_number);
            ((CheckBox) (view.findViewById(R.id.checkbox))).setChecked(item.isChecked);

            return view;
        }
    }


    private ArrayList<FandeckItem> fandecksList = null;
    private ItemsAdapter fandecksAdapter = null;


    @Override
    protected int layoutId() {
        return R.layout.fragment_fandecks_list;
    }


    @Override
    protected void init() {
        this.setTitle("Select Fandecks");
        this.enableCloseButton();

        DCICaranLib dciCaranLib = getDciCaranLib();
        maxFandecks = dciCaranLib.maxNumFandecks();
        maxColors = dciCaranLib.maxNumColors();

        fandecksListView = (ListView) getView().findViewById(R.id.list_view);

        fandecksList = new ArrayList<FandeckItem>();

        ArrayList<Fandeck> fandecks = Fandeck.fandecksListFromLocalJson(getMainActivity());
        for (Fandeck fandeck : fandecks)
            fandecksList.add(new FandeckItem(fandeck));

        fandecksAdapter = new ItemsAdapter(getMainActivity(), fandecksList);

        fandecksListView.setAdapter(fandecksAdapter);

        fandecksListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FandeckItem item = fandecksList.get(position);
                item.setChecked(!item.isChecked);
                reload();
            }
        });

        ((Button) getView().findViewById(R.id.button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Fandeck> selectedFandecks = new ArrayList<Fandeck>();
                for (FandeckItem item : fandecksList) {
                    if (item.isChecked())
                        selectedFandecks.add(item.fandeck);
                }
                getMainActivity().fragmentFandecksListDidRequestDownloadFandecks((FragmentFandecksList) self, selectedFandecks);
            }
        });

        this.reload();
    }


    @Override
    public void reload() {
        ((TextView) (getView().findViewById(R.id.num_colors))).setText("" + numColors + " of " + maxColors + " colors");
        ((ProgressBar) (getView().findViewById(R.id.progress_bar))).setProgress(round(((float)(numColors)) / ((float)(maxColors)) * 100.0f));
        ((TextView) (getView().findViewById(R.id.download_time))).setText("Approximate download time: " + getMainActivity().estimatedDownloadTimeForNumColors(numColors));
        DCICaranLib dciCaranLib = getDciCaranLib();
        ((Button) (getView().findViewById(R.id.button))).setEnabled((numFandecks > 0));
        fandecksListView.invalidateViews();
    }


    @Override
    protected void dismiss() {
        getMainActivity().fragmentFandecksListDidRequestDismiss(this);
    }
}