//
//  DeviceStatusRow.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 06/10/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import UIKit

@IBDesignable

class DeviceStatusRow: UIView {

    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: PROPERTIES
    // MARK:
    //--------------------------------------------------------------------------------
    
    var contentView : UIView?    
    
    @IBOutlet weak var statusIndicator: UIView!
    
    @IBOutlet weak var labelAndValueItem: LabelAndValueView!
    
    private var _showStatusIndicator : Bool = true
    public var showStatusIndicator : Bool {
        
        get {
            return _showStatusIndicator
        }
        
        set {
            
            _showStatusIndicator = newValue
            
            if(_showStatusIndicator) {
                statusIndicator.isHidden = false
            }
            else {
                statusIndicator.isHidden = true
            }
        }
    }
    
    
    public var labelText : String {
        
        get {
            return labelAndValueItem.labelItem.text!
        }
        set {
            return labelAndValueItem.labelItem.text = newValue
        }
    }
    
    
    public var valueText : String {
        
        get {
            return labelAndValueItem.valueItem.text!
        }
        set {
            return labelAndValueItem.valueItem.text = newValue
        }
    }
    
    //labelLeading
    @IBInspectable var labelLeading: Float = 0 {
        didSet {
            let filteredConstraints = labelAndValueItem.constraints.filter { $0.identifier == "labelLeading" }
            if filteredConstraints.first != nil {
                filteredConstraints.first?.constant = CGFloat(labelLeading)
            }
        }
    }
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: METHODS
    // MARK:
    //--------------------------------------------------------------------------------
    
    
    
    public func statusOK(){
        statusIndicator.backgroundColor = UIColor.green
    }
    
    public func statusWarning(){
        statusIndicator.backgroundColor = UIColor.yellow
    }
    
    public func statusError(){
        statusIndicator.backgroundColor = UIColor.red
    }
    
    
    
    
    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: INIT
    // MARK:
    //--------------------------------------------------------------------------------
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        contentView = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        contentView!.frame = bounds
        
        // Make the view stretch with containing view
        contentView!.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView!)
    }
    
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        
        // initial setup
        self.statusOK()
        
        
        // return the view
        return view
    }
    
    
  
    
    
    
    

}
