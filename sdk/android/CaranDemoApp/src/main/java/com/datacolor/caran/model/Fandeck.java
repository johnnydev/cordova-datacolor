//
//  Fandeck.java
//  Caran Demo App
//
//  Copyright © 2017 Datacolor. All rights reserved.
//

package com.datacolor.caran.model;


import android.app.Activity;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;

import static android.support.v4.graphics.ColorUtils.LABToColor;

public class Fandeck {
    public String name;
    public String id;
    public String version;
    public String device;
    public int colors_number;
    public String filename;


    //private methods


    private static String stringFromAssetFile(Activity activity, String filename) {
        String result = "";
        try {
            InputStream is = activity.getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            result = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return "";
        }
        return result;
    }


    private static String jsonStringFromAsset(Activity activity) {
        return Fandeck.stringFromAssetFile(activity, "fandecks.json");
    }

    //public methods


    public Fandeck(String name, String id, String version, String device, int colorsNumber, String filename) {
        this.name = name;
        this.id = id;
        this.version = version;
        this.device = device;
        this.colors_number = colorsNumber;
        this.filename = filename;
    }


    public static ArrayList<Fandeck> fandecksListFromLocalJson(Activity activity) {
        ArrayList<Fandeck> result = new ArrayList<Fandeck>();

        String jsonString = Fandeck.jsonStringFromAsset(activity);
        if (jsonString != null) {
            try {
                JSONObject obj = new JSONObject(jsonString);
                JSONArray arr = obj.getJSONArray("fandecks");
                if (arr != null) {
                    for (int i=0; i<arr.length(); i++) {
                        JSONObject fandeckInfo = arr.getJSONObject(i);
                        if (fandeckInfo != null) {
                            Fandeck fandeck = new Fandeck(fandeckInfo.getString("name"), fandeckInfo.getString("id"), fandeckInfo.getString("version"),
                                    fandeckInfo.getString("device"), fandeckInfo.getInt("colors_number"), fandeckInfo.getString("filename"));
                            result.add(fandeck);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return result;
    }


    public static String getCsvFileAsString(Activity activity, String filename) {
        return Fandeck.stringFromAssetFile(activity, filename);
    }
}