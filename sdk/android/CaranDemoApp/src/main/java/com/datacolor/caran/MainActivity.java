//
//  MainActivity.java
//  Caran Demo App
//
//  Copyright © 2017 Datacolor. All rights reserved.
//

package com.datacolor.caran;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.datacolor.DCICaranLib;
import com.datacolor.NSMutableArray;
import com.datacolor.caran.model.Fandeck;
import com.datacolor.caran.model.LABMeasure;

import java.util.ArrayList;

import static java.lang.Math.round;


public class MainActivity extends AppCompatActivity implements DCICaranLib.CaranProtocol, FragmentHome.FragmentHomeDelegate,
        FragmentPeripheralList.FragmentPeripheralListDelegate, FragmentMeasure.FragmentMeasureDelegate, FragmentMatch.FragmentMatchDelegate,
        FragmentDevice.FragmentDeviceDelegate, FragmentModifyCodes.FragmentModifyCodesDelegate, FragmentMeasurementsList.FragmentMeasurementsListDelegate,
        FragmentCalibrate.FragmentCalibrateDelegate, FragmentCalibrationSuccess.FragmentCalibrationSuccessDelegate,
        FragmentFandecksList.FragmentFandecksListDelegate, FragmentFandeckDownload.FragmentFandeckDownloadDelegate,
        FragmentFandeckDownloadSuccess.FragmentFandeckDownloadSuccessDelegate {

    //region variables
    private DCICaranLib dciCaranLib = null;
    private ProgressDialog progressDialog = null;
    private boolean showingModalFragment = false;

    private boolean offlineMode = false;
    private ArrayList<BluetoothDevice> peripherals = null;

    private LABMeasure lastMeasurement = null;
    private ArrayList<LABMeasure> allMeasurements = new ArrayList<>();

    private String oemCode = "";
    private String registrationCode = "";
    private boolean codesReceived = false;
    private boolean codesRequested = false;

    private String downloadFandecksNames = "";
    private double downloadTimeForChunk = 1.744186;
    private long previousChunkTime = 0;
    private int colorsToDownload = 0;
    private int colorsDownloaded = 0;
    //endregion

    public boolean calibrationWarning = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (dciCaranLib == null) {
            BluetoothManager bluetoothManager = (BluetoothManager) this.getSystemService(Context.BLUETOOTH_SERVICE);
            dciCaranLib = DCICaranLib.libWithDelegate(this, bluetoothManager, this.getApplicationContext());
            this.restoreMeasurementMode();
            this.loadMeasurements();
        }

        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter != null) {
            // Device supports Bluetooth
            if (!mBluetoothAdapter.isEnabled()) {
                // Bluetooth isn't enabled, so enable it.
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        this.setFragment(new FragmentHome());
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        this.saveMeasurementMode();

        if (dciCaranLib != null) {
            if (isConnectedToDevice())
                dciCaranLib.disconnectFromDevice();
            dciCaranLib = null;
        }
    }


    //region private methods


    private void setFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        try {
            fragmentTransaction.commit();
        } catch (Exception e) {
            finish();
        } finally {
            showingModalFragment = false;
        }
    }


    private void showModalFragment(Fragment fragment) {
        this.dismissModalFragment();

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.add(R.id.fragment_container, fragment, "modal");
        try {
            fragmentTransaction.commit();
        } catch (Exception e) {
            finish();
        } finally {
            showingModalFragment = true;
        }
    }


    private void dismissModalFragment() {
        if (showingModalFragment) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment fragment = fragmentManager.findFragmentByTag("modal");
            if (fragment != null) {
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                fragmentTransaction.remove(fragment);
                try {
                    fragmentTransaction.commit();
                } catch (Exception e) {
                    finish();
                } finally {
                    showingModalFragment = false;
                }
                fragmentManager.executePendingTransactions();
            } else {
                showingModalFragment = false;
            }
        }
    }


    private void reloadModalFragment() {
        if (showingModalFragment) {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag("modal");
            if (fragment != null) {
                ((FragmentBase) fragment).reload();
            }
        }
    }


    private void showProgressDialog (String text) {
        hideProgressDialog();
        progressDialog = ProgressDialog.show(this, text, null);
    }


    private void hideProgressDialog() {
        if (progressDialog != null){
            progressDialog.cancel();
            progressDialog = null;
        }
    }


    private void connectToDevice(BluetoothDevice device) {
        Toast.makeText(this, "Connecting to device: " + device.getName(), Toast.LENGTH_SHORT).show();
        dciCaranLib.connectToDevice(device);
    }


    private FragmentTabs getTabsFragment() {
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (currentFragment instanceof FragmentTabs)
            return ((FragmentTabs)currentFragment);
        return null;
    }


    private void reloadActiveTab() {
        FragmentTabs tabs = this.getTabsFragment();
        if (tabs != null) {
            tabs.reloadActiveTab();
        }
    }


    private void reloadTabsFragment() {
        FragmentTabs tabs = this.getTabsFragment();
        if (tabs != null) {
            tabs.reload();
        }
    }


    private void loadMeasurements() {
        allMeasurements.clear();
        String serialized = PreferenceManager.getDefaultSharedPreferences(this).getString("measurements", "");
        if (serialized.length() > 0) {
            String[] measurementsB64s = serialized.split(",");
            for (String measurementB64String : measurementsB64s) {
                LABMeasure measure = new LABMeasure(measurementB64String);
                allMeasurements.add(measure);
            }
        }
    }


    private void saveMeasurements() {
        if (allMeasurements.size() > 0) {
            String serialized = "";
            for (LABMeasure measurement : allMeasurements)
                serialized = serialized + measurement.toB64String() + ",";
            serialized.substring(0, serialized.length() - 1);
            PreferenceManager.getDefaultSharedPreferences(this).edit().putString("measurements", serialized).commit();
        } else {
            PreferenceManager.getDefaultSharedPreferences(this).edit().putString("measurements", "").commit();
        }
    }


    private void saveLastMeasurement() {
        allMeasurements.add(lastMeasurement.copy());
        this.saveMeasurements();
    }


    private void clearSavedMeasurements() {
        allMeasurements.clear();
        this.saveMeasurements();
    }


    private void saveMeasurementMode() {
        if (dciCaranLib != null) {
            int mode = dciCaranLib.getMeasurementMode().ordinal();
            PreferenceManager.getDefaultSharedPreferences(this).edit().putInt("measurementMode", mode).commit();
        }
    }


    private void restoreMeasurementMode() {
        if (dciCaranLib != null) {
            int mode = PreferenceManager.getDefaultSharedPreferences(this).getInt("measurementMode",
                    DCICaranLib.CrnMeasurementMode.modeMeasureLabPlusMatch.ordinal());
            dciCaranLib.setMeasurementModeAs(DCICaranLib.CrnMeasurementMode.values()[mode]);
        }
    }
    //endregion


    //region public methods


    public DCICaranLib getDciCaranLib() {
        return dciCaranLib;
    }


    public boolean isConnectedToDevice() { return !offlineMode; }


    public String getVersionName() {
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(this.getPackageName(), PackageManager.GET_META_DATA);
            return pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }


    public void hideKeyboard() {
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        View currentFocus = this.getCurrentFocus();
        if (currentFocus != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(currentFocus.getWindowToken(), 0);
        }
    }


    public int estimatedDownloadSecondsForNumColors(int numColors) {
        int[] bytesArr = new int[1];
        int[] chunksArr = new int[1];
        dciCaranLib.getFandeckDownloadStatsFromTotalColorsAndBytesAndChunks(numColors, bytesArr, chunksArr);
        int chunks = chunksArr[0];
        double estimatedTime = downloadTimeForChunk * ((double)(chunks));
        return ((int)(estimatedTime));
    }


    public String estimatedDownloadTimeForNumColors(int numColors) {
        int seconds = this.estimatedDownloadSecondsForNumColors(numColors);
        int hours = seconds / 3600;
        int minutes = (seconds % 3600) / 60;
        seconds = seconds % 60;
        String result = "";
        if (hours > 0)
            result += hours + " h ";
        if ((minutes > 0) || (hours > 0))
            result += String.format("%02d min ", minutes);
        result += String.format("%02d sec", seconds);
        return result;
    }
    //endregion


    //region interfaces


    //region FragmentHomeDelegate
    public void fragmentHomeDidRequestConnect(FragmentHome sender) {
        this.showProgressDialog("Connecting...");
        dciCaranLib.getListOfVisibleDevices();
    }


    public void fragmentHomeDidRequestContinueDisconnected(FragmentHome sender) {
        offlineMode = true;
        this.setFragment(new FragmentTabs());
    }
    //endregion


    //region FragmentPeripheralListDelegate
    public ArrayList<BluetoothDevice> fragmentPeripheralListPeripherals(FragmentPeripheralList sender) {
        return peripherals;
    }


    public void fragmentPeripheralListDidSelectPeripheral(FragmentPeripheralList sender, BluetoothDevice peripheral) {
        this.showProgressDialog("Connecting...");
        this.connectToDevice(peripheral);
    }
    //endregion


    //region FragmentMeasureDelegate
    public void fragmentMeasureDidRequestBack(FragmentMeasure sender) {
        this.setFragment(new FragmentHome());
    }


    public void fragmentMeasureDidRequestList(FragmentMeasure sender) {
        this.showModalFragment(new FragmentMeasurementsList());
    }


    public LABMeasure fragmentMeasureMeasurement(FragmentMeasure sender) {
        return lastMeasurement;
    }


    public void fragmentMeasureDidTouchMeasureButton(FragmentMeasure sender) {
        dciCaranLib.takeMeasurement();;
    }

    //endregion


    //region FragmentMatchDelegate
    public void fragmentMatchDidRequestBack(FragmentMatch sender) {
        this.setFragment(new FragmentHome());
    }


    public void fragmentMatchDidRequestList(FragmentMatch sender) {
        this.showModalFragment(new FragmentMeasurementsList());
    }


    public LABMeasure fragmentMatchMeasurement(FragmentMatch sender) {
        return lastMeasurement;
    }
    //endregion


    //region FragmentDeviceDelegate
    public void fragmentDeviceDidRequestBack(FragmentDevice sender) {
        this.setFragment(new FragmentHome());
    }


    public String fragmentDeviceOemCode(FragmentDevice sender) {
        if (!codesReceived && !codesRequested) {
            codesRequested = true;
            dciCaranLib.getCodes();
        }
        return oemCode;
    }


    public String fragmentDeviceRegistrationCode(FragmentDevice sender) {
        if (!codesReceived && !codesRequested) {
            codesRequested = true;
            dciCaranLib.getCodes();
        }
        return registrationCode;
    }


    public void fragmentDeviceDidRequestCalibrate(FragmentDevice sender) {
        this.showModalFragment(new FragmentCalibrate());
    }


    public void fragmentDeviceDidRequestFandeckDownload(FragmentDevice sender) {
        this.showModalFragment(new FragmentFandecksList());
    }


    public void fragmentDeviceDidRequestModifyCodes(FragmentDevice sender) {
        this.showModalFragment(new FragmentModifyCodes());
    }
    //endregion


    //region FragmentModifyCodesDelegate
    public String fragmentModifyCodesOemCode(FragmentModifyCodes sender) {
        return oemCode;
    }


    public String fragmentModifyCodesRegistrationCode(FragmentModifyCodes sender) {
        return registrationCode;
    }


    public void fragmentModifyCodesDidRequestDismiss(FragmentModifyCodes sender) {
        this.dismissModalFragment();
    }


    public void fragmentModifyCodesDidRequestSaveCodes(FragmentModifyCodes sender, String oemCode, String regCode) {
        this.hideKeyboard();
        dciCaranLib.setCodesWithOEMAndRegistrationCodes(oemCode, regCode);
    }
    //endregion


    //region FragmentMeasurementsListDelegate
    public ArrayList<LABMeasure> fragmentMeasurementsListMeasurements(FragmentMeasurementsList sender) {
        return allMeasurements;
    }


    public void fragmentMeasurementsListDidRequestDismiss(FragmentMeasurementsList sender) {
        this.dismissModalFragment();
    }


    public void fragmentMeasurementsListDidRequestDeleteAll(FragmentMeasurementsList sender) {
        this.clearSavedMeasurements();
        this.dismissModalFragment();
    }

    public void fragmentMeasurementsListDidSelectMeasurement(FragmentMeasurementsList sender, LABMeasure measurement) {
        this.lastMeasurement = measurement;
        this.dismissModalFragment();
        this.reloadActiveTab();
    }
    //endregion


    //region FragmentCalibrateDelegate
    public void fragmentCalibrateDidRequestDismiss(FragmentCalibrate sender) {
        this.dismissModalFragment();
    }


    public void fragmentCalibrateDidRequestCalibrate(FragmentCalibrate sender) {
        this.showProgressDialog("Calibrating...");
        dciCaranLib.calibrate();
    }
    //endregion


    //region FragmentCalibrationSuccessDelegate
    public void fragmentCalibrationSuccessDidRequestDismiss(FragmentCalibrationSuccess sender) {
        this.dismissModalFragment();
        this.reloadTabsFragment();
        this.reloadActiveTab();
    }
    //endregion


    //region FragmentFandecksListDelegate
    public void fragmentFandecksListDidRequestDismiss(FragmentFandecksList sender) {
        this.dismissModalFragment();
    }


    public void fragmentFandecksListDidRequestDownloadFandecks(FragmentFandecksList sender, ArrayList<Fandeck> fandecks) {
        if (fandecks.size() > 0) {
            ArrayList<String> names = new ArrayList<String>();
            ArrayList<String> csvs = new ArrayList<String>();
            colorsToDownload = 0;
            for (Fandeck fandeck : fandecks) {
                names.add(fandeck.name);
                csvs.add(Fandeck.getCsvFileAsString(this, fandeck.filename));
                colorsToDownload += fandeck.colors_number;
            }
            downloadFandecksNames = TextUtils.join(", ", names);
            colorsDownloaded = 0;
            this.dismissModalFragment();
            this.showModalFragment(new FragmentFandeckDownload());
            dciCaranLib.downloadFandeckWithCSVs(csvs);
        }
    }
    //endregion


    //region FragmentFandeckDownloadDelegate
    public String fragmentFandeckDownloadFandeckName(FragmentFandeckDownload sender) {
        return downloadFandecksNames;
    }


    public int fragmentFandeckDownloadFandeckProgress(FragmentFandeckDownload sender) {
        return round(((float)(colorsDownloaded)) / ((float)(colorsToDownload)) * 100.0f);
    }


    public String fragmentFandeckDownloadTimeRemaining(FragmentFandeckDownload sender) {
        return this.estimatedDownloadTimeForNumColors(colorsToDownload - colorsDownloaded);
    }


    public String fragmentFandeckDownloadColorsDownloaded(FragmentFandeckDownload sender) {
        return (Integer.toString(colorsDownloaded) + "/" + Integer.toString(colorsToDownload));
    }
    //endregion


    //region FragmentFandeckDownloadSuccessDelegate
    public void fragmentFandeckDownloadSuccessDidRequestDismiss(FragmentFandeckDownloadSuccess sender) {
        this.dismissModalFragment();
        this.reloadActiveTab();
    }
    //endregion


    //region CaranProtocol
    public void didReceiveListOfVisibleDevices(ArrayList<BluetoothDevice> peripheralList) {
        if (peripheralList.size() == 0) {
            Toast.makeText(this, "No Caran devices found", Toast.LENGTH_SHORT).show();
            this.hideProgressDialog();
        } else if (peripheralList.size() == 1) {
            this.connectToDevice(peripheralList.get(0));
        } else {
            peripherals = peripheralList;
            this.hideProgressDialog();
            this.showModalFragment(new FragmentPeripheralList());
        }
    }


    public void didConnectToDevice(BluetoothDevice _device) {
        this.hideProgressDialog();
        offlineMode = false;
        this.setFragment(new FragmentTabs());
    }


    public void didFailToConnectToDevice(BluetoothDevice _device) {
        Toast.makeText(this, "Failed to connect to device", Toast.LENGTH_SHORT).show();
        this.hideProgressDialog();
        this.dismissModalFragment();
    }


    public void didDisconnectFromDevice() {
        Toast.makeText(this, "Connection terminated", Toast.LENGTH_SHORT).show();
        this.hideProgressDialog();
        offlineMode = true;
        this.setFragment(new FragmentHome());
    }


    public void didGetButtonPress() {
        Toast.makeText(this, "button pushed", Toast.LENGTH_SHORT).show();
        this.showProgressDialog("Measuring...");
    }


    public void didReceiveMeasurement(double l, double a, double b) {
        this.hideProgressDialog();
        lastMeasurement = new LABMeasure(l, a, b);
        this.saveLastMeasurement();
        this.reloadActiveTab();
    }


    public void didReceiveMeasurement(double l, double a, double b, DCICaranLib.FandeckMatch m1, DCICaranLib.FandeckMatch m2, DCICaranLib.FandeckMatch m3) {
        this.hideProgressDialog();
        lastMeasurement = new LABMeasure(l, a, b, new DCICaranLib.FandeckMatch[]{m1, m2, m3});
        this.saveLastMeasurement();
        this.reloadActiveTab();
    }


    public void didReceiveMeasurement(NSMutableArray theData) {
        // Placeholder, not currently implemented
        //this.hideProgressDialog();
        //lastMeasurement = new LABMeasure(l, a, b);
        //this.saveLastMeasurement();
        //this.reloadActiveTab();
    }


    public void didBecomeUncalibrated() {
//        Toast.makeText(this, "didBecomeUncalibrated", Toast.LENGTH_SHORT).show();
        this.reloadTabsFragment();
    }


    public void didPassCalibrationWithWarning(boolean warning) {
        this.hideProgressDialog();
        this.dismissModalFragment();
        calibrationWarning = warning;
        this.showModalFragment(new FragmentCalibrationSuccess());
    }


    public void didFailCalibration() {
        Toast.makeText(this, "Calibration failed - make sure the device is on the white tile.", Toast.LENGTH_LONG).show();
        this.hideProgressDialog();
    }


    public void didStartFandeckDownloadForTotalChunks(int totalChunks) {
//        Toast.makeText(this, "didStartFandeckDownloadForTotalChunks: " + totalChunks, Toast.LENGTH_SHORT).show();
    }


    public void didFandeckDownloadForChunk(int thisChunk, int totalChunks, int numTransferedColors, int totalColors) {
        colorsDownloaded = numTransferedColors;
        if (previousChunkTime == 0) {
            previousChunkTime = System.currentTimeMillis();
        } else {
            long currentChunkTime = System.currentTimeMillis();
            double deltaSeconds = ((double)(currentChunkTime - previousChunkTime)) / 1000.0;
            if (deltaSeconds > 0.0) {
                previousChunkTime = currentChunkTime;
                downloadTimeForChunk = (0.9 * downloadTimeForChunk) + (0.1 * deltaSeconds);
            }
            this.reloadModalFragment();
        }
    }


    public void didCompleteFandeckDownloadWithBytesTransferred(long totalBytesTransferred, boolean success) {
        previousChunkTime = 0;
        downloadFandecksNames = "";
        this.dismissModalFragment();
        this.showModalFragment(new FragmentFandeckDownloadSuccess());
    }


    public void didGetOEMAndRegistrationCodes(String oemCode, String registrationCode, boolean success) {
        codesRequested = false;
        if (success) {
            codesReceived = true;
            this.oemCode = oemCode;
            this.registrationCode = registrationCode;
            this.reloadActiveTab();
        }
    }


    public void didSetCodesWithSuccess(boolean success) {
        if (success) {
            Toast.makeText(this, "Codes were set successfully", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Failed to set new codes", Toast.LENGTH_SHORT).show();
        }
        this.dismissModalFragment();
        if (success)
            codesReceived = false;
        this.reloadActiveTab();
    }
    //endregion
    //endregion
}
