//
//  CalibrationNavigationController.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 04/10/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import UIKit

class CalibrationNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Disconnect Notification
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didDisconnect),
            name: NSNotification.Name(rawValue: "DisconnectFromDeviceNotification"),
            object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // Remove the observers
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: METHODS
    // MARK:
    //--------------------------------------------------------------------------------
    
    
    func didDisconnect(notification: NSNotification) {
        
        print("CalibrationNavigationController was notified of the disconnection")
        
        if(CaranController.instance.connectedDevice == nil)
        {
            _ = self.dismiss(animated: true, completion: nil)            
        }
        
        
        
    }

}
