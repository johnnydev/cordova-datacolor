//
//  FragmentMeasurementsList.java
//  Caran Demo App
//
//  Copyright © 2017 Datacolor. All rights reserved.
//

package com.datacolor.caran;


import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.datacolor.caran.model.LABMeasure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FragmentMeasurementsList extends FragmentBase {
    interface FragmentMeasurementsListDelegate {
        ArrayList<LABMeasure> fragmentMeasurementsListMeasurements(FragmentMeasurementsList sender);
        void fragmentMeasurementsListDidRequestDismiss(FragmentMeasurementsList sender);
        void fragmentMeasurementsListDidRequestDeleteAll(FragmentMeasurementsList sender);
        void fragmentMeasurementsListDidSelectMeasurement(FragmentMeasurementsList sender, LABMeasure measurement);
    }

    private ListView measurementsListView = null;

    private class MeasurementItem {
        public LABMeasure measurement;
        public String title;

        public MeasurementItem(LABMeasure measurement) {
            this.measurement = measurement;
            this.title = "L: " + String.format("%.2f", measurement.getLabL()) + " A: " + String.format("%.2f", measurement.getLabA()) + " B: " + String.format("%.2f", measurement.getLabB());
        }
    }

    private class ItemsAdapter extends ArrayAdapter<MeasurementItem> {
        public View view;
        public ItemsAdapter(Context context, List<MeasurementItem> objects) {
            super(context, R.layout.list_item_measurement, objects);
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup parent) {
            this.view = convertView;
            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.list_item_measurement, null);
            }

            final MeasurementItem item = getItem(pos);

            ((ImageView)(view.findViewById(R.id.color))).setColorFilter(item.measurement.getColor());
            ((TextView) (view.findViewById(R.id.title))).setText(item.title);

            return view;
        }
    }


    private ArrayList<MeasurementItem> measurementsList = null;
    private ItemsAdapter measurementsAdapter = null;


    @Override
    protected int layoutId() {
        return R.layout.fragment_measurements_list;
    }


    @Override
    protected void init() {
        this.setTitle("Measurements");
        this.enableCloseButton();

        measurementsListView = (ListView) getView().findViewById(R.id.list_view);

        measurementsList = new ArrayList<MeasurementItem>();
        measurementsAdapter = new ItemsAdapter(getMainActivity(), measurementsList);

        measurementsListView.setAdapter(measurementsAdapter);

        measurementsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MeasurementItem item = measurementsList.get(position);
                getMainActivity().fragmentMeasurementsListDidSelectMeasurement((FragmentMeasurementsList) self, item.measurement);
            }
        });

        ((Button) getView().findViewById(R.id.bottom_button_left)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMainActivity().fragmentMeasurementsListDidRequestDeleteAll((FragmentMeasurementsList) self);
            }
        });

        this.reload();
    }


    @Override
    public void reload() {
        measurementsList.clear();

        ArrayList<LABMeasure> measurements = getMainActivity().fragmentMeasurementsListMeasurements(this);
        for (LABMeasure measurement : measurements)
            measurementsList.add(new MeasurementItem(measurement));

        Collections.reverse(measurementsList);

        measurementsListView.invalidateViews();
    }


    @Override
    protected void dismiss() {
        getMainActivity().fragmentMeasurementsListDidRequestDismiss(this);
    }
}
