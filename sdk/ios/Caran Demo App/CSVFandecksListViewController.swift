//
//  DeviceListViewController.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 29/09/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import UIKit

class CSVFandecksListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // Data model: These strings will be the data for the table view cells
    var fandecks: [Fandeck] = [];
    var selectedFandecks: [Fandeck] = [];
    var maxNumberOfFandecks = 0
    var maxNumberOfColors = 0
    var currentNumberOfColors = 0
    
    static var StaticSelectedFandecks:[Fandeck] = [];
    static var FinishedDownload = false
    
    @IBOutlet weak var buttonDownload: UIButton!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var storageStateLabel: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var loadingView: UITableView!
    @IBOutlet weak var downloadTimeLabel: UILabel!
    
    @IBOutlet weak var indicatorWrapper: UIView!

    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        print("CSVFandecksListViewController did load");
        
        CSVFandecksListViewController.FinishedDownload = false
        
        maxNumberOfColors = CaranController.instance.maxNumColors()
        maxNumberOfFandecks = CaranController.instance.maxNumFandecks()
        
        // hides the table view
        tableView.isHidden = false
        loadingView.isHidden = true
        
        // This view controller itself will provide the delegate methods and row data for the table view.
        tableView.delegate = self
        tableView.dataSource = self
        
        // transform the proress bar
        progressBar.transform = CGAffineTransform(scaleX: 1.0, y: 6.0)
        progressBar.layer.cornerRadius = 3.0
       
        // create headerView
        let headerView = UIView()
        headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: indicatorWrapper.layer.bounds.maxY)
        headerView.isHidden = true
        self.tableView.tableHeaderView = headerView
        
        self.uiRefresh()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(CaranController.instance.connectedDevice == nil){
            self.dismiss(animated: false, completion: nil)
        }
        if(CSVFandecksListViewController.FinishedDownload){
            CSVFandecksListViewController.FinishedDownload = false
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: NAVIGATION
    // MARK:
    //--------------------------------------------------------------------------------
    
    
    @IBAction func CloseView(_ sender: AnyObject) {

        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: METHODS
    // MARK:
    //--------------------------------------------------------------------------------
  
    func uiRefresh(){
    
        currentNumberOfColors = 0
        progressBar.progressTintColor = UIColor.green
        
        for fandeck in selectedFandecks{
            currentNumberOfColors = currentNumberOfColors + fandeck.colors_number
        }
        
        if(currentNumberOfColors == 0){
            progressBar.setProgress(0.0, animated: false)
            buttonDownload.isEnabled = false
            buttonDownload.alpha = 0.5
        }
        else if(currentNumberOfColors > maxNumberOfColors){
            progressBar.setProgress(1.0, animated: false)
            buttonDownload.isEnabled = false
            buttonDownload.alpha = 0.5
            progressBar.progressTintColor = UIColor.orange
        }
        else{
            let progressBarProgress = (Float(currentNumberOfColors) / Float(maxNumberOfColors)) as Float
            progressBar.setProgress(progressBarProgress, animated: true)
            buttonDownload.isEnabled = true
            buttonDownload.alpha = 1
        }
    
        // set the text
        storageStateLabel.text = "\(self.currentNumberOfColors) of \(self.maxNumberOfColors) colors"
        
        
        // Check the number of fandecks
        if(selectedFandecks.count > maxNumberOfFandecks){
            buttonDownload.isEnabled = false
            buttonDownload.alpha = 0.5
            progressBar.progressTintColor = UIColor.orange
            storageStateLabel.text = "Max \(maxNumberOfFandecks) fandecks allowed"
        }
        
        // get the estimated download time
        if(currentNumberOfColors == 0){
            self.downloadTimeLabel.text = ""
        }else{
            let numberOfChunks = CaranController.instance.getChunksFormNumberOfColors(colors: currentNumberOfColors)
            let estimatedTime = CaranController.instance.getDownloadTimeForChunk() * Double(numberOfChunks)
            self.downloadTimeLabel.text = "Approximate download time: " + DCUtils.instance.stringFromTimeInterval(time: estimatedTime)
        }
        
        
    }
    
    
    @IBAction func buttonDownloadPressed(_ sender: Any) {
       
        var array = [String]()
        
        for fandeck in selectedFandecks{
            array.append(Fandeck.getCsvFileAsString(fileName: fandeck.filename))
        }
        
        CSVFandecksListViewController.StaticSelectedFandecks = selectedFandecks
        
        if(array.count > 0){
            // present the progress screen
            self.present(self.storyboard!.instantiateViewController(withIdentifier: "UploadNavigationController"), animated: true, completion: {
                CaranController.instance.uploadFandeckCsv(array: array)
            })
        }
        
    }
    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: TABLE PROTOCOLS IMPLEMENTATION
    // MARK:
    //--------------------------------------------------------------------------------
    
        // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.fandecks.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let reuseIdentifier : String = "mycell"
        
        var cell:UITableViewCell? =
            tableView.dequeueReusableCell(withIdentifier: reuseIdentifier)! as UITableViewCell
        
        
        if (cell != nil)
        {
            cell = UITableViewCell(style: UITableViewCellStyle.subtitle,
                                   reuseIdentifier: reuseIdentifier)
        }
        
        // set the text from the data model
        let fandeck = self.fandecks[indexPath.row] as Fandeck;
        
        cell!.selectionStyle = .none
        cell!.accessoryType = .none
        
        // check if the fandeck is selected
        for i in 0..<selectedFandecks.count{
            let aFandeck = selectedFandecks[i]
            if(aFandeck.id == fandeck.id){
                cell!.accessoryType = .checkmark
                break
            }
        }
        
        cell!.textLabel?.text = fandeck.name
        cell!.detailTextLabel?.text = "Number of colors: \(fandeck.colors_number)"
        return cell!
    }
    
    // method to run when table view cell is selected
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell:UITableViewCell = tableView.cellForRow(at: indexPath){
            
            cell.accessoryType = .checkmark
            
            let fandeck = self.fandecks[indexPath.row] as Fandeck;
            selectedFandecks.append(fandeck)
            
            self.uiRefresh()
        }
    }
    // method to run when table view cell is deselected
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        if let cell:UITableViewCell = tableView.cellForRow(at: indexPath){
            
            cell.accessoryType = .none
            
            let fandeck = self.fandecks[indexPath.row] as Fandeck;
            
            for i in 0..<selectedFandecks.count{
                let aFandeck = selectedFandecks[i]
                if(aFandeck.id == fandeck.id){
                    selectedFandecks.remove(at: i)
                    break
                }
            }
            
            self.uiRefresh()
        }
    }
    
}
