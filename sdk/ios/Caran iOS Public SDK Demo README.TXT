Caran iOS Public SDK - App version 1.0, SDK version 1.1

@ 2016-17, Datacolor

David Miller, 7.28.17

1. System requirements:

OSX El Capitan or later
XCode 8.1 or later
iPhone 4S or later (or iPad 3 or later) running iOS 9.3.3 or later

2. The Caran Demo App project contains two parts:

- The Caran Demo App itself, written in Swift 3.0. It builds as a standalone iOS app and links with the Caran SDK library described below for all device communications. The Caran Demo App not only demonstrates how to use all of the SDK functions to control the Caran device; it also shows how to set up certain advanced operations in the UI for things such as measuring; stored lists of measurements; displaying device information such as fandeck content; and fandeck downloading, with progress and download time estimations.

- The Caran SDK library. This is written in Objective-C and is included in the project as a standalone precompiled library (libDCICaran.a, located in the Frameworks folder). The associated header files in the Headers folder in the project define its API.

- For writing your own apps: if you use Swift, then the existing source is a perfect example. Note the use of the bridging header in the Headers folder. If you use Objective C, you should just be able to build directly on top of the SDK without need for the bridging header.

3. To use:

- Open the project in XCode. Apply your own provisioning so that you can build and run directly on a target IOS device that meets the system requirements. There are 10 generic fandecks included in the project, as CSV files with 1000 colors each, that can be used for testing. The first step is build, run, connect to a Caran and verify functionality. You should be able to discover, connect, view information about an existing Caran, take measurements, and download fandecks from the demo set.

- NOTE that if you download new internal fandeck content into Caran from the generic samples included with this SDK, the old fandeck content will be overwritten.

- The API to the Caran SDK library is provided through the main header file, DCICaranLib.h, which is found in the Headers folder in the project. This is meant to be a self-documenting file, with detailed descriptions of what each function does and how various functions are related.

In general:

The first set of functions either implement commands or expose internal information about the contents of the currently connected Caran. Issuing a command (such as connection; calibration; read/write OEM/Registration codes; download fandeck data) will initiate one or more commands and interactions between the SDK and the device itself. Time is needed to complete these interactions. While they are going on, the host (demo) app needs to listen for a response from the SDK, which will eventually call back into the host (demo) app through an associated protocol delegate function.

The second set of functions are protocols, all of which need to be implemented by the host (demo) app. These provide responses and, in some cases, return data to the host (demo) app based on which command was sent. For instance, after calling the “connectToDevice” function, there will be a corresponding callback from either didConnectToDevice or didFailToConnectToDevice.

Some specifics:

Measuring: The button press on the Caran itself will initiate measurement directly, and the SDK library detects this whenever a Caran is connected to the running host app. Button press on Caran will call back into the host (demo) app as soon as the SDK finishes communicating with the device and retrieving measurement data (and this callback provides all data associated with the measurement). In addition (new with the 1.0 SDK), there’s a takeMeasurement command which the host app can use to initiate a measurement as well.

Depending on the measurement mode (and there is an API to set this); the callback will either return Lab data only (and this is faster - less data to retrieve); or, Lab data plus the 3 best matches from the device (which takes longer as there’s more data to retrieve). There’s a separate callback for each kind of returned measurement, based on the mode. (default is Lab Plus Match).

Fandeck download is a more complex process. The SDK can accept CSV files that match the currently supported format (see the sample CSVs in the project as the definitive examples); and can also accept NSArrays of Fandeck objects (these objects are defined for reference by CRNFandeck.h and CRNFandeckcolor.h) You can create your own CRNFandeck objects by creating them programmatically in your own app and populating them with a list of CRNFandeckColors that you can also create programmatically. The downloadFandeckWithCSVs function is the first variation and downloadFandeckWithArray is the latter.

To set up fandeck downloading in your app: use the maxNumFandecks and maxNumColors to set limits on the maximum allowable fandecks and total allowable colors from all fandecks to combine and download into the device. You can call getFandeckDownloadStatsFromTotalColors:asTotalBytes:andTotalChunks to get back the total number of bytes that will be created in the SDK and then downloaded into the device (as well as the # of chunks for which the SDK will provide progress callbacks) to calculate an approximate download time (for a known starting data transfer rate).

In either case, once a download has started, the SDK calls back into the host (demo) app so that it can show progress; there are 3 different delegate functions that are used during the process; one to say downloading has started, another to provide ongoing progress, and a 3rd to handle completion.

While downloading is in progress, didFandeckDownloadForChunk:ofTotalChunks:withColorsTransferred:ofTotalColors is used to call back into the host (demo) app so that it cah show progress after each chunk is transferred. The host (demo) app can also see, from this information, how much data transfer has occurred and how much remains. The host (demo) app can then, by tracking time, see how long the last chunk took to download, and then from the # of remaining chunks, calculate and display a remaining time estimation to the user. By updating these calculations on the fly (and then saving a local copy of the per-chunk transfer time from “real world” data for the next app session), the host (demo) app can provide reasonably accurate times for estimated total download time (prior to starting) and then estimated remaining time (during the download). All of this is implemented in the Caran Demo App and it’s possible to see how to apply the same principles by looking at the source code.

For more details, please read the documentation in the .h files and see the usage of the SDK in the Demo App source code.

***************
Update History:
***************

- 12.12.16: Initial release (app version 0.6, SDK version 0.6)

- 7.28.17: Second release (app version 1.0, SDK version 1.1), in parallel with first release of the Android SDK

  - Updated comments about Measuring to reflect an addition since the original SDK. There is now an SDK command to initiate and take a measurement, in addition to the automatic recognition of the button press on the device. This can be wired to a “soft” measurement button in the app. See the updated Demo App as an example - there’s a Measure button now included at the bottom of the Measure view.

  - The API for calibration has a slight modification. A new “warning” flag is returned, to handle cases where calibration will not “fail”, but, may be far enough out of tolerance where we’d recommend warning the user to be sure that they have the device on the proper calibration tile surface.





