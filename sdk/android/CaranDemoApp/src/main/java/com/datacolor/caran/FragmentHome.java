//
//  FragmentHome.java
//  Caran Demo App
//
//  Copyright © 2017 Datacolor. All rights reserved.
//

package com.datacolor.caran;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class FragmentHome extends FragmentBase {
    interface FragmentHomeDelegate {
        void fragmentHomeDidRequestConnect(FragmentHome sender);
        void fragmentHomeDidRequestContinueDisconnected(FragmentHome sender);
    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_home;
    }


    @Override
    protected void init() {
        ((Button) getView().findViewById(R.id.home_button_connect)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getMainActivity().fragmentHomeDidRequestConnect((FragmentHome)self);
            }
        });

        ((Button) getView().findViewById(R.id.home_button_continue_disconnected)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getMainActivity().fragmentHomeDidRequestContinueDisconnected((FragmentHome)self);
            }
        });
    }
}