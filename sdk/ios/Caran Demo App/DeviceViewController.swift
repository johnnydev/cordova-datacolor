//
//  DeviceViewController.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 04/10/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import UIKit

class DeviceViewController: UIViewController {

    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: PROPERTIES
    // MARK:
    //--------------------------------------------------------------------------------
    @IBOutlet weak var deviceScrollView: UIScrollView!
    @IBOutlet weak var fandeckScrollView: UIScrollView!
    @IBOutlet weak var registrationScrollView: UIScrollView!
    
    @IBOutlet weak var fandeckNameRow: DeviceStatusRow!
    
    @IBOutlet weak var theStackView: UIStackView!
    @IBOutlet weak var switchMode: UISwitch!
    @IBOutlet weak var methodModeRow: DeviceStatusRow!
    @IBOutlet weak var sdkVersionRow: DeviceStatusRow!
    @IBOutlet weak var firmwareVersionRow: DeviceStatusRow!
    @IBOutlet weak var deviceNameRow: DeviceStatusRowWithButton!
    @IBOutlet weak var deviceSerialRow: DeviceStatusRow!
    @IBOutlet weak var deviceCalibrationRow: DeviceStatusRowWithButton!
    @IBOutlet weak var appVersionRow: DeviceStatusRow!
    @IBOutlet weak var fandeckCreationDateRow: DeviceStatusRow!
    @IBOutlet weak var fandeckIlluminantRow: DeviceStatusRow!
    @IBOutlet weak var fandeckCountRow: DeviceStatusRow!
    @IBOutlet weak var fandeckRecordsRow: DeviceStatusRow!
 
    @IBOutlet weak var downloadButtonView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var btnNewCodes: UIButton!
    @IBOutlet weak var oemRow: DeviceStatusRow!
    @IBOutlet weak var registrationRow: DeviceStatusRow!
    @IBOutlet weak var noCodesView: UIView!
    
    var alertText:UITextField!
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: OVERRIDES
    // MARK:
    //--------------------------------------------------------------------------------
    
    
    // Remove the observers
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        // get the registration codes
        noCodesView.isHidden = true
        CaranController.instance.getCodes()
        
        
        // hide the views
        fandeckScrollView.isHidden = true
        registrationScrollView.isHidden = true
        
        
        //
        self.tabBarController?.title = "Device"
        self.initDeviceStatusRows()
        
        // Receive uncalibrated event
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(refreshCalibrationStatus),
            name: NSNotification.Name(rawValue: "DeviceUncalibratedNotification"),
            object: nil)
        
        // Receive calibrated event
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(refreshCalibrationStatus),
            name: NSNotification.Name(rawValue: "DeviceCalibratedNotification"),
            object: nil)
        
        // Receive Get Codes event
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didGetCodes),
            name: NSNotification.Name(rawValue: "DidGetCodes"),
            object: nil)
        
 
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setDeviceStatusRows()
        self.tabBarController?.navigationItem.rightBarButtonItem = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBarController?.title = "Device"
    }
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: METHODS
    // MARK:
    //--------------------------------------------------------------------------------
    @IBAction func donwloadButtonPressed(_ sender: Any) {
        startDownload()
    }

    @IBAction func segmentedControlChanged(_ sender: Any) {
        if(segmentedControl?.selectedSegmentIndex == 0){
            self.deviceScrollView.isHidden = false
            self.fandeckScrollView.isHidden = true
            self.registrationScrollView.isHidden = true
            
        }
        if(segmentedControl?.selectedSegmentIndex == 1){
            self.deviceScrollView.isHidden = true
            self.fandeckScrollView.isHidden = false
            self.registrationScrollView.isHidden = true
        }
        if(segmentedControl?.selectedSegmentIndex == 2){
            self.deviceScrollView.isHidden = true
            self.fandeckScrollView.isHidden = true
            self.registrationScrollView.isHidden = false
        }
    }
    
    
    func startDownload(){
        let fandecksView = self.storyboard!.instantiateViewController(withIdentifier: "CSVFandecksListView") as! CSVFandecksListViewController
        fandecksView.fandecks = Fandeck.fandecksListFromLocalJson()
        
        // present the fandecks selection view screen
        self.present(fandecksView, animated: true, completion: nil)
        
    }
    
    @IBAction func switchModeChanged(_ sender: Any) {
        // todo change mode based on the switch state
        print("switchModeChanged: ")
        setModeRowText(isLabPlusMatch:switchMode.isOn)
        
        if(switchMode.isOn){
            print("to lab+match")
            CaranController.instance.setMeasurementModeAs(_mode: modeMeasureLabPlusMatch)
        }else{
            print("to lab only")
            CaranController.instance.setMeasurementModeAs(_mode: modeMeasureLabOnly)
        }
    }
    
    func setDeviceStatusRows() {
    
        // No device connected
        if(CaranController.instance.connectedDevice == nil) {
        
            let nodevice = "Not connected"
            
            // device name row
            deviceNameRow.valueText = nodevice
            deviceNameRow.showButton = false
            
            // device serial row
            deviceSerialRow.valueText = ""
            
            // calibration
            deviceCalibrationRow.valueText = ""
            deviceCalibrationRow.showButton = false
            deviceCalibrationRow.showStatusIndicator = false
            
            // Fandeck info
            fandeckCreationDateRow.valueText = ""
            fandeckIlluminantRow.valueText = ""
            fandeckCountRow.valueText = ""
            fandeckRecordsRow.valueText = ""
            
            fandeckNameRow.valueText = ""
            downloadButtonView.isHidden = true
          
            btnNewCodes.isHidden = true
        }
        
        // Device connected
        if(CaranController.instance.connectedDevice != nil) {
        
            
            // device name row
            deviceNameRow.valueText = (CaranController.instance.connectedDevice?.name)!
            deviceNameRow.showButton = true
            
            // device serial row
            deviceSerialRow.valueText = CaranController.instance.getDeviceSerial()
            
            // device calibration row
            deviceCalibrationRow.showButton = true
            deviceCalibrationRow.showStatusIndicator = true
            if(CaranController.instance.needsCalibration) {
                deviceCalibrationRow.valueText = "Needs calibration"
                deviceCalibrationRow.statusError()
            }
            else {
                deviceCalibrationRow.valueText = "Calibrated"
                deviceCalibrationRow.statusOK()
            }
            
            // Fandeck info
            // if fandeckRecords is -1 set mode to lab only and disable the switch
            if(CaranController.instance.getFandeckRecords() < 1){
                CaranController.instance.setMeasurementModeAs(_mode: modeMeasureLabOnly)
                switchMode.isEnabled = false
                fandeckCreationDateRow.valueText = ""
                fandeckIlluminantRow.valueText = ""
                fandeckCountRow.valueText = ""
                fandeckRecordsRow.valueText = ""
                
                fandeckNameRow.valueText = "Empty"

            }else{
                switchMode.isEnabled = true
                fandeckCreationDateRow.valueText = CaranController.instance.getFandeckCreationDate()
                fandeckIlluminantRow.valueText = CaranController.instance.getFandeckIlluminant()
                fandeckCountRow.valueText = "\(CaranController.instance.getFandeckCount())"
                fandeckRecordsRow.valueText = "\(CaranController.instance.getFandeckRecords())"
                fandeckNameRow.valueText = CaranController.instance.getFandeckNames().joined(separator: ", ")

            }
            
            downloadButtonView.isHidden = false
            btnNewCodes.isHidden = false
        }
        
        // app version row
        appVersionRow.valueText = ""
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            appVersionRow.valueText = version
        }
 
        // firmware version
        firmwareVersionRow.valueText = CaranController.instance.getFirwareVersion()

        // SDK version
        sdkVersionRow.valueText = CaranController.instance.getSdkVersion()
        
        // Mode
        switchMode.isOn = CaranController.instance.isModeLabPlusMatch()
        setModeRowText(isLabPlusMatch:switchMode.isOn)

    }
    
    func setModeRowText(isLabPlusMatch:Bool){
    
        if(isLabPlusMatch){
            methodModeRow.valueText = "Lab plus Match"
        }else{
            methodModeRow.valueText = "Lab only"
        }
        
    }
    
    
    func initDeviceStatusRows(){
    
        // calibration
        deviceCalibrationRow.button.setTitle("Calibrate", for: UIControlState.normal)
        deviceCalibrationRow.button.addTarget(self, action: #selector(calibrationButtonPressed), for: .touchUpInside)
        
        // name
        deviceNameRow.button.setTitle("Disconnect", for: UIControlState.normal)
        deviceNameRow.button.addTarget(self, action: #selector(disconnectButtonPressed), for: .touchUpInside)
        
        // registration
        oemRow.valueText = ""
        registrationRow.valueText = ""
    }
    
    func refreshCalibrationStatus(notification: NSNotification) {
        
       setDeviceStatusRows()        
    }
    
    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: HEADER READ/WRITE PROTOCOL
    // MARK:
    //--------------------------------------------------------------------------------
    @IBAction func btnRetryGetCodesPressed(_ sender: Any) {
        CaranController.instance.getCodes()
    }

 
    @IBAction func btnNewCodePressed(_ sender: Any) {
        
        //CaranController.instance.writeCodes(_oemCode: "AAA", _registrationCode: "BBB")
        //CaranController.instance.getCodes()
     
        let modalViewController = self.storyboard!.instantiateViewController(withIdentifier: "CodesView") as! CodesViewController
        modalViewController.modalPresentationStyle = .fullScreen
        
        modalViewController.oemCode = self.oemRow.valueText
        modalViewController.registrationCode = self.registrationRow.valueText
        
        self.present(modalViewController, animated: true, completion: nil)

        
    }
    
    func didGetCodes(notification: NSNotification) {
        
        // Get the parameter
        let userInfo = notification.userInfo as! [String: Any]
        let success = userInfo["success"] as! Bool?
        let oemCode = userInfo["oemCode"] as! String?
        let registrationCode = userInfo["registrationCode"] as! String?
        
        if(success!){
            print("didGetCodes: success")
            oemRow.valueText = oemCode!
            registrationRow.valueText = registrationCode!
            
            noCodesView.isHidden = true
        }
        else{
            print("didGetCodes: fail")
            oemRow.valueText = ""
            registrationRow.valueText = ""
            
            noCodesView.isHidden = false
            if(CaranController.instance.connectedDevice == nil){
                noCodesView.isHidden = true
            }
            

        }
    }
    
    
    

    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: BUTTONS CALLBACK FUNCTIONS
    // MARK:
    //--------------------------------------------------------------------------------
    
    func calibrationButtonPressed(sender: UIButton!){
    
        let modalViewController = self.storyboard!.instantiateViewController(withIdentifier: "CalibrationNavigationController")
        modalViewController.modalPresentationStyle = .fullScreen
        self.present(modalViewController, animated: true, completion: nil)
        
    }
    
    func disconnectButtonPressed(sender: UIButton!){
        CaranController.instance.disconnectFromDevice()
    }
    
}
