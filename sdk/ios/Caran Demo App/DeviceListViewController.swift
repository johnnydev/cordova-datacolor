//
//  DeviceListViewController.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 29/09/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import UIKit

class DeviceListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // Data model: These strings will be the data for the table view cells
    var devices: [CBPeripheral] = [];
    
    // don't forget to hook this up from the storyboard
    @IBOutlet var tableView: UITableView!
    
    // View that contains the activity loader
    @IBOutlet var loadingView: UITableView!
    
    @IBOutlet weak var loadingLabel: UILabel!

    @IBOutlet weak var btOFFView: UIView!
    
    // Remove the observers
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        print("DeviceListViewController did load");
        
        
        // hides the table view
        tableView.isHidden = true
        
        // This view controller itself will provide the delegate methods and row data for the table view.
        tableView.delegate = self
        tableView.dataSource = self

        
        // Subscribe to the CaranListOfVisibleDevicesNotification
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didReceiveListOfVisibleDevices),
            name: NSNotification.Name(rawValue: "CaranListOfVisibleDevicesNotification"),
            object: nil)
        
        // Subscribe to the CaranListOfVisibleDevicesNotification
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didReceiveDeviceConnected),
            name: NSNotification.Name(rawValue: "DeviceConnectedNotification"),
            object: nil)
     
        // Disconnect Notification
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didDisconnect),
            name: NSNotification.Name(rawValue: "DisconnectFromDeviceNotification"),
            object: nil)
        
        // Failure Notification
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didFailToConnect),
            name: NSNotification.Name(rawValue: "FailToConnectNotification"),
            object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: NAVIGATION
    // MARK:
    //--------------------------------------------------------------------------------
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        checkBTStatusAndDiscover()
    }
    
    
    @IBAction func CloseView(_ sender: AnyObject) {
        
        NotificationCenter.default.removeObserver(self)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnRetryPressed(_ sender: Any) {
        checkBTStatusAndDiscover()
    }
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: METHODS
    // MARK:
    //--------------------------------------------------------------------------------
    
    func checkBTStatusAndDiscover(){
        // check if the bt is on
        if(CaranController.instance.isBlueToothPoweredOn()){
            self.btOFFView.isHidden = true
            // search for visible devices
            CaranController.instance.searchForDevices()
            loadingLabel.text = "Discovering Devices"
        }
        else{
            self.btOFFView.isHidden = false
        }

    }
    
    
    func didReceiveListOfVisibleDevices(notification: NSNotification) {
        
        print("the DeviceListViewController was notified of the list of visible devices")
        
        // Get the parameter
        let userInfo = notification.userInfo as! [String: AnyObject]
        devices = userInfo["devices"] as! [CBPeripheral]
        
        // Print out the number of visible devices
        print("devices found: \(devices.count)")
        
        tableView.reloadData()

        
        if(devices.count < 1) {
            showAlertRetry()
        }
        else if(devices.count == 1){
            loadingLabel.text = "Connecting"
            self.loadingView.isHidden = false;
            CaranController.instance.connectToDevice(device: self.devices[0])
        }
        else{
            tableView.isHidden = false;
            loadingView.isHidden = true;
        }
        
        
    }
    
    
    func didReceiveDeviceConnected(notification: NSNotification) {
        
        print("The DeviceListViewController was notified of the connected instrument")
        
        // Get the parameter
        // let userInfo = notification.userInfo as! [String: Any]
        // var device = userInfo["device"] as! CBPeripheral?
        
        self.dismiss(animated: true, completion: nil)
        
    }

    
    func didFailToConnect(notification: NSNotification) {
        
        print("DeviceListViewController received FailToConnectNotification")

        
        // display an alert
        let alert = UIAlertController(title: "Connection failed", message:
            "Please try again", preferredStyle: .alert)
        
        self.present(alert, animated: false, completion: nil)
        
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertActionStyle.default,
                                      handler: {action in
                                       
                                       self.dismiss(animated: true, completion: nil)
                                        
                                    }))
        
    }
    
    
    
    func didDisconnect(notification: NSNotification) {
        
        print("DeviceListViewController was notified of the disconnection")
    
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // Retry connection / If no result is found
    func showAlertRetry()
    {
        let alert = UIAlertController(title: "No Caran devices found", message:
            "Be sure that your Caran is turned on", preferredStyle: .alert)
        
        self.present(alert, animated: false, completion: nil)
        
        alert.addAction(UIAlertAction(title: "Cancel",
                                      style: UIAlertActionStyle.default,
                                      handler: {action in
                                        // Handler code
                                        self.dismiss(animated: true, completion: nil)
                                       
        }))
        alert.addAction(UIAlertAction(title: "Retry", style: .default, handler:
            {action in
               // Handler code
               CaranController.instance.searchForDevices()
        }))
    }

    
    
    // MARK: TABLE PROTOCOLS IMPLEMENTATION
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.devices.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let reuseIdentifier : String = "mycell"
        
        // create a new cell if needed or reuse an old one
        var cell:UITableViewCell? =
            tableView.dequeueReusableCell(withIdentifier: reuseIdentifier)! as UITableViewCell
        
        if (cell != nil)
        {
            cell = UITableViewCell(style: UITableViewCellStyle.subtitle,
                                   reuseIdentifier: reuseIdentifier)
        }
        
        // set the text from the data model
        
        let device = self.devices[indexPath.row] as CBPeripheral;
        
        cell!.textLabel?.text = device.name
        cell!.detailTextLabel?.text = CaranController.instance.getDeviceSerialFromName(aName: device.name!) //CaranController.instance.getDeviceSerial()
        return cell!
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // show loading state
        loadingLabel.text = "Connecting"
        self.loadingView.isHidden = false;
        
        // connect to device
        CaranController.instance.connectToDevice(device: self.devices[indexPath.row])
        
    }
    
}
