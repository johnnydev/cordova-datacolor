//
//  HomeViewController.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 29/09/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: PROPERTIES
    // MARK:
    //--------------------------------------------------------------------------------
    
    private var shouldDisplayDisconnectionAlert = false
    private var loadingAlert : UIAlertController?
    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: OVERRIDES
    // MARK:
    //--------------------------------------------------------------------------------
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("HomeViewController did load");
        
  
        // Button Pushed Notification
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didPushTheButton),
            name: NSNotification.Name(rawValue: "ButtonPushedNotification"),
            object: nil)
        
        // Disconnect Notification
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didDisconnect),
            name: NSNotification.Name(rawValue: "DisconnectFromDeviceNotification"),
            object: nil)
        
        // Receive measure
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didReceiveMeasurement),
            name: NSNotification.Name(rawValue: "MeasureFromDeviceNotification"),
            object: nil)
        
        
        // Create the Caran View
        loadingAlert = UIAlertController(title: nil, message: "measuring", preferredStyle: .alert)
        loadingAlert!.view.tintColor = UIColor.gray
        let loadingIndicator: UIActivityIndicatorView = MeasuringAnimated(frame: CGRect(x: 10, y: 8, width: 44, height: 44)) as MeasuringAnimated
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        loadingIndicator.startAnimating()
        loadingAlert!.view.addSubview(loadingIndicator)
        
        // Initialize the CaranController
        CaranController.instance.initialize()
                       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
        if(shouldDisplayDisconnectionAlert)
        {
            // Show a notification
            let alertController = UIAlertController(title: "Warning", message: "Connection Interrupted", preferredStyle: .alert)
            self.present(alertController, animated: true, completion: nil)
            
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                alertController.dismiss(animated: true, completion: nil)
            }))

            shouldDisplayDisconnectionAlert = false
            
        }
        
    }
    // Remove the observers
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: NAVIGATION
    // MARK:
    //--------------------------------------------------------------------------------
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated);
        
        self.navigationController!.setNavigationBarHidden(true, animated: true)
        
        // Check if a device is connected
        if(CaranController.instance.connectedDevice != nil){
            self.navigationController?.pushViewController(self.storyboard!.instantiateViewController(withIdentifier: "tabVC"), animated: false)
        }
        
    }

    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: METHODS
    // MARK:
    //--------------------------------------------------------------------------------
 
    func didReceiveMeasurement(notification: NSNotification) {
        
        print("HomeViewController was notified of a measurement")
        
        // hide alert
        if(loadingAlertIsVisible){
            loadingAlert!.dismiss(animated: false, completion: nil)
            loadingAlertIsVisible = false
        }
        
    }
    
    private var loadingAlertIsVisible : Bool = false
    func didPushTheButton(notification: NSNotification) {
    
        print("HomeViewController was notified of the button pushed")
    
        // if alert already visible do nothing
        if(loadingAlertIsVisible){
            return
        }
        
        // loops to get the top view controller
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            // presents the view on the top controller
            topController.present(loadingAlert!, animated: true, completion: nil)
            loadingAlertIsVisible = true;
        }
    }
    
    
    func didDisconnect(notification: NSNotification) {
        
        print("HomeViewController was notified of the disconnection")
        
        // hide alert
        if(loadingAlertIsVisible){
            loadingAlert!.dismiss(animated: false, completion: nil)
            loadingAlertIsVisible = false
        }
        
        if(CaranController.instance.connectedDevice == nil)
        {
            _ = self.navigationController?.popToRootViewController(animated: true)
            
            if(!CaranController.instance.wasManuallyDisconnected)
            {
                
                shouldDisplayDisconnectionAlert = true
                DCUtils.instance.sendLocalNotificationForDisconnection()
                
            }
            
            CaranController.instance.wasManuallyDisconnected = false;
            
        }
        
        
        
    }
    
}
