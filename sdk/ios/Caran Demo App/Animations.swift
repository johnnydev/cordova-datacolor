//
//  Animations.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 06/10/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import Foundation
import UIKit

class Animations {

    
    class func ZoomIn(viewToAnimate : UIView  ) {
    
        let currentTransform = viewToAnimate.transform
        
        UIView.animate(
            withDuration: 0.0,
            animations: {
                viewToAnimate.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            },
            completion: { finish in
                UIView.animate(withDuration: 0.1){
                viewToAnimate.transform = currentTransform
            }
        })
    }
}
