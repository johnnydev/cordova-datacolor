
class DCICaranLib {

//--------------------------------------------------------------------------------
//
// Caran SDK Public Structs, Methods, etc.
//
//--------------------------------------------------------------------------------

static class FandeckMatch {     byte name[];     // Fandeck record name of match as a null terminated C string     byte fdname[];   // Fandeck name of match as a null terminated C string     float l;                    	 // Fandeck Lab of the match     float a;     float b;     float deltaE;               	 // dE of the match vs. the measured color.
}


// Used to set and get the measurement mode. enum CrnMeasurementMode {     modeMeasureLabOnly, modeMeasureLabPlusMatch } 
//--------------------------------------------------------------------------------
//
// getListOfVisibleDevices
//
// The host app calls this to start the Bluetooth discovery process. When this is
// complete, the host app's didReceiveListOfVisibileDevices method is called, to
// provide a BluetoothDevice array of the found results.
//
//--------------------------------------------------------------------------------
public void getListOfVisibleDevices()


//--------------------------------------------------------------------------------
//
// connectToDevice
//
// Host app calls this to connect to a BluetoothDevice selected from the array returned
// by didReceiveListOfVisibileDevices. When the connection attempt is complete:
//
// On PASS, didConnectToDevice will be called. The host app can move on to its
// connected UI; other SDK functions which follow will be usable. When the user
// presses the Caran button, didGetButtonPress and then one of the didReceiveMeasurement
// delegate handlers will be called to notify the host app about the button press and then
// subsequently give it the measured value and associated data. Etc.
//
// On FAIL, didFailToConnectToDevice will be called. (This is an unexpected error
// and in normal operation, should not happen).
//
//
//--------------------------------------------------------------------------------
public void connectToDevice(BluetoothDevice _device)


//--------------------------------------------------------------------------------
//
// retrieveConnectedDevice
//
// Returns the currently connected BluetoothDevice device (if any). Returns nil
// if no Caran is connected. (info only, not currently used in the demo app and
// not required at this point).
//
//--------------------------------------------------------------------------------
public BluetoothDevice retrieveConnectedDevice()


//--------------------------------------------------------------------------------
//
// SDKRev
//
// Returns the SDK revision.
//
//--------------------------------------------------------------------------------
public String SDKRev()


//--------------------------------------------------------------------------------
//
// firmwareRev
//
// Returns the firmware revision of the currently connected device.
//
//--------------------------------------------------------------------------------
public String firmwareRev() 


//--------------------------------------------------------------------------------
//
// maxNumFandecks
//
// Returns the maximum number of internal fandecks supported by the device.
//
//--------------------------------------------------------------------------------
public int maxNumFandecks()


//--------------------------------------------------------------------------------
//
// maxNumColors
//
// Returns the maximum number of internal colors supported by the device.
//
//--------------------------------------------------------------------------------
public int maxNumColors()


//--------------------------------------------------------------------------------
//
// Fandeck Information
//
// These functions return details about the currently loaded aggregate fandeck in the
// connected device.
//
//--------------------------------------------------------------------------------
public String fandeckCreationDate;   			// Creation date
public String fandeckIlluminant;     			// Illuminant
public int fandeckCount;           			// Number of fandecks combined into the internal fandeck
public ArrayList<String> fandeckNames;         	// Array of Strings of the internal fandeck names.
public ArrayList<String> fandeckGUIDs;         	// Array of Strings of the internal fandeck GUIDs.
public ArrayList<int> fandeckVersions;			// Array of ints of the internal fandeck versions.
public ArrayList<String> fandeckRecords;		// Total # of fandeck records in the internal fandeck


//--------------------------------------------------------------------------------
//
// getFandeckDownloadStatsFromTotalColorsAndBytesAndChunks
//
// Host app can call this from its screen that lets the user select fandecks
// for aggregation and download. Given the total # of colors, this will return
// the total size of the binary and total # of chunks that will be downloaded
// from this selection. If the host app knows the approximate transfer time per
// chunk, it can calculate an approximate download time for the selection and report
// this to the user.
//
//--------------------------------------------------------------------------------
public void getFandeckDownloadStatsFromTotalColorsAndBytesAndChunks(int numColors, int[] totalSize, int[] totalChunks)


//--------------------------------------------------------------------------------
//
// Fandeck Download from multiple CSV files. SDK will internally create
// the final binary from these CSVs and proceed with the download, and the protocol
// functions below will be called (see descriptions below):
//
// didStartFandeckDownloadForTotalChunks, (when downloading starts)
// didFandeckDownloadForChunk, (for progress during download)
// didCompleteFandeckDownloadWithBytesTransferred (when finished or aborted)
//
//--------------------------------------------------------------------------------
public void downloadFandeckWithCSVs(ArrayList theCSVs)


//--------------------------------------------------------------------------------
//
// Fandeck Download, each object in the array should be a FanDeck rather than CSV.
// Otherwise, the same as the above.
//
//--------------------------------------------------------------------------------
public void downloadFandeckWithArray(ArrayList<CRNFandeck> theFandecks)


//--------------------------------------------------------------------------------
//
// needsCalibration
//
// If true, then Caran should be calibrated on the white tile based on internal
// criteria as determined by the SDK. This is a state flag that can be tested
// at any point while a device is connected, however, see the note below about
// "when" the SDK updates this state flag internally.
//
// NOTE: The SDK determines calibration state (a) when a device is connected,
// immediately following connection and just prior to didConnectToDevice being called,
// and (b) after a measurement has completed, and just prior a didReceiveMeasurement
// delegate being called. Between measurements, while Caran is awake and connected, it's
// "possible" that the calibration state limit based on time may have been exceeded;
// this will be picked up on the next measurement and/or reconnection/session.
//
//--------------------------------------------------------------------------------
public boolean needsCalibration()


//--------------------------------------------------------------------------------
//
// isBluetoothPoweredOn
//
// If true, then yes.
// If false, then no. :-)
//
// If false, then the SDK won't be able to discover and connect to devices. Call this
// after the SDK is initialized to find out whether discovery and connection are
// possible; can notify the user and tell them to turn on BlueTooth if false.
//
//--------------------------------------------------------------------------------
public boolean isBluetoothPoweredOn()


//--------------------------------------------------------------------------------
//
// setMeasurementModeAs
//
// Sets the measurement mode (default is modeMeasureLabPlusMatch). Behavior:
//
// modeMeasureLabPlusMatch: Caran button presses will return measurement data through
// didReceiveMeasurement(double l, double a, double b, FandeckMatch m1, FandeckMatch m2, FandeckMatch m3),
// so the matches plus the Lab measurement will be returned. The process takes longer
// and it's advisable to display a progress indicator between didGetButtonPress
// and didReceiveMeasurement(double l, double a, double b).
//
// modeMeasureLabOnly: Caran button presses will return measurement data
// through didReceiveMeasurement(double l, double a, double b), so only the Lab measurement will
// be returned. The process is much faster and it may not be necessary to display
// a progress indicator between didGetButtonPress and didReceiveMeasurement(double l, double a, double b)
//
//--------------------------------------------------------------------------------
public void setMeasurementModeAs(CrnMeasurementMode theMode)


//--------------------------------------------------------------------------------
//
// getMeasurementMode
//
//--------------------------------------------------------------------------------
public CrnMeasurementMode getMeasurementMode()


//--------------------------------------------------------------------------------
//
// getCodes
//
// Tell the SDK to get the OEM and Registration code strings from Caran, maximum 16
// characters each.
//
// On completion, the results will come back from protocol method
// didGetOEMAndRegistrationCodes
//
//--------------------------------------------------------------------------------
public void getCodes()


//--------------------------------------------------------------------------------
//
// setCodesWithOEMAndRegistrationCodes
//
// Write OEM and Registration Code strings into Caran, maximum 16 characters each.
// (If longer, they will be truncated).
//
// On completion, pass/fail for writing the codes will come back from protocol method
// didSetCodesWithSuccess
//
//--------------------------------------------------------------------------------
public void setCodesWithOEMAndRegistrationCodes(String oemCode, String registrationCode)


//--------------------------------------------------------------------------------
//
// calibrate
//
// The host app calls this command to perform a calibration procedure on the white tile.
// Prior to calling this method, the host app should tell the user to put the Caran on
// the white tile, with description/pictures etc. and NOT to push the button on the Caran.
//
// When calibration is complete, it will either pass or fail.
//
// On PASS, didBecomeCalibrated will be called (success!) The host app can congratulate
// the user on success, etc. and continue.
//
// On FAIL, didFailCalibration will be called. The host app can inform the user and then
// let them try again (call the calibrate function again etc. and repeat) or continue
// without calibrating.
//
//--------------------------------------------------------------------------------
public void calibrate()


//--------------------------------------------------------------------------------
//
// takeMeasurement
//
// Use this command to take a measurement, based on touching a button or other
// UI element in the host application. Results will come back through one of the
// "didReceiveMeasurement" protocols based on the measurement mode.
//
//--------------------------------------------------------------------------------
public void takeMeasurement()

//--------------------------------------------------------------------------------
//
// disconnectFromDevice
//
// The host app calls this to disconnect from Caran.
//
//--------------------------------------------------------------------------------
public void disconnectFromDevice()


//--------------------------------------------------------------------------------
//
// Caran SDK protocol
//
//--------------------------------------------------------------------------------

static interface CaranProtocol {

//--------------------------------------------------------------------------------
//
// didReceiveListOfVisibileDevices
//
// Following the host app's call to getListOfVisibleDevices, this is called when
// Bluetooth discovery is complete, and peripheralList is filled with any/all
// Carans that were found. The host app should use the array to display a list for the
// user to choose from.
//
//--------------------------------------------------------------------------------
void didReceiveListOfVisibleDevices(ArrayList<BluetoothDevice> peripheralList);


//--------------------------------------------------------------------------------
//
// didConnectToDevice
//
// Following the host app's call to connectToDevice, this is called when the connection
// attempt is successful. (Note: The SDK may run one or more commands to the device after
// the connection is established, and didConnectToDevice is not called until after
// these command(s) have completed, and both the device and SDK itself are ready to go).
//
//--------------------------------------------------------------------------------
void didConnectToDevice(BluetoothDevice _device);


//--------------------------------------------------------------------------------
//
// didFailToConnectToDevice:
//
// Following the host app's call to connectToDevice, this is called when the connection
// attempt failed. (Atypical, should not happen).
//
//--------------------------------------------------------------------------------
void didFailToConnectToDevice(BluetoothDevice _device);


//--------------------------------------------------------------------------------
//
// didDisconnectFromDevice
//
// Following the host app's call to disconnectFromDevice, this is called when
// Caran's internal Bluetooth component state changes to disconnected. It's also
// called if the device was connected and the connection is broken for other
// reasons (Caran goes to sleep; goes out of Bluetooth range; etc).
//
//--------------------------------------------------------------------------------
void didDisconnectFromDevice();


//--------------------------------------------------------------------------------
//
// This is called whenever the Caran button press is detected. Can be used
// to immediately show an indication or progress wheel, etc in the host app while
// waiting for the measurement data to be retrieved (didReceiveMeasurement...)
//
//--------------------------------------------------------------------------------
void didGetButtonPress();


//--------------------------------------------------------------------------------
//
// didReceiveMeasurement(double l, double a, double b)
//
// (NOTE: this is used when the measurement mode is modeMeasureLabOnly)
//
// Called after a measurement has occurred, to return measurement data to the host.
// This is triggered automatically by the user pressing the Caran's button while the
// Caran is awake and in a connected state.
//
// This returns only the measured Lab value.
//
// NOTE: There will be a SHORT time gap between the button press and when the measurement
// data has been retrieved from the device. The host app can detect the start of
// the measurement sequence through didGetButtonPress (immediately when the Caran
// button is pressed) and then display indeterminate progress until this method is called).
//
//--------------------------------------------------------------------------------
void didReceiveMeasurement(double l, double a, double b);


//--------------------------------------------------------------------------------
//
// didReceiveMeasurement(double l, double a, double b, FandeckMatch m1, FandeckMatch m2, FandeckMatch m3)
//
// (NOTE: this is used when the measurement mode is modeMeasureLabPlusMatch)
//
// Called after a measurement has occurred, to return measurement data to the host.
// This is triggered automatically by the user pressing the Caran's button while the
// Caran is awake and in a connected state.
//
// This returns the measured Lab value as well as the best 3 matches from the
// internal fandeck.
//
// NOTE: There will be LONGER time gap between button press and when the measurement
// plus match data is retrieved from the device. The host app can detect the start of
// the measurement sequence through didGetButtonPress (immediately when the Caran
// button is pressed) and then display indeterminate progress until this method is called).
//
//--------------------------------------------------------------------------------
void didReceiveMeasurement(double l, double a, double b, FandeckMatch m1, FandeckMatch m2, FandeckMatch m3);


//--------------------------------------------------------------------------------
//
// didBecomeUncalibrated
//
// Called during this host app connection session if the device needs calibration.
//
// After each measurement, the calibration state of the connection session is checked,
// and if the internal thresholds have been exceeded, this method is called to notify
// the host app. (after doing this once, it won't be sent again during that connection
// session unless the user calibrates and then exceeds the threshold again in that session).
//
//--------------------------------------------------------------------------------
void didBecomeUncalibrated();


//--------------------------------------------------------------------------------
//
// didPassCalibrationWithWarning
//
// This is called, following a call to calibrate, if calibration was successful.
//
// For SDK 1.1, the API name has changed (it had been, didPassCalibration with no parameters), and
// the new flag "warning" is now returned as an additional qualifier for what "pass" means.
// If warning = false, then the white tile measurement during calibration was below a certain minimum
// threshold for which there's no warning. If warning = true, calibration has still "passed", but
// white tile measurement during calibration is in a range, far enough away from the factory stored
// value, where the user should be warned (for instance: "Are you sure that you've calibrated on your
// white calibration tile?".
//
//--------------------------------------------------------------------------------
void didPassCalibrationWithWarning(boolean warning);


//--------------------------------------------------------------------------------
//
// didFailCalibration
//
// This is called, following a call to calibrate, if calibration failed.
//
//--------------------------------------------------------------------------------
void didFailCalibration();


//--------------------------------------------------------------------------------
//
// This is called once, just before internal fandeck downloading starts. totalChunks here
// matches totalChunks that will be sent for each didFandeckDownloadForChunk
// (host app can use this to set scaling for a progress indicator to be filled by
// those calls.
//
//--------------------------------------------------------------------------------
void didStartFandeckDownloadForTotalChunks(int totalChunks);


//--------------------------------------------------------------------------------
//
// This is called repeatedly, during internal fandeck download, so the host app can show progress.
//
//--------------------------------------------------------------------------------
void didFandeckDownloadForChunk(int thisChunk, int totalChunks, int numTransferedColors, int totalColors);


//--------------------------------------------------------------------------------
//
// This is called when the internal fandeck download is complete, with pass/fail.
//
// If PASS, totalChunks of fandeck data will have been successfully downloaded.
// If FAIL, (can be called earlier in the process, without all progress indication
// being done), then something went wrong partway through and the process should be
// started again. Internal state of the device would be incomplete.
//
// Note: After a fandeck dowload completion, the headar info that's exposed will have
// also been updated. The host app should re-get its copy of the header info to bring
// itself up-to-date on the internal state of the device fandeck.
//
//--------------------------------------------------------------------------------
void didCompleteFandeckDownloadWithBytesTransferred(long totalBytesTransferred, boolean success);


//--------------------------------------------------------------------------------
//
// This is called following getCodes.
//
// (There's a slight delay while data is read from the device).
// The host app should wait for this response before updating its UI.
//
// If success = YES, the codes were successfully retrieved and can be shown
// to the user; if NO, the host app can tell the user there was an error retrieving
// the codes and handle the failure as it chooses.
//
// For a factory initialized device which has never had either code written to it,
// the returned NSString's will be the empty string "". The same will be true if there's
// failure to read.
//
//--------------------------------------------------------------------------------
void didGetOEMAndRegistrationCodes(String oemCode, String registrationCode, boolean success);


//--------------------------------------------------------------------------------
//
// This is called following setCodesWithOEMAndRegistrationCodes.
//
// (There's a slight delay while data is written to the device).
// The host app should wait for this response before updating its UI.
//
// If success = YES, it can show a message saying the OEM and Registration
// codes were successfully written; if NO, it could tell the user that the attempt to
// save the codes into the device failed, please try again, etc.
//
// For verification: the host app could then call getCodes again to re-read the
// two codes from the device and display them.
//
//--------------------------------------------------------------------------------
public void didSetCodesWithSuccess(boolean success);

} // CaranProtocol

} // DCICaranLib

