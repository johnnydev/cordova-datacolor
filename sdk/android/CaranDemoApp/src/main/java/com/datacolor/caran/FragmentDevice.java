//
//  FragmentDevice.java
//  Caran Demo App
//
//  Copyright © 2017 Datacolor. All rights reserved.
//

package com.datacolor.caran;


import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.datacolor.DCICaranLib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class FragmentDevice extends FragmentBase {
    interface FragmentDeviceDelegate {
        void fragmentDeviceDidRequestBack(FragmentDevice sender);
        String fragmentDeviceOemCode(FragmentDevice sender);
        String fragmentDeviceRegistrationCode(FragmentDevice sender);
        void fragmentDeviceDidRequestCalibrate(FragmentDevice sender);
        void fragmentDeviceDidRequestFandeckDownload(FragmentDevice sender);
        void fragmentDeviceDidRequestModifyCodes(FragmentDevice sender);
    }

    private enum ActiveSegment {ActiveSegmentInfo, ActiveSegmentFandeck, ActiveSegmentRegistration};
    ActiveSegment activeSegment;

    private boolean shouldSelectInfoSegmentOnReload = false;

    private ListView infoListView = null;

    private static class InfoItem {
        public enum AccessoryType {AccessoryTypeNone, AccessoryTypeButton, AccessoryTypeSwitch};

        public String title;
        public String value;
        public AccessoryType accessoryType;
        public String accessoryText;
        public int accessoryTag;
        public boolean badgeVisible;
 
        public InfoItem(String title, String value, AccessoryType accessoryType, String accessoryText, int accessoryTag, boolean badgeVisible) {
            this.title = title;
            this.value = value;
            this.accessoryType = accessoryType;
            this.accessoryText = accessoryText;
            this.accessoryTag = accessoryTag;
            this.badgeVisible = badgeVisible;
        }

        public InfoItem(String title, String value, AccessoryType accessoryType, String accessoryText, int accessoryTag) {
            this(title, value, accessoryType, accessoryText, accessoryTag, false);
        }

        public InfoItem(String title, String value) {
            this(title, value, AccessoryType.AccessoryTypeNone, null, 0, false);
        }
    }

    private class ItemsAdapter extends ArrayAdapter<InfoItem> {
        public View view;
        public ItemsAdapter(Context context, List<InfoItem> objects) {
            super(context, R.layout.list_item_device_info, objects);
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup parent) {
            this.view = convertView;
            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.list_item_device_info, null);
            }

            final InfoItem item = getItem(pos);

            if (item.badgeVisible) {
                view.findViewById(R.id.badge).setVisibility(View.VISIBLE);
            } else {
                view.findViewById(R.id.badge).setVisibility(View.INVISIBLE);
            }

            ((TextView) (view.findViewById(R.id.title))).setText(item.title);
            ((TextView) (view.findViewById(R.id.value))).setText(item.value);

            ViewGroup accessoryHolder = (ViewGroup) view.findViewById(R.id.accessory);
            accessoryHolder.removeAllViews();
            switch (item.accessoryType) {
                case AccessoryTypeNone: {
                } break;
                case AccessoryTypeButton: {
                    View accessory = getLayoutInflater().inflate(R.layout.list_item_accessory_button, accessoryHolder);
                    Button button = ((Button) accessory.findViewById(R.id.button));
                    button.setText(item.accessoryText);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            actionAccessoryButtonTouchedForItem(item);
                        }
                    });
                } break;
                case AccessoryTypeSwitch: {
                    View accessory = getLayoutInflater().inflate(R.layout.list_item_accessory_switch, accessoryHolder);
                    Switch aSwitch = ((Switch) accessory.findViewById(R.id.aswitch));
                    aSwitch.setChecked((item.accessoryText.equals("on")));
                    aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        actionAccessorySwitchStateChangedForItem(item, buttonView);
                        }
                    });
                } break;
            }

            return view;
        }
    }

    private void actionAccessoryButtonTouchedForItem(InfoItem item) {
        switch (item.accessoryTag) {
            case ACCESSORY_TAG_DISCONNECT: {
                getDciCaranLib().disconnectFromDevice();
            } break;
            case ACCESSORY_TAG_CALIBRATE: {
                getMainActivity().fragmentDeviceDidRequestCalibrate((FragmentDevice) self);
            } break;
        }
    }


    private void actionAccessorySwitchStateChangedForItem(InfoItem item, CompoundButton buttonView) {
        if (getDciCaranLib().fandeckRecords() < 1) {
            buttonView.setChecked(false);
        } else {
            DCICaranLib.CrnMeasurementMode mode = buttonView.isChecked() ? DCICaranLib.CrnMeasurementMode.modeMeasureLabPlusMatch : DCICaranLib.CrnMeasurementMode.modeMeasureLabOnly;
            getDciCaranLib().setMeasurementModeAs(mode);
            this.reload();
        }
    }


    private ArrayList<InfoItem> itemsList = null;
    private ItemsAdapter itemsAdapter = null;
    private View footerView = null;

    private final int ACCESSORY_TAG_DISCONNECT = 1;
    private final int ACCESSORY_TAG_CALIBRATE = 2;
    private final int ACCESSORY_TAG_MEASURE_MODE = 3;


    private void refreshSegmentedButtonColors(Button button, boolean isOn) {
        Resources res = getMainActivity().getResources();
        if (isOn) {
            button.setTextColor(res.getColor(R.color.colorSegmentedControlTextHighlighted));
            button.setBackgroundColor(res.getColor(R.color.colorSegmentedControlBackgroundHighlighted));
        } else {
            button.setTextColor(res.getColor(R.color.colorSegmentedControlText));
            button.setBackgroundColor(res.getColor(R.color.colorSegmentedControlBackground));
        }
    }


    private void setupSegmentedControl() {
        final Button buttonInfo = (Button) getView().findViewById(R.id.toggleButtonInfo);
        final Button buttonFandeck = (Button) getView().findViewById(R.id.toggleButtonFandeck);
        final Button buttonRegistration = (Button) getView().findViewById(R.id.toggleButtonRegistration);

        activeSegment = ActiveSegment.ActiveSegmentInfo;
        refreshSegmentedButtonColors(buttonInfo, true);
        refreshSegmentedButtonColors(buttonFandeck, false);
        refreshSegmentedButtonColors(buttonRegistration, false);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == buttonInfo) {
                    activeSegment = ActiveSegment.ActiveSegmentInfo;
                    refreshSegmentedButtonColors(buttonInfo, true);
                    refreshSegmentedButtonColors(buttonFandeck, false);
                    refreshSegmentedButtonColors(buttonRegistration, false);
                } else if (v == buttonFandeck) {
                    activeSegment = ActiveSegment.ActiveSegmentFandeck;
                    refreshSegmentedButtonColors(buttonInfo, false);
                    refreshSegmentedButtonColors(buttonFandeck, true);
                    refreshSegmentedButtonColors(buttonRegistration, false);
                } else if (v == buttonRegistration) {
                    activeSegment = ActiveSegment.ActiveSegmentRegistration;
                    refreshSegmentedButtonColors(buttonInfo, false);
                    refreshSegmentedButtonColors(buttonFandeck, false);
                    refreshSegmentedButtonColors(buttonRegistration, true);
                }

                self.reload();
            }
        };

        buttonInfo.setOnClickListener(listener);
        buttonFandeck.setOnClickListener(listener);
        buttonRegistration.setOnClickListener(listener);
    }


    private void setFooterStrings(String buttonText, String description) {
        if (footerView != null) {
            ((Button) footerView.findViewById(R.id.button)).setText(buttonText);
            ((TextView) footerView.findViewById(R.id.description)).setText(description);
        }
    }


    private void footerButtonTouched() {
        switch (activeSegment) {
            case ActiveSegmentFandeck: {
                shouldSelectInfoSegmentOnReload = true;
                getMainActivity().fragmentDeviceDidRequestFandeckDownload(this);
            } break;

            case ActiveSegmentRegistration: {
                getMainActivity().fragmentDeviceDidRequestModifyCodes(this);
            } break;
        }
    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_device;
    }


    @Override
    protected void init() {
        if (getMainActivity().isConnectedToDevice() == false) {
            this.enableBackButton();
        }
        this.setTitle("Device");

        this.setupSegmentedControl();

        infoListView = (ListView) getView().findViewById(R.id.list_view);

        itemsList = new ArrayList<InfoItem>();
        itemsAdapter = new ItemsAdapter(getMainActivity(), itemsList);

        infoListView.setAdapter(itemsAdapter);

        footerView = getLayoutInflater().inflate(R.layout.button_footer, null, false);
        infoListView.addFooterView(footerView);
        ((Button) footerView.findViewById(R.id.button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footerButtonTouched();
            }
        });
    }


    @Override
    public void reload() {
        itemsList.clear();

        if (shouldSelectInfoSegmentOnReload) {
            shouldSelectInfoSegmentOnReload = false;
            activeSegment = ActiveSegment.ActiveSegmentInfo;
            refreshSegmentedButtonColors((Button) getView().findViewById(R.id.toggleButtonInfo), true);
            refreshSegmentedButtonColors((Button) getView().findViewById(R.id.toggleButtonFandeck), false);
            refreshSegmentedButtonColors((Button) getView().findViewById(R.id.toggleButtonRegistration), false);
        }

        DCICaranLib dciCaranLib = getDciCaranLib();

        switch (activeSegment) {
            case ActiveSegmentInfo: {
                BluetoothDevice connectedDevice = dciCaranLib.retrieveConnectedDevice();

                if (connectedDevice != null) {
                    String name = connectedDevice.getName();
                    itemsList.add(new InfoItem("NAME", name, InfoItem.AccessoryType.AccessoryTypeButton, "Disconnect", ACCESSORY_TAG_DISCONNECT));

                    String serial = null;
                    if (name.toUpperCase().contains("DC_CARAN_"))
                        serial = name.replaceAll("(?i)"+"DC_CARAN_", "");;
                    itemsList.add(new InfoItem("SERIAL NUMBER", serial));

                    String calibrationTitle = "Calibrated";
                    boolean calibrationBadgeVisible = false;
                    if (dciCaranLib.needsCalibration()) {
                        calibrationTitle = "Needs calibration";
                        calibrationBadgeVisible = true;
                    }

                    itemsList.add(new InfoItem("CALIBRATION STATUS", calibrationTitle, InfoItem.AccessoryType.AccessoryTypeButton, "Calibrate", ACCESSORY_TAG_CALIBRATE, calibrationBadgeVisible));

                    int fandeckRecords = dciCaranLib.fandeckRecords();
                    if (fandeckRecords < 1)
                        dciCaranLib.setMeasurementModeAs(DCICaranLib.CrnMeasurementMode.modeMeasureLabOnly);

                    if (dciCaranLib.getMeasurementMode() == DCICaranLib.CrnMeasurementMode.modeMeasureLabOnly)
                        itemsList.add(new InfoItem("MEASURE MODE", "Lab only", InfoItem.AccessoryType.AccessoryTypeSwitch, "off", ACCESSORY_TAG_MEASURE_MODE));
                    else
                        itemsList.add(new InfoItem("MEASURE MODE", "Lab plus Match", InfoItem.AccessoryType.AccessoryTypeSwitch, "on", ACCESSORY_TAG_MEASURE_MODE));
                }

                itemsList.add(new InfoItem("APP VERSION", getMainActivity().getVersionName()));
                itemsList.add(new InfoItem("SDK VERSION", dciCaranLib.SDKRev()));
                itemsList.add(new InfoItem("FIRMWARE VERSION", dciCaranLib.firmwareRev()));

                footerView.setVisibility(View.GONE);
            } break;

            case ActiveSegmentFandeck: {
                int fandeckRecords = dciCaranLib.fandeckRecords();
                if (fandeckRecords < 1) {
                    itemsList.add(new InfoItem("CURRENT INTERNAL FANDECK NAME(S)", "Empty"));
                    itemsList.add(new InfoItem("CREATION DATE", ""));
                    itemsList.add(new InfoItem("FANDECK COUNT", ""));
                    itemsList.add(new InfoItem("FANDECK RECORDS", ""));
                    itemsList.add(new InfoItem("ILLUMINANT", ""));
                } else {
                    itemsList.add(new InfoItem("CURRENT INTERNAL FANDECK NAME(S)", TextUtils.join(", ", dciCaranLib.fandeckNames())));
                    itemsList.add(new InfoItem("CREATION DATE", dciCaranLib.fandeckCreationDate()));
                    itemsList.add(new InfoItem("FANDECK COUNT", Integer.toString(dciCaranLib.fandeckCount())));
                    itemsList.add(new InfoItem("FANDECK RECORDS", Integer.toString(fandeckRecords)));
                    itemsList.add(new InfoItem("ILLUMINANT", dciCaranLib.fandeckIlluminant()));
                }

                setFooterStrings("Download", "Replace internal fandecks with new content");
                footerView.setVisibility(View.VISIBLE);
            } break;

            case ActiveSegmentRegistration: {
                itemsList.add(new InfoItem("OEM CODE", getMainActivity().fragmentDeviceOemCode(this)));
                itemsList.add(new InfoItem("REGISTRATION CODE", getMainActivity().fragmentDeviceRegistrationCode(this)));

                setFooterStrings("Modify Codes", "");
                footerView.setVisibility(View.VISIBLE);
            } break;
        }

        if (getMainActivity().isConnectedToDevice() == false) {
            footerView.setVisibility(View.GONE);
        }

        infoListView.invalidateViews();
    }


    @Override
    protected void navigateBack() {
        getMainActivity().fragmentDeviceDidRequestBack(this);
    }
}