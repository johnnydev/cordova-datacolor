//
//  Fandeck.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 11/11/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import Foundation

class Fandeck{

    private(set) var name:String
    private(set) var id:String
    private(set) var version:String
    private(set) var device:String
    private(set) var colors_number:Int
    private(set) var filename:String
 
    init(aName:String, aID:String, aVersion:String, aDevice:String, aNumber:Int, aFilename:String) {
        
        name = aName
        id = aID
        version = aVersion
        device = aDevice
        colors_number = aNumber
        filename = aFilename
        
    }
 
    static func fandecksListFromLocalJson()->[Fandeck]{
    
        var fandecksList = [Fandeck]()
        
        // get the json data from a local file
        let path = Bundle.main
        let pathForFile = path.path(forResource: "fandecks", ofType: "json")
        
        var fileData:Data?
        
        do {
            fileData = try Data(contentsOf: URL(fileURLWithPath: pathForFile!))
            print("Get fandeck json file:" + (fileData?.count.description)!)
        } catch {
            print("Unable to load fandeck json file")
        }
        
        if(fileData != nil){
    
            
            do {
                let jsonSerialized = try JSONSerialization.jsonObject(with: fileData!, options: .allowFragments) as! [String:AnyObject]
                if let fandecks = jsonSerialized["fandecks"] as? [[String: AnyObject]] {
                    for fandeck in fandecks{
                        
                        fandecksList.append(Fandeck(aName: (fandeck["name"] as! String),
                                                    aID: (fandeck["id"] as! String),
                                                    aVersion: (fandeck["version"] as! String),
                                                    aDevice: (fandeck["device"] as! String),
                                                    aNumber: (fandeck["colors_number"] as! Int),
                                                    aFilename: (fandeck["filename"] as! String)))                    }
                }
                
            } catch _ {
                print("[ERROR] An error has happened with parsing of json data")
            }
        }
        
        return fandecksList
    }
    
    static func getCsvFileAsString(fileName:String)->String{
    
        var returnString = ""
        
        let name = NSString(string: fileName)
        
        let path = Bundle.main
        let pathForFile = path.path(forResource:name.deletingPathExtension, ofType: name.pathExtension)
        do{
            returnString = try String(contentsOfFile: pathForFile!)
        }
        catch{
            print("error loading csv file")
        }
        return returnString
        
    }
    
}
