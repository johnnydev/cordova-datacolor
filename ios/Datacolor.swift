//  Datacolor.swift

import Foundation
import CoreBluetooth
import UIKit

// Debugging
let printDebugMessages = true

@objc(Datacolor) class Datacolor : CDVPlugin, CaranProtocol
{
  // Singleton Pattern
  // Caran Controller can be accessed from Datacolor.sharedInstance
  class var instance: Datacolor {
    struct Singleton {
      static let instance = Datacolor()
    }
    return Singleton.instance
  }


  // Properties
  private var caran : DCICaranLib!            // Our Caran SDK property
  var devices: [CBPeripheral] = []            // Our list of found Caran device(s) property
  var connectedDevice: CBPeripheral?          // Connected Device
  var wasManuallyDisconnected:Bool = false    // Should be set to true to notify the app that the user manually disconnected the instrument
  var lastMeasure:LABMeasure?
  private let averageDownloadTimeForChunk = 1.744186

  // callbacks
  var cbScanDevices:String?
  var cbConnected:String?
  var cbButtonPress:String?
  var cbCalibrateDevice:String?
  var cbDidBecomeUncalibrated:String?
  var cbGetMeasure:String?
  var cbDidReceiveMeasurement:String?
  var cbEventsHandler:String?

  // private var measures = [LABMeasure]()
  // public var needsCalibration : Bool {
  //   get { return self.caran.needsCalibration()}
  // }

  override func pluginInitialize()
  {
    self.log(s:"dentro pluginInitialize")
    super.pluginInitialize(); // cordova initialization
    self.caran = DCICaranLib.init(delegate: self) // Initialize the SDK
    self.caran.setMeasurementModeAs(modeMeasureLabOnly)
  }

    private override init() {
        print("dentro innit");
        super.init();

    }
  private func log(s: String) {
    print ("[Datacolor] \(s)")
  }

  @objc(initialize:)
  func initialize(command: CDVInvokedUrlCommand) {
    let pluginResult:CDVPluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: true)
    self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
  }

  @objc(needsCalibration:)
  func needsCalibration(command: CDVInvokedUrlCommand) {
    let ret = self.caran.needsCalibration()
    let pluginResult:CDVPluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: ret)
    // pluginResult.setKeepCallbackAs(true)
    self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
  }

  @objc(scanDevices:)
  func scanDevices(command: CDVInvokedUrlCommand)
  {
    self.cbScanDevices = command.callbackId
    self.caran.getListOfVisibleDevices()
    let pluginResult:CDVPluginResult = CDVPluginResult(status: CDVCommandStatus_NO_RESULT)
    pluginResult.setKeepCallbackAs(true)
    self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
  }

  @objc(connectToDevice:)
  func connectToDevice(command: CDVInvokedUrlCommand)
  {
    self.log(s:"connectToDevice")
    self.log(s:command.arguments[0] as! String)
    self.cbConnected = command.callbackId

    let deviceName = command.arguments[0] as? String
    if let device = self.devices.first(where: {$0.name == deviceName}) {
      self.log(s:"Ho trovato il device e mi collego!")
      self.caran.connect(toDevice: device)
      let pluginResult:CDVPluginResult = CDVPluginResult(status: CDVCommandStatus_NO_RESULT)
      pluginResult.setKeepCallbackAs(true)
      self.commandDelegate.send(pluginResult, callbackId: cbConnected)
    }
  }

  @objc(calibrateDevice:)
  func calibrateDevice(command: CDVInvokedUrlCommand)
  {
    self.log(s:"calibrateDevice")
    self.cbCalibrateDevice = command.callbackId
    self.caran.calibrate()
    let pluginResult:CDVPluginResult = CDVPluginResult(status: CDVCommandStatus_NO_RESULT)
    pluginResult.setKeepCallbackAs(true)
    self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
  }

  @objc(takeMeasure:)
  public func takeMeasure(command: CDVInvokedUrlCommand)
  {
    self.log(s:"Take measure")
    self.cbGetMeasure = command.callbackId
    self.caran.takeMeasurement()
    let pluginResult:CDVPluginResult = CDVPluginResult(status: CDVCommandStatus_NO_RESULT)
    pluginResult.setKeepCallbackAs(true)
    self.commandDelegate!.send(pluginResult, callbackId: command.callbackId)
  }

  @objc(isBlueToothPoweredOn:)
  func isBlueToothPoweredOn(command: CDVInvokedUrlCommand)
  {
    let ret = self.caran.isBluetoothPoweredOn()
    let pluginResult:CDVPluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs:ret)
    self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
  }

  @objc(eventsHandler:)
  func eventsHandler(command: CDVInvokedUrlCommand) {
    self.log(s:"attachEvent")
    self.cbEventsHandler = command.callbackId
    let pluginResult:CDVPluginResult = CDVPluginResult(status: CDVCommandStatus_NO_RESULT)
    pluginResult.setKeepCallbackAs(true)
    self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
  }

  private func sendEvent(event: String, args: [Any]) {
    // prepare a ret value
    let ret = [
        "event": event,
        "args": args ] as [ AnyHashable: Any]
    self.log(s:"sendEvent \(event)")
    let pluginResult:CDVPluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: ret)
    pluginResult.setKeepCallbackAs(true)
    if let cb = self.cbEventsHandler {
      self.commandDelegate.send(pluginResult, callbackId: cb)
    } else {
      self.log(s:"Come mai sono qui ?!?!")
    }
  }
    struct Color : Codable {
        var name: String
        var L: Float
        var a: Float
        var b: Float
    }
    
  @objc(sendFandeck:)
  func sendFandeck(command: CDVInvokedUrlCommand) {
    self.log(s:"dentro sendFandeck")
    // creo il fandecks
    let c = command.arguments[0] as! String
    let jsonData:Data = c.data(using: .utf8) as! Data
    var colors:[Color] = []
    do {
        let decoder = JSONDecoder()
        colors = try decoder.decode([Color].self, from: jsonData)

    } catch {
        print("error")
    }
    print("\(colors)")
        let fandeck = CRNFandeck.init()
    fandeck?.fandeckColors = [] // ciao
    fandeck?.numColors = 0  
    for(_, color) in colors.enumerated() {
        print("\(color)")
        let c:CRNFandeckColor = CRNFandeckColor.init(name: color.name, andL: color.L, andA: color.a, andB: color.b)
        print("\(c)")
        fandeck?.fandeckColors.append( c  )
        fandeck?.numColors += 1
    }

    fandeck?.libName = "bestone"
    fandeck?.guid = "ciao"
    fandeck?.version = 1
//    fandeck?.fandeckColors = fdColors
//    let c = CRNFandeckColor.init(name: "prova", andL: 1.0, andA: 1.0, andB: 1.0)
//    let colors: [CRNFandeckColor] = [c!]
//    // fandeck?.fandeckColors = colors.enumerate().map({(_: Int, color: NSObject)})
//    fandeck?.fandeckColors = colors

    let fandecks: [CRNFandeck] = [fandeck!]
    self.caran.downloadFandeck(with: fandecks)

    let pluginResult:CDVPluginResult = CDVPluginResult(status: CDVCommandStatus_NO_RESULT)
    // pluginResult.setKeepCallbackAs(true)
    self.commandDelegate.send(pluginResult, callbackId: command.callbackId)
  }
  ////////////////// CALLBACK

  public func didReceiveList(ofVisibileDevices peripheralList: NSMutableArray!) {
    self.log(s:"didReceiveList")
    // Clear our device list.
    devices = []
    var deviceNames = [String]()

    // Populate our device list with what came back from the scan.
    for element in peripheralList {
      let dev = element as! CBPeripheral
      devices.append(dev)
      deviceNames.append(dev.name as! String)
    }

    let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: deviceNames)
    if let cb = self.cbScanDevices {
      self.commandDelegate!.send(
        pluginResult,
        callbackId: cb
      )
    }
  }

  public func didGetButtonPress() {
    self.log(s:"didGetButtonPress")
    if let cb = self.cbButtonPress {
      let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK)
      self.commandDelegate.send(pluginResult, callbackId: cb)
    }
  }

  public func didConnect(toDevice _device: CBPeripheral!) {
    self.log(s:"didConnecte to device")
    self.connectedDevice = _device
    let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "OK")
    if let cb = self.cbConnected {
      self.log(s:"mando il delegate del didconnect!")
      self.commandDelegate!.send(pluginResult, callbackId: cb)
    }
  }

  @available(iOS 5.0, *)
  public func didFailToConnect(toDevice _device: CBPeripheral!) {
    self.log(s:"didFailToConnect")
    connectedDevice = nil
    let pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR)
    if let cb = self.cbConnected {
      self.commandDelegate.send(pluginResult, callbackId: cb)
    }
  }


  public func didReceiveMeasurement(withL l: NSNumber!,
                                    andA a: NSNumber!,
                                    andB b: NSNumber!) {


//    let fandeckMatchArr = [MeasureMatch]()
//    let lastMeasure = LABMeasure(_l: Float(l),_a: Float(a),_b: Float(b),_matchArr: fandeckMatchArr)
//    let color = LabMeasureUtils(lastMeasure)
    self.log(s:"dentro didReceiveMeasurement");
    let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: [l,a,b])
    self.sendEvent(event:"receiveMeasure", args:[l,a,b])

    if let cb = self.cbGetMeasure {
      self.commandDelegate.send(pluginResult, callbackId: cb)
    }


    // if let cb = self.cbDidReceiveMeasurement {
    //   self.commandDelegate.send(pluginResult, callbackId: cb)
    // }

    // store the measurement in the list
    // self.storeMeasure(_labMeasure: lastMeasure!)

    // print("number of measurements: " + measures.count.description)

    // Send the notification
    // print("Send: MeasureNotification")
    // NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MeasureFromDeviceNotification"), object: self, userInfo: [
    //     "LABMeasure" : lastMeasure as Any
    //     ])

  }

  func didBecomeUncalibrated() {
    self.log(s:"didbecomeuncalibrated")
    self.sendEvent(event: "didBecomeUncalibrate", args: [])
  }

  func didPassCalibration(withWarning warning: Bool) {
    self.log(s:"calibration passed")
    let pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: warning)
    if let cb = self.cbCalibrateDevice {
      self.commandDelegate.send(pluginResult, callbackId: cb)
    }
  }

  func didFailCalibration() {
    print("Send: Calibration failed!")
    let pluginResult = CDVPluginResult(status: CDVCommandStatus_ERROR)
    if let cb = self.cbCalibrateDevice {
      self.commandDelegate.send(pluginResult, callbackId: cb)
    }
  }


    func didDisconnectFromDevice() {
      self.log(s:"didDisconnectFromDevice")
      self.sendEvent(event:"disconnected", args:["ciao"])
    }

    func didReceiveMeasurement(withL l: NSNumber!, andA a: NSNumber!, andB b: NSNumber!, andMatch1 m1: FandeckMatch, andMatch2 m2: FandeckMatch, andMatch3 m3: FandeckMatch) {
      self.log(s:"sono dentro didReceiveMeasurement nel mode!")
    }

    func didStartFandeckDownload(forTotalChunks totalChunks: UInt32) {
      self.log(s:"didStartFandeckDownload \(totalChunks)")
    }

    func didFandeckDownload(forChunk thisChunk: UInt32, ofTotalChunks totalChunks: UInt32, withColorsTransferred numColors: UInt32, ofTotalColors totalColors: UInt32) {
      self.log(s:"didFandeckDownload \(thisChunk) \(totalChunks) \(numColors) \(totalColors)")
    }

    func didCompleteFandeckDownload(withBytesTransferred totalBytesTransferred: UInt, withSuccess success: Bool) {
      self.log(s:"didCompleteFandeckDownload!")
    }

    func didGetCodesOEM(_ oemCode: String!, andRegistration registrationCode: String!, withSuccess success: Bool) {

    }

    func didSetCodes(withSuccess success: Bool) {

    }


  // @available(iOS 5.0, *)
  // func didDisconnectFromDevice() {

  //     if printDebugMessages {
  //         print("APP: didDisconnectFromDevice")
  //     }
  //     connectedDevice = nil

  //     // Send a notification
  //     print("Send: MeasureNotification")
  //     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DisconnectFromDeviceNotification"), object: self)


  // }


    //--------------------------------------------------------------------------------
    //
    // didReceiveMeasurement
    // Called after successful measurement.
    //
    // Should perform whatever additional operations are needed in the app after
    // getting the measurement data.
    //
    //--------------------------------------------------------------------------------

    // public func didReceiveMeasurement(withL l: NSNumber!,
    //                                   andA a: NSNumber!,
    //                                   andB b: NSNumber!,
    //                                   andMatch1 m1: FandeckMatch, // Added these to the protocol.
    //                                   andMatch2 m2: FandeckMatch,
    //                                   andMatch3 m3: FandeckMatch) {

    //     if printDebugMessages {
    //         print("APP: didReceiveMeasurement Lab + Match")
    //     }

    //     var fandeckMatchArr = [MeasureMatch]()

    //     let name1 = MeasureMatch.cnameToString(cObj: m1.name)
    //     let name2 = MeasureMatch.cnameToString(cObj: m2.name)
    //     let name3 = MeasureMatch.cnameToString(cObj: m3.name)

    //     let fandeck1 = MeasureMatch.cnameToString(cObj: m1.fdname)
    //     let fandeck2 = MeasureMatch.cnameToString(cObj: m2.fdname)
    //     let fandeck3 = MeasureMatch.cnameToString(cObj: m3.fdname)

    //     let match_1 = MeasureMatch(_name: name1, _l: m1.l, _a: m1.a, _b: m1.b, _dE: m1.deltaE, _fandeck:fandeck1)
    //     let match_2 = MeasureMatch(_name: name2, _l: m2.l, _a: m2.a, _b: m2.b, _dE: m2.deltaE, _fandeck:fandeck2)
    //     let match_3 = MeasureMatch(_name: name3, _l: m3.l, _a: m3.a, _b: m3.b, _dE: m3.deltaE, _fandeck:fandeck3)

    //     fandeckMatchArr.append(match_1)
    //     fandeckMatchArr.append(match_2)
    //     fandeckMatchArr.append(match_3)

    //     lastMeasure = LABMeasure(_l: Float(l),_a: Float(a),_b: Float(b),_matchArr: fandeckMatchArr)

    //     // store the measurement in the list
    //     self.storeMeasure(_labMeasure: lastMeasure!)

    //     print("number of measurements: " + measures.count.description)


    //     // Send the notification
    //     print("Send: MeasureNotification")
    //     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "MeasureFromDeviceNotification"), object: self, userInfo: [
    //         "LABMeasure" : lastMeasure as Any
    //         ])

    // }


    //--------------------------------------------------------------------------------
    //
    // didBecomeUncalibrated
    //
    // Called ONCE at most, during a connection session, if the SDK says that Caran needs
    // calibration. This may be called after a measurement is taken, and also
    // after connection, so it's useful in case Caran goes from a calibrated
    // to an uncalibrated state during the connection session.
    //
    // Note: host app can call needsCalibration at any time while connected to check
    // the calibration status in addition to observing this warning.
    //
    //--------------------------------------------------------------------------------


    //--------------------------------------------------------------------------------
    //
    // didStartFandeckDownloadForTotalChunks
    //
    //
    //
    //--------------------------------------------------------------------------------

    // func didStartFandeckDownload(forTotalChunks totalChunks: UInt32) {

    //     if printDebugMessages {
    //         print("APP: didStartFandeckDownload")
    //     }

    //     // Send a notification
    //     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DidStartFandeckDownloadNotification"), object: self, userInfo: ["totalChunks" : Int(totalChunks) as Int])


    // }


    //--------------------------------------------------------------------------------
    //
    // didFandeckDownloadForChunk
    //
    //
    //
    //--------------------------------------------------------------------------------

    // public func didFandeckDownload(forChunk thisChunk: UInt32, ofTotalChunks totalChunks: UInt32, withColorsTransferred numColors: UInt32, ofTotalColors totalColors: UInt32) {

    //     if printDebugMessages {
    //         print("APP: didFandeckDownload")
    //     }

    //     // Send a notification
    //     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DidFandeckDownloadNotification"), object: self, userInfo: [
    //         "totalChunks" : Int(totalChunks) as Int,
    //         "thisChunk" : Int(thisChunk) as Int,
    //         "transferredColors" : Int(numColors) as Int,
    //         "totalColors" : Int(totalColors) as Int
    //         ])

    // }



    //--------------------------------------------------------------------------------
    //
    // didCompleteFandeckDownload
    //
    //
    //
    //--------------------------------------------------------------------------------

    // func didCompleteFandeckDownload(withBytesTransferred totalBytesTransferred: UInt, withSuccess success: Bool) {


    //     if printDebugMessages {
    //         print("APP: didCompleteFandeckDownload")
    //     }

    //     // Send a notification
    //     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DidCompleteFandeckDownloadNotification"), object: self, userInfo: [
    //         "totalBytesTransferred" : Int(totalBytesTransferred) as Int,
    //         "success" : success as Bool
    //         ])
    // }

    //--------------------------------------------------------------------------------
    //
    // didGetCodesOEM:(NSString *)oemCode andRegistration:(NSString *)registrationCode withSuccess:(BOOL)success;
    //
    //
    //
    //--------------------------------------------------------------------------------
    // func didGetCodesOEM(_ oemCode: String!, andRegistration registrationCode: String!, withSuccess success: Bool) {
    //     if printDebugMessages {
    //         print("APP: didGetCodesOEM")
    //     }

    //     // Send a notification
    //     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DidGetCodes"), object: self, userInfo: [
    //         "oemCode" : String(oemCode) as String,
    //         "registrationCode" : String(registrationCode) as String,
    //         "success" : success as Bool
    //         ])
    // }

    //--------------------------------------------------------------------------------
    //
    // didSetCodesWithSuccess:(BOOL)success;
    //
    //
    //
    //--------------------------------------------------------------------------------
    // func didSetCodes(withSuccess success: Bool) {
    //     if printDebugMessages {
    //         print("APP: didSetCodes")
    //     }

    //     // Send a notification
    //     NotificationCenter.default.post(name: NSNotification.Name(rawValue: "DidSetCodes"), object: self, userInfo: [
    //         "success" : success as Bool
    //     ])
    // }

  // public func disconnectFromDevice()
  // {
  //     self.wasManuallyDisconnected = true
  //     if(caran != nil){
  //         caran.disconnectFromDevice()
  //     }

  // }

  // public func clearMeasurementsList(){
  //     measures.removeAll()
  //     saveMeasurements()
  // }

    // public func deleteFromMeasurementsListAt(_index : Int){
    //     measures.remove(at: _index)
    // }

    // public func getDeviceSerialFromName(aName : String) -> String{

    //     var name = aName
    //     let range = aName.range(of: "DC_CARAN_", options: .caseInsensitive, range: nil, locale: nil)

    //     if (range != nil){
    //         name.removeSubrange(range!)
    //         return name.trimmingCharacters(in: .whitespacesAndNewlines)
    //     }
    //     else{
    //         return ""
    //     }
    // }

    // public func getDeviceSerial() -> String{

    //     var name = ""

    //     if(self.connectedDevice != nil){
    //         name = getDeviceSerialFromName(aName: self.connectedDevice!.name!)
    //     }

    //     return name
    // }

    // public func getFirwareVersion() -> String {

    //     var fwVersion = ""

    //     if(connectedDevice != nil){

    //         fwVersion = String(self.caran.firmwareRev())

    //     }

    //     return fwVersion
    // }

    // // SDK
    // public func getSdkVersion() -> String {
    //     var sdkversion = ""
    //     if(caran != nil){
    //         sdkversion = String(caran.sdkRev())
    //     }
    //     return sdkversion
    // }

    // // MODE
    // public func isModeLabPlusMatch() -> Bool {
    //     var measureMode = false
    //     if(caran != nil){
    //         if(caran.getMeasurementMode() == modeMeasureLabPlusMatch){
    //             measureMode = true
    //         }
    //     }
    //     return measureMode
    // }

    // public func setMeasurementModeAs(_mode:CrnMeasurementMode){

    //     if(caran != nil){
    //         caran.setMeasurementModeAs(_mode)
    //         DCUtils.instance.saveLocalSetting(_settingValue: _mode.rawValue, _withKey: "MeasurementMode")
    //     }
    // }

    // Fandeck Info

    // public func getFandeckNames()->[String] {

    //     var fandeckNames = [String]()

    //     if(caran != nil){
    //         let sdkFandeckNames = caran.fandeckNames()
    //         if(sdkFandeckNames != nil){
    //             for element in sdkFandeckNames! {
    //                 fandeckNames.append(String(element as! NSMutableString))
    //             }
    //         }
    //     }
    //     return fandeckNames;
    // }


    // public func getFandeckCreationDate()->String {

    //     var creationDate = "Not Available"
    //     if(caran != nil){
    //         creationDate = String(caran.fandeckCreationDate())
    //     }
    //     return creationDate;
    // }

    // public func getFandeckIlluminant()->String {

    //     var illuminant = "Not Available"
    //     if(caran != nil){
    //         illuminant = String(caran.fandeckIlluminant())
    //     }
    //     return illuminant;
    // }

    // public func getFandeckCount()->Int {

    //     var fandeckCount = 0
    //     if(caran != nil){
    //         fandeckCount = Int(caran.fandeckCount())
    //     }
    //     return fandeckCount;
    // }

    // public func getFandeckRecords()->Int {

    //     var fandeckRecords = 0
    //     if(caran != nil){
    //         fandeckRecords = Int(caran.fandeckRecords())
    //     }
    //     return fandeckRecords;
    // }


    // public func uploadFandeckCsv(array:[String]){
    //     if(caran != nil){
    //         caran.downloadFandeck(withCSVs: array)
    //     }
    // }

    // public func maxNumFandecks()->Int{
    //    return Int(self.caran.maxNumFandecks())
    // }

    // public func maxNumColors()->Int{
    //    return Int(self.caran.maxNumColors())
    // }

    // public func getDownloadTimeForChunk()->Double{

    //     let time = DCUtils.instance.loadLocalSetting(_withKey: "DownloadTimeForChunk")

    //     if(time == nil){
    //         return averageDownloadTimeForChunk
    //     }else{
    //         return time as! Double
    //     }

    // }

    // public func setDownloadTimeForChunk(time:Double){
    //     DCUtils.instance.saveLocalSetting(_settingValue: time, _withKey: "DownloadTimeForChunk")
    // }


    // public func getChunksFormNumberOfColors(colors:Int)->Int{

    //     var totalBytes:UInt32 = 0
    //     var totalChunks:UInt32 = 0

    //     caran.getFandeckDownloadStats(fromTotalColors: UInt32(colors), asTotalBytes: &totalBytes, andTotalChunks: &totalChunks)

    //     return Int(totalChunks)
    // }



    // // Registration

    // public func getCodes(){
    //     if(caran != nil){
    //         caran.getCodes()
    //     }
    // }

    // public func writeCodes(_oemCode:String,_registrationCode:String){
    //     if(caran != nil){
    //         caran.setCodesWithOEMCode(_oemCode, andRegistrationCode: _registrationCode)
    //     }
    // }




    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: PRIVATE METHODS
    // MARK:
    //--------------------------------------------------------------------------------

    // private func storeMeasure(_labMeasure: LABMeasure){
    //     measures.insert(_labMeasure, at: 0)
    //     saveMeasurements()
    // }

    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: NSCODING
    // MARK:
    //--------------------------------------------------------------------------------

    // func saveMeasurements() {
    //     let isSuccessfulSave = NSKeyedArchiver.archiveRootObject(measures, toFile: LABMeasure.ArchiveURL.path)

    //     if !isSuccessfulSave {
    //         print("Failed to save measurements...")
    //     }
    // }

    // func loadMeasurements() -> [LABMeasure]? {

    //     return NSKeyedUnarchiver.unarchiveObject(withFile: LABMeasure.ArchiveURL.path) as? [LABMeasure]
    // }



    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: CARAN PROTOCOL
    // MARK:
    //--------------------------------------------------------------------------------

    //--------------------------------------------------------------------------------
    //
    // didGetButtonPress
    //
    // Called when the button is pushed on the device
    //
    //--------------------------------------------------------------------------------




    //--------------------------------------------------------------------------------
    //
    // didConnectToDevice
    
    // Called after successful connection to a Caran.
    
    // --------------------------------------------------------------------------------


}
