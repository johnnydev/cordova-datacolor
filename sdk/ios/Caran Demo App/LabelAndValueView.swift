//
//  LabelAndValueView.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 04/10/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import UIKit

@IBDesignable

class LabelAndValueView: UIView {

    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: @IBInspectable PROPERTIES
    // MARK:
    //--------------------------------------------------------------------------------
    
    @IBInspectable var labelMessage: String = "" {
        didSet {
            labelItem.text = labelMessage
        }
    }
    
    @IBInspectable var valueMessage: String = "" {
        didSet {
            valueItem.text = valueMessage
        }
    }


    
    
    
    // INIT
    
    
    var contentView : UIView?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        contentView = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        contentView!.frame = bounds
        
        // Make the view stretch with containing view
        contentView!.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView!)
    }
    
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }

    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: PROPERTIES
    // MARK:
    //--------------------------------------------------------------------------------
    @IBOutlet weak var labelItem: UILabel!
    @IBOutlet weak var valueItem: UILabel!
    
    
    private var _centered = false
    public var centered : Bool {
        set {
            
            _centered = newValue
            
            if(_centered) {
                labelItem.textAlignment = NSTextAlignment.center
                valueItem.textAlignment = NSTextAlignment.center
            }
            else {
                labelItem.textAlignment = NSTextAlignment.left
                valueItem.textAlignment = NSTextAlignment.left
            }
                        
        }
        get{ return _centered }
        
    }
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: METHODS
    // MARK:
    //--------------------------------------------------------------------------------
    
    public func setTransparent(){
        self.backgroundColor = UIColor.clear
        self.isOpaque = false
    }
    
    public func setLabelColorWith(l: Float){
    
        if(l > 55){
            labelItem.textColor = UIColor.darkGray
            valueItem.textColor = UIColor.black
        }else{
            labelItem.textColor = UIColor.lightGray
            valueItem.textColor = UIColor.white
        }
        
    }
    
    public func setLabelAndValue(_label:String,_value:String)
    {
        setLabel(_label: _label)
        setValue(_value: _value)
    }
    public func setLabel(_label:String)
    {
        labelItem.text = _label;
    }
    public func setValue(_value:String)
    {
        valueItem.text = _value
    }
    
    
}
