//
//  DCUtils.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 03/10/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

class DCUtils
{

    
    public static let MainColor = UIColor(red: 203/255, green: 51/255, blue: 59/255, alpha: 1)
    
    
    // Singleton
    class var instance: DCUtils {
        struct Singleton {
            static let instance = DCUtils()
        }
        return Singleton.instance
    }
    
    
    // MARK: CSV Export    
    static let DocDir = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let CSVExportfileDestinationUrl = DocDir.appendingPathComponent("caran_export.csv")
    
    func exportCSV(aList : [LABMeasure]) -> Bool
    {
        
        let fileDestinationUrl = DCUtils.CSVExportfileDestinationUrl
        var filecreated = true
        
        do {
            let mytext = try String(contentsOf: fileDestinationUrl, encoding: String.Encoding.utf8)
            print(mytext)   // "some text\n"
        } catch let error as NSError {
            print("error loading from url \(fileDestinationUrl)")
            print(error.localizedDescription)
        }
        
        
        var tempString = String(format: "%@;%@;%@;%@;%@;%@;%@;%@;%@;%@;%@;%@;%@\n",
                                "L","A","B","Date",
                                "Match 1 - Name","Match 1 - Fandeck","Match 1 - Delta E",
                                "Match 2 - Name","Match 2 - Fandeck","Match 2 - Delta E",
                                "Match 3 - Name","Match 3 - Fandeck","Match 3 - Delta E"
                                )
        
        for labMeasure in aList.reversed()
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            
            // putting the LabMeasure values into the String
            tempString += String(format: "%@;%@;%@;%@", String(format: "%.2f", labMeasure.lab_l),String(format: "%.2f", labMeasure.lab_a),String(format: "%.2f", labMeasure.lab_b),dateFormatter.string(from: labMeasure.date))
            
            // Checking the match array
            for measureMatch in labMeasure.matchArr{
                tempString += String(format: ";%@;%@;%@",measureMatch.name,measureMatch.fandeck,measureMatch.getDeltaEString())
            }
            
            // new line
            tempString += "\n"
            
            
        }
        
        
        do{
            try tempString.write(to: fileDestinationUrl, atomically: true, encoding: String.Encoding.utf8)
        }
        catch{
            print("erro creating the file")
            filecreated = false
        }
        
        return filecreated
    }
    
    
    
    // MARK: Local Notifications
    
    
    func sendLocalNotificationForDisconnection(){
    
        print("local notification disconnection")
        
        let localNotification = UILocalNotification()
        // localNotification.alertTitle = "Warning"
        localNotification.alertBody = "Connection interrupted with the instrument"
        localNotification.fireDate = Date(timeIntervalSinceNow: 1)
        localNotification.timeZone = NSTimeZone.default
        localNotification.soundName = UILocalNotificationDefaultSoundName
        UIApplication.shared.scheduleLocalNotification(localNotification)
        
    }
    
    func sendLocalNotificationForNotCalibrated(){
    
        print("local notification not calibrated")
        
        // send a notification
        let localNotification = UILocalNotification()
        // localNotification.alertTitle = "Warning"
        localNotification.alertBody = "Your instrument needs calibration"
        localNotification.fireDate = Date(timeIntervalSinceNow: 1)
        localNotification.timeZone = NSTimeZone.default
        localNotification.applicationIconBadgeNumber = 1
        localNotification.soundName = UILocalNotificationDefaultSoundName
        UIApplication.shared.scheduleLocalNotification(localNotification)
        
        // set the badge number to 1
        UIApplication.shared.applicationIconBadgeNumber = 1
        
    }
    
    func sendLocalNotificationForCalibrated(){
        
        print("local notification calibrated")
        
        // send a notification
        let localNotification = UILocalNotification()
        // localNotification.alertTitle = "Congratulations"
        localNotification.alertBody = "Your instrument is now calibrated"
        localNotification.fireDate = Date(timeIntervalSinceNow: 1)
        localNotification.timeZone = NSTimeZone.default
        localNotification.applicationIconBadgeNumber = -1
        localNotification.soundName = UILocalNotificationDefaultSoundName
        UIApplication.shared.scheduleLocalNotification(localNotification)
        
        // remove the badge
        UIApplication.shared.applicationIconBadgeNumber = -1
        
    }
    
    // Local setting
    func saveLocalSetting(_settingValue:Any,_withKey:String){
        let userDefaults = UserDefaults.standard
        userDefaults.set(_settingValue, forKey: _withKey)
    }
    
    func loadLocalSetting(_withKey:String)->Any?{
        let userDefaults = UserDefaults.standard
        if let userSettings = userDefaults.object(forKey: _withKey){
            return(userSettings)
        }
        return nil
    }
    
    // timeInterval to string
    func stringFromTimeInterval(time:TimeInterval)->String{
    
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        
        if(minutes > 0){
            return String(format:"%02i min %02i sec", minutes, seconds)
        }else{
            return String(format:"%02i sec", seconds)
        }
        
    }
}
