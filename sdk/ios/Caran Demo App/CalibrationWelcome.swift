//
//  CalibrationWelcome.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 04/10/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import UIKit

class CalibrationWelcome: UIViewController {

    @IBOutlet weak var btnClose: UIBarButtonItem!
    @IBOutlet weak var btnCalibrate: UIButton!
    @IBOutlet weak var loadingView: UIView!
    
    deinit {
        NotificationCenter.default.removeObserver(self)        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        // Receive uncalibrated event
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didReceiveCalibrationFailedNotification),
            name: NSNotification.Name(rawValue: "DeviceCalibrationFailedNotification"),
            object: nil)
        
        // Receive calibrated event
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didReceiveCalibratedNotification),
            name: NSNotification.Name(rawValue: "DeviceCalibratedNotification"),
            object: nil)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func tapOnNextButton(_ sender: AnyObject) {
        
        //todo should disable the button on the device if possible
        
        // go to the next screen
        self.navigationController?.pushViewController(self.storyboard!.instantiateViewController(withIdentifier: "calibrationCalibrate"), animated: true)
        
    }
    
    @IBAction func tabOnCalibrate(_ sender: AnyObject) {
        
        doCalibration()
    
    }
    
    func doCalibration(){

        // disable the buttons
        btnCalibrate.isEnabled = false
        btnClose.isEnabled = false;
        
        self.loadingView.isHidden = false
        
        // Send the calibration command.
        CaranController.instance.calibrateDevice()
        
    }


    @IBAction func tapOnClose(_ sender: AnyObject) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    // MARK: Calibration events
    func didReceiveCalibratedNotification(notification: NSNotification) {
        print("didReceiveCalibratedNotification")
        self.loadingView.isHidden = true
        self.btnCalibrate.isEnabled = true
        self.btnClose.isEnabled = true;
        self.navigationController?.pushViewController(self.storyboard!.instantiateViewController(withIdentifier: "calibrationResult"), animated: true)
    }
    
    func didReceiveCalibrationFailedNotification(notification: NSNotification) {
        print("DeviceCalibrationFailedNotification")
        showAlertRetry()
    }
    
    
    func showAlertRetry()
    {
        let alert = UIAlertController(title: "Calibration failed", message:
            "Are you sure that the device is on the white tile?", preferredStyle: .alert)
        
        self.present(alert, animated: false, completion: nil)
        
        alert.addAction(UIAlertAction(title: "Cancel",
                                      style: UIAlertActionStyle.default,
                                      handler:  {action in
                                        // Handler code
                                        self.loadingView.isHidden = true
                                        self.btnCalibrate.isEnabled = true
                                        self.btnClose.isEnabled = true;
        }))
        alert.addAction(UIAlertAction(title: "Try again", style: .default, handler:
            {action in
                // Handler code
                self.doCalibration()
        }))
    }
    
}
