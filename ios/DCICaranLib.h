//
// DCICaranLib.h
//  CaranSDK
//
//  David Miller
//
//  Copyright © 2016-2019 Datacolor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "CRNFandeck.h"
#import "CRNFandeckColor.h"

// The SDK and protocol.
@class DCICaranLib;
@protocol CaranProtocol;

// These classes can be used to construct Fandecks, which can be used in downloadFandeckWithArray
// for combining fandecks and downloading into the device. (ColorReader PRO only)
@class CRNFandeck;
@class CRNFandeckColor;

@interface DCICaranLib : NSObject

// The host app must adopt CaranProtocol.
@property (nonatomic, weak) id<CaranProtocol> delegate;

//--------------------------------------------------------------------------------
//
// Caran SDK Public Structs, Methods, etc.
//
//--------------------------------------------------------------------------------

//--------------------------------------------------------------------------------
//
// FandeckMatch
//
// This is used to return a matching fandeck value from internal fandeck storage.
// When modeMeasureLabPlusMatch is used while measuring, and when there is internal
// fandeck data stored in the device, 3 of these will be along with the measured
// Lab value by didReceiveMeasurementWithL:andA:andB:andMatch1:andMatch2:andMatch3:
//
// (ColorReader PRO only)
//
//--------------------------------------------------------------------------------
struct FandeckMatch
{
    unsigned char name[12];     // Fandeck record name of match as a null terminated C string
    unsigned char fdname[12];   // Fandeck name of match as a null terminated C string
    float l;                    // Fandeck Lab of the match
    float a;
    float b;
    float deltaE;               // dE of the match vs. the measured color.
};

// Used to set and get the measurement mode.
// ColorReader PRO can use either mode. ColorReader can only use modeMeasureLabOnly (internal default)
typedef enum {modeMeasureLabOnly, modeMeasureLabPlusMatch} CrnMeasurementMode;

//--------------------------------------------------------------------------------
//
// initWithDelegate
//
// The host app calls this to init the Caran SDK library and set itself as the delegate.
//
//--------------------------------------------------------------------------------
-(id) initWithDelegate:(NSObject*) _delegate;

//--------------------------------------------------------------------------------
//
// getListOfVisibleDevices
//
// The host app calls this to start the Bluetooth discovery process. When this is
// complete, the host app's didReceiveListOfVisibileDevices method is called, to
// provide a CBPeripheral array of the found results.
//
//--------------------------------------------------------------------------------
-(void) getListOfVisibleDevices;

//--------------------------------------------------------------------------------
//
// connectToDevice
//
// Host app calls this to connect to a CBPeripheral selected from the array returned
// by didReceiveListOfVisibileDevices. When the connection attempt is complete:
//
// On PASS, didConnectToDevice will be called. The host app can move on to its
// connected UI; other SDK functions which follow will be usable. When the user
// presses the Caran button, didGetButtonPress and then didReceiveMeasurementWith
// will be called to notify the host app about the button press and then
// subsequently give it the measured value and associated data. Etc.
//
// On FAIL, didFailToConnectToDevice will be called. (This is an unexpected error
// and in normal operation, should not happen).
//
//--------------------------------------------------------------------------------
-(void) connectToDevice:(CBPeripheral*) _device;

//--------------------------------------------------------------------------------
//
// retrieveConnectedDevice
//
// Returns the currently connected CBPeripherals device (if any). Returns nil
// if no Caran is connected. (info only, not currently used in the demo app and
// not required at this point).
//
//--------------------------------------------------------------------------------
-(CBPeripheral*) retrieveConnectedDevice;

//--------------------------------------------------------------------------------
//
// SDKRev
//
// Returns the SDK revision.
//
//--------------------------------------------------------------------------------
-(NSMutableString *)SDKRev;

//--------------------------------------------------------------------------------
//
// firmwareRev
//
// Returns the firmware revision of the currently connected device.
//
//--------------------------------------------------------------------------------
-(NSMutableString *) firmwareRev;

//--------------------------------------------------------------------------------
//
// maxNumFandecks
//
// Returns the maximum number of internal fandecks supported by the device.
//
// (ColorReader PRO only)
//
//--------------------------------------------------------------------------------
-(unsigned int)maxNumFandecks;

//--------------------------------------------------------------------------------
//
// maxNumColors
//
// Returns the maximum number of internal colors supported by the device.
//
// (ColorReader PRO only)
//
//--------------------------------------------------------------------------------
-(unsigned int)maxNumColors;

//--------------------------------------------------------------------------------
//
// Fandeck Information
//
// These functions return details about the currently loaded aggregate fandeck in the connected device.
//
// (ColorReader PRO only)
//
//--------------------------------------------------------------------------------
-(NSMutableString *) fandeckCreationDate;   // Creation date
-(NSMutableString *) fandeckIlluminant;     // Illuminant
-(unsigned int) fandeckCount;               // Number of fandecks combined into the internal fandeck
-(NSArray *) fandeckNames;                  // Array of NSMutableStrings of the internal fandeck names.
-(NSArray *) fandeckGUIDs;                  // Array of NSMutableStrings of the internal fandeck GUIDs.
-(NSArray *) fandeckVersions;               // Array of NSNumbers of the internal fandeck versions.
-(unsigned int) fandeckRecords;             // Total # of fandeck records in the internal fandeck

//--------------------------------------------------------------------------------
//
// getFandeckDownloadStatsFromTotalColors:asTotalBytes:andTotalChunks:
//
// Host app can call this from its screen that lets the user select fandecks
// for aggregation and download. Given the total # of colors, this will return
// the total size of the binary and total # of chunks that will be downloaded
// from this selection. If the host app knows the approximate transfer time per
// chunk, it can calculate an approximate download time for the selection and report
// this to the user.
//
// ColorReader PRO only.
//
//--------------------------------------------------------------------------------
-(void) getFandeckDownloadStatsFromTotalColors:(unsigned int)numColors
                                  asTotalBytes:(unsigned int *)totalSize
                                andTotalChunks:(unsigned int *)totalChunks;

//--------------------------------------------------------------------------------
//
// Fandeck Download from multiple CSV files. SDK will internally create
// the final binary from these CSVs and proceed with the download, and the protocol
// functions below will be called (see descriptions below):
//
// didStartFandeckDownloadForTotalChunks, (when downloading starts)
// didFandeckDownloadForChunk:ofTotalChunks:, (for progress during download)
// didCompleteFandeckDownloadWithBytesTransferred:withSuccess: (when finished or aborted)
//
// (ColorReader PRO only)
//
//--------------------------------------------------------------------------------
-(void) downloadFandeckWithCSVs:(NSArray*)theCSVs;

//--------------------------------------------------------------------------------
//
// Fandeck Download, each object in the array should be a FanDeck rather than CSV.
// Otherwise, the same as the above.
//
// (ColorReader PRO only)
//
//--------------------------------------------------------------------------------
-(void) downloadFandeckWithArray:(NSArray*)theFandecks;

//--------------------------------------------------------------------------------
//
// needsCalibration
//
// If YES, then Caran should be calibrated on the white tile based on internal
// criteria as determined by the SDK. This is a state flag that can be tested
// at any point while a device is connected, however, see the note below about
// "when" the SDK updates this state flag internally.
//
// NOTE: The SDK determines calibration state (a) when a device is connected,
// immediately following connection and just prior to didConnectToDevice being called,
// and (b) after a measurement has completed, and just prior to didReceiveMeasurement...
// being called. Between measurements, while Caran is awake and connected, it's
// "possible" that the calibration state limit based on time may have been exceeded;
// this will be picked up on the next measurement and/or reconnection/session.
//
//--------------------------------------------------------------------------------
-(bool) needsCalibration;

//--------------------------------------------------------------------------------
//
// isBluetoothPoweredOn
//
// If YES, then yes.
// If NO, then no. :-)
//
// If NO, then the SDK won't be able to discover and connect to devices. Call this
// after the SDK is initialized to find out whether discovery and connection are
// possible; can notify the user and tell them to turn on BlueTooth if NO.
//
//--------------------------------------------------------------------------------
-(bool) isBluetoothPoweredOn;

//--------------------------------------------------------------------------------
//
// isMontBlanc
//
// Tells the caller if the connected device is MontBlanc (ColorReader), or not.
// If "not", it's a ColorReader PRO.
//
//--------------------------------------------------------------------------------
-(bool) isMontBlanc;

//--------------------------------------------------------------------------------
//
// setMeasurementModeAs
//
// Sets the measurement mode (default is modeMeasureLabPlusMatch). Behavior:
//
// modeMeasureLabPlusMatch: Caran button presses will return measurement data
// through didReceiveMeasurementWithL:andA:andB:andMatch1:andMatch2:andMatch3, so
// the matches plus the Lab measurement will be returned. The process takes longer
// and it's advisable to display a progress indicator between didGetButtonPress
// and didReceiveMeasurementWithL:andA:andB:andMatch1:andMatch2:andMatch3.
//
// modeMeasureLabOnly: Caran button presses will return measurement data
// through didReceiveMeasurementWithL:andA:andB:, so only the Lab measurement will
// be returned. The process is much faster and it may not be necessary to display
// a progress indicator between didGetButtonPress and didReceiveMeasurementWithL:andA:andB:
//
// ColorReader PRO can use either measurement mode.
// ColorReader will always use LabOnly (does not need to be set)
//
//--------------------------------------------------------------------------------
-(void) setMeasurementModeAs: (CrnMeasurementMode)theMode;

//--------------------------------------------------------------------------------
//
// getMeasurementMode
//
//--------------------------------------------------------------------------------
-(CrnMeasurementMode) getMeasurementMode;

//--------------------------------------------------------------------------------
//
// getCodes
//
// Tell the SDK to get the OEM and Registration code strings from Caran, maximum 16
// characters each.
//
// On completion, the results will come back from protocol method
// didGetCodesOEM:andRegistration:withSuccess:
//
//--------------------------------------------------------------------------------
-(void) getCodes;

//--------------------------------------------------------------------------------
//
// setCodesWithOEMCode
//
// Write OEM and Registration Code strings into Caran, maximum 16 characters each.
// (If longer, they will be truncated).
//
// On completion, pass/fail for writing the codes will come back from protocol method
// didSetCodesWithSuccess:
//
//--------------------------------------------------------------------------------
-(void) setCodesWithOEMCode:(NSString *)oemCode andRegistrationCode:(NSString *)registrationCode;

//--------------------------------------------------------------------------------
//
// calibrate
//
// The host app calls this command to perform a calibration procedure on the white tile.
// Prior to calling this method, the host app should tell the user to put the Caran on
// the white tile, with description/pictures etc. and NOT to push the button on the Caran.
//
// When calibration is complete, it will either pass or fail.
//
// On PASS, didBecomeCalibrated will be called (success!) The host app can congratulate
// the user on success, etc. and continue.
//
// On FAIL, didFailCalibration will be called. The host app can inform the user and then
// let them try again (call the calibrate function again etc. and repeat) or continue
// without calibrating.
//
//--------------------------------------------------------------------------------
-(void) calibrate;

//--------------------------------------------------------------------------------
//
// takeMeasurement
//
// Use this command to take a measurement, based on touching a button or other
// UI element in the host application. Results will come back through one of the
// "didReceiveMeasurement" protocols based on the measurement mode.
//
// Optional, for ColorReader PRO (the physical button on the device will also trigger a measurement).
// REQUIRED, to take a measurement with ColorReader
//
//--------------------------------------------------------------------------------
-(void) takeMeasurement;

//--------------------------------------------------------------------------------
//
// disconnectFromDevice
//
// The host app calls this to disconnect from Caran.
//
//--------------------------------------------------------------------------------
-(void) disconnectFromDevice;

@end

//--------------------------------------------------------------------------------
//
// Caran SDK protocol
//
//--------------------------------------------------------------------------------

@protocol CaranProtocol <NSObject>

//--------------------------------------------------------------------------------
//
// didReceiveListOfVisibileDevices
//
// Following the host app's call to getListOfVisibleDevices, this is called when
// Bluetooth discovery is complete, and peripheralList is filled with any/all
// Carans that were found. The host app should use the array to dispay a list for the
// user to choose from.
//
//--------------------------------------------------------------------------------
-(void) didReceiveListOfVisibileDevices:(NSMutableArray<CBPeripheral*>*)peripheralList;

//--------------------------------------------------------------------------------
//
// didConnectToDevice
//
// Following the host app's call to connectToDevice, this is called when the connection
// attempt is successful. (Note: The SDK may run one or more commands to the device after
// the connection is established, and didConnectToDevie is not called until after
// these command(s) have completed, and both the device and SDK itself are ready to go).
//
//--------------------------------------------------------------------------------
-(void) didConnectToDevice:(CBPeripheral*) _device;

//--------------------------------------------------------------------------------
//
// didFailToConnectToDevice:
//
// Following the host app's call to connectToDevice, this is called when the connection
// attempt failed. (Atypical, should not happen).
//
//--------------------------------------------------------------------------------
-(void) didFailToConnectToDevice:(CBPeripheral*) _device;

//--------------------------------------------------------------------------------
//
// didDisconnectFromDevice
//
// Following the host app's call to disconnectFromDevice, this is called when
// Caran's internal Bluetooth component state changes to disconnected. It's also
// called if the device was connected and the connection is broken for other
// reasons (Caran goes to sleep; goes out of Bluetooth range; etc).
//
//--------------------------------------------------------------------------------
-(void) didDisconnectFromDevice;

//--------------------------------------------------------------------------------
//
// This is called whenever the Caran button press is detected. Can be used
// to immediately show an indication or progress wheel, etc in the host app while
// waiting for the measurement data to be retrieved (didReceiveMeasurementWith...)
//
// (ColorReader PRO only)
//
//--------------------------------------------------------------------------------
-(void) didGetButtonPress;

//--------------------------------------------------------------------------------
//
// didReceiveMeasurementWithL:andA:andB:
//
// (NOTE: this is used when the measurement mode is modeMeasureLabOnly)
//
// Called after a measurement has occurred, to return measurement data to the host.
// This is triggered automatically by the user pressing the Caran's button while the
// Caran is awake and in a connected state.
//
// This returns only the measured Lab value.
//
// NOTE: There will be a SHORT time gap between the button press and when the measurement
// data has been retrieved from the device. The host app can detect the start of
// the measurement sequence through didGetButtonPress (immediately when the Caran
// button is pressed) and then display indeterminate progress until this method is called).
//
//--------------------------------------------------------------------------------
-(void) didReceiveMeasurementWithL:(NSNumber *)l
                              andA:(NSNumber *)a
                              andB:(NSNumber *)b;

//--------------------------------------------------------------------------------
//
// didReceiveMeasurementWithL:andA:andB:andMatch1:andMatch2:andMatch3:
//
// (NOTE: this is used when the measurement mode is modeMeasureLabPlusMatch)
//
// Called after a measurement has occurred, to return measurement data to the host.
// This is triggered automatically by the user pressing the Caran's button while the
// Caran is awake and in a connected state.
//
// This returns the measured Lab value as well as the best 3 matches from the
// internal fandeck.
//
// NOTE: There will be LONGER time gap between button press and when the measurement
// plus match data is retrieved from the device. The host app can detect the start of
// the measurement sequence through didGetButtonPress (immediately when the Caran
// button is pressed) and then display indeterminate progress until this method is called).
//
// (ColorReader PRO only)
//
//--------------------------------------------------------------------------------
-(void) didReceiveMeasurementWithL:(NSNumber *)l
                              andA:(NSNumber *)a
                              andB:(NSNumber *)b
                         andMatch1:(struct FandeckMatch)m1
                         andMatch2:(struct FandeckMatch)m2
                         andMatch3:(struct FandeckMatch)m3;


//--------------------------------------------------------------------------------
//
// didBecomeUncalibrated
//
// Called during this host app connection session if the device needs calibration.
//
// After each measurement, the calibration state of the connection session is checked,
// and if the internal thresholds have been exceeded, this method is called to notify
// the host app. (after doing this once, it won't be sent again during that connection
// session unless the user calibrates and then exceeds the threshold again in that session).
//
//--------------------------------------------------------------------------------
-(void) didBecomeUncalibrated;

//--------------------------------------------------------------------------------
//
// didPassCalibrationWithWarning
//
// This is called, following a call to calibrate, if calibration was successful.
//
// For SDK 1.1, the API name has changed (it had been, didPassCalibration with no parameters), and
// the new flag "warning" is now returned as an additional qualifier for what "pass" means.
// If warning = NO, then the white tile measurement during calibration was below a certain minimum
// threshold for which there's no warning. If warning = YES, calibration has still "passed", but
// white tile measurement during calibration is in a range, far enough away from the factory stored
// value, where the user should be warned (for instance: "Are you sure that you've calibrated on your
// white calibration tile?".
//
//--------------------------------------------------------------------------------
-(void) didPassCalibrationWithWarning:(BOOL)warning;

//--------------------------------------------------------------------------------
//
// didFailCalibration
//
// This is called, following a call to calibrate, if calibration failed.
//
//--------------------------------------------------------------------------------
-(void) didFailCalibration;

//--------------------------------------------------------------------------------
//
// This is called once, just before internal fandeck downloading starts. totalChunks here
// matches totalChunks that will be sent for each didFandeckDownloadForChunk:ofTotalChunks
// (host app can use this to set scaling for a progress indicator to be filled by
// those calls.
//
// (ColorReader PRO only)
//
//--------------------------------------------------------------------------------
-(void) didStartFandeckDownloadForTotalChunks:(unsigned int)totalChunks;

//--------------------------------------------------------------------------------
//
// This is called repeatedly, during internal fandeck download, so the host app can show progress.
//
// (ColorReader PRO only)
//
//--------------------------------------------------------------------------------
-(void) didFandeckDownloadForChunk:(unsigned int)thisChunk
                     ofTotalChunks:(unsigned int)totalChunks
             withColorsTransferred:(unsigned int)numColors
                     ofTotalColors:(unsigned int)totalColors;

//--------------------------------------------------------------------------------
//
// This is called when the internal fandeck download is complete, with pass/fail.
//
// If PASS, totalChunks of fandeck data will have been successfully downloaded.
// If FAIL, (can be called earlier in the process, without all progress indication
// being done), then something went wrong partway through and the process should be
// started again. Internal state of the device would be incomplete.
//
// Note: After a fandeck dowload completion, the headar info that's exposed will have
// also been updated. The host app should re-get its copy of the header info to bring
// itself up-to-date on the internal state of the device fandeck.
//
// (ColorReader PRO only)
//
//--------------------------------------------------------------------------------
-(void) didCompleteFandeckDownloadWithBytesTransferred:(unsigned long)totalBytesTransferred withSuccess:(bool)success;

//--------------------------------------------------------------------------------
//
// These is called following getCodes.
//
// (There's a slight delay while data is read from the device).
// The host app should wait for this response before updating its UI.
//
// If success = YES, the codes were successfully retrieved and can be shown
// to the user; if NO, the host app can tell the user there was an error retrieving
// the codes and handle the failure as it chooses.
//
// For a factory initialized device which has never had either code written to it,
// the returned NSString's will be the empty string "". The same will be true if there's
// failure to read.
//
//--------------------------------------------------------------------------------
-(void) didGetCodesOEM:(NSString *)oemCode andRegistration:(NSString *)registrationCode withSuccess:(BOOL)success;

//--------------------------------------------------------------------------------
//
// This is called following setCodesWithOEMCode:andRegistrationCode:
//
// (There's a slight delay while data is written to the device).
// The host app should wait for this response before updating its UI.
//
// If success = YES, it can show a message saying the OEM and Registration
// codes were successfully written; if NO, it could tell the user that the attempt to
// save the codes into the device failed, please try again, etc.
//
// For verification: the host app could then call getCodes again to re-read the
// two codes from the device and display them.
//
//--------------------------------------------------------------------------------
-(void) didSetCodesWithSuccess:(BOOL)success;


@required

@optional


@end
