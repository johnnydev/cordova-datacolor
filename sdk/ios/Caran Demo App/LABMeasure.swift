//
//  LABMeasure.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 03/10/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import Foundation
import UIKit

class LABMeasure : NSObject, NSCoding {
    
    
    // MARK: Properties
    
    private(set) var lab_l : Float!
    private(set) var lab_a : Float!
    private(set) var lab_b : Float!
    private(set) var name : String!
    private(set) var date : Date
    private(set) var matchArr : [MeasureMatch]
        
    // MARK: Types
    struct PropertyKey {
        static let nameKey = "name"
        static let lab_lKey = "lab_l"
        static let lab_aKey = "lab_a"
        static let lab_bKey = "lab_b"
        static let dateKey = "date"
        static let matchArrKey = "matchArr"
    }
    
    
    // MARK: Init
    init(_l : Float, _a : Float, _b : Float, _matchArr : [MeasureMatch]){
     
        name = ""
        
        lab_l = _l
        lab_a = _a
        lab_b = _b
        
        date = Date()
        
        matchArr = _matchArr
        
        super.init()
    }
    
    init(_l : Float, _a : Float, _b : Float, _name : String) {
    
        name = _name
        
        lab_l = _l
        lab_a = _a
        lab_b = _b
        
        date = Date()
        
        matchArr = []
        
        super.init()
    }
    
    init(_l : Float, _a : Float, _b : Float, _name : String, _date : Date, _matchArr : [MeasureMatch]) {
        
        name = _name
        
        lab_l = _l
        lab_a = _a
        lab_b = _b
        
        date = _date
        
        matchArr = _matchArr
        
        super.init()
    }
    
    
    // MARK: NSCoding
    
    func encode(with aCoder:NSCoder) {
        
        aCoder.encode(name, forKey: PropertyKey.nameKey)
        
        aCoder.encode(lab_l, forKey: PropertyKey.lab_lKey)
        aCoder.encode(lab_a, forKey: PropertyKey.lab_aKey)
        aCoder.encode(lab_b, forKey: PropertyKey.lab_bKey)
        
        aCoder.encode(date, forKey: PropertyKey.dateKey)
        
        aCoder.encode(matchArr, forKey: PropertyKey.matchArrKey)
     }
    
    required convenience init?(coder aDecoder: NSCoder) {
        
        let name = aDecoder.decodeObject(forKey: PropertyKey.nameKey) as! String
        let l = aDecoder.decodeObject(forKey: PropertyKey.lab_lKey) as! Float
        let a = aDecoder.decodeObject(forKey: PropertyKey.lab_aKey) as! Float
        let b = aDecoder.decodeObject(forKey: PropertyKey.lab_bKey) as! Float
        var date = aDecoder.decodeObject(forKey: PropertyKey.dateKey) as? Date
        var arr = aDecoder.decodeObject(forKey: PropertyKey.matchArrKey) as? [MeasureMatch]
        
        
        if(date == nil){
            date = Date()
        }
        
        if(arr == nil){
            arr = []
        }
                
        self.init(_l : l, _a : a, _b : b, _name : name, _date: date!, _matchArr: arr!)
        
    }
    
    // MARK: Archiving Paths
    private static let DocDir = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocDir.appendingPathComponent("measurements")
    
}
