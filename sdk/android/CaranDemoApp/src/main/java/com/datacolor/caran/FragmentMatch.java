//
//  FragmentMatch.java
//  Caran Demo App
//
//  Copyright © 2017 Datacolor. All rights reserved.
//

package com.datacolor.caran;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.datacolor.DCICaranLib;
import com.datacolor.caran.model.LABMeasure;

import java.util.ArrayList;
import java.util.List;

public class FragmentMatch extends FragmentBase {
    interface FragmentMatchDelegate {
        void fragmentMatchDidRequestBack(FragmentMatch sender);
        void fragmentMatchDidRequestList(FragmentMatch sender);
        LABMeasure fragmentMatchMeasurement(FragmentMatch sender);
    }

    private ListView fandeckMatchesListView = null;

    private LABMeasure measurement = null;

    private class FandeckMatchItem {
        DCICaranLib.FandeckMatch match;

        public FandeckMatchItem(DCICaranLib.FandeckMatch match) {
            this.match = match;
        }
    }

    private class ItemsAdapter extends ArrayAdapter<FandeckMatchItem> {
        public View view;
        public ItemsAdapter(Context context, List<FandeckMatchItem> objects){
            super(context, R.layout.list_item_fandeck_match, objects);
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup parent) {
            this.view = convertView;
            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.list_item_fandeck_match, null);
            }

            final FandeckMatchItem item = getItem(pos);
            DCICaranLib.FandeckMatch match = item.match;
            LABMeasure colorUtil = new LABMeasure(match.getL(), match.getA(), match.getB());

            ((View)(view.findViewById(R.id.color_large))).setBackgroundColor(colorUtil.getColor());
            ((ImageView)(view.findViewById(R.id.color))).setColorFilter(measurement.getColor());
            ((TextView) (view.findViewById(R.id.delta_e))).setText(String.format("%.2f", match.getDeltaE())); // was Float.toString(match.getDeltaE())
            ((TextView) (view.findViewById(R.id.color_name))).setText(match.getName());
            ((TextView) (view.findViewById(R.id.fandeck_name))).setText(match.getFDName());

            if (match.getL() > 55) {
                ((TextView) (view.findViewById(R.id.delta_e_title))).setTextColor(Color.parseColor("#2C2C2C"));
                ((TextView) (view.findViewById(R.id.delta_e))).setTextColor(Color.parseColor("#000000"));
            } else {
                ((TextView) (view.findViewById(R.id.delta_e_title))).setTextColor(Color.parseColor("#D3D3D3"));
                ((TextView) (view.findViewById(R.id.delta_e))).setTextColor(Color.parseColor("#FFFFFF"));
            }

            return view;
        }
    }


    private ArrayList<FandeckMatchItem> fandeckMatchesList = null;
    private ItemsAdapter fandeckMatchesAdapter = null;

    @Override
    protected int layoutId() {
        return R.layout.fragment_match;
    }


    @Override
    protected void init() {
        if (getMainActivity().isConnectedToDevice() == false) {
            this.enableBackButton();
        }
        this.setTitle("Match");
        this.enableListButton();

        fandeckMatchesListView = (ListView) getView().findViewById(R.id.list_view);

        fandeckMatchesList = new ArrayList<FandeckMatchItem>();
        fandeckMatchesAdapter = new ItemsAdapter(getMainActivity(), fandeckMatchesList);

        fandeckMatchesListView.setAdapter(fandeckMatchesAdapter);
    }


    @Override
    public void reload() {
        this.measurement = getMainActivity().fragmentMatchMeasurement((FragmentMatch)self);
        if ((measurement == null) || (measurement.getMatches() == null)) {
            getView().findViewById(R.id.no_matching_data).setVisibility(View.VISIBLE);
            fandeckMatchesListView.setVisibility(View.GONE);
        } else {
            fandeckMatchesList.clear();

            getView().findViewById(R.id.no_matching_data).setVisibility(View.GONE);
            fandeckMatchesListView.setVisibility(View.VISIBLE);

            DCICaranLib.FandeckMatch[] matches = measurement.getMatches();
            for (DCICaranLib.FandeckMatch match : matches)
                fandeckMatchesList.add(new FandeckMatchItem(match));

            fandeckMatchesListView.invalidateViews();
        }
    }


    @Override
    protected void navigateBack() {
        getMainActivity().fragmentMatchDidRequestBack(this);
    }


    @Override
    protected void requestList() {
        getMainActivity().fragmentMatchDidRequestList(this);
    }
}