//
//  UploadProgressViewController.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 04/11/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import UIKit

class UploadProgressViewController: UIViewController {

    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: PROPERTIES
    // MARK:
    //--------------------------------------------------------------------------------
    
    @IBOutlet weak var progressBar: UIProgressView!
    private var totalChunks = 0
    private var thisChunk = 0
    private var totalColors = 0
    private var transferredColors = 0
    
   
    private var success = false
    @IBOutlet weak var timePanel: UIView!
    @IBOutlet weak var colorsPanel: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var colorsLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var fandecksLabel: UILabel!
    
    private var time0:Date?
    private var time1:Date?
    private var timeInterval:TimeInterval?
    
    public var fandecks:[Fandeck]?
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: OVERRIDES
    // MARK:
    //--------------------------------------------------------------------------------
    
    // Remove the observers
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fandecks = CSVFandecksListViewController.StaticSelectedFandecks
        
        if((fandecks?.count)! > 1){
            titleLabel.text = "Downloading Fandecks"
        }else{
            titleLabel.text = "Downloading Fandeck"
        }
        
        let fandecksNames = fandecks!.map { $0.name }        
        fandecksLabel.text = fandecksNames.joined(separator: ", ")
        
        // Disable sleep mode
        UIApplication.shared.isIdleTimerDisabled = true
        
        // reset the progress bar
        self.updateProgress(total: 0, progress: 0)
        
        
        // Receive download start
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didStartFandeckDownload),
            name: NSNotification.Name(rawValue: "DidStartFandeckDownloadNotification"),
            object: nil)
        
        // Receive download progress
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didFandeckDownload),
            name: NSNotification.Name(rawValue: "DidFandeckDownloadNotification"),
            object: nil)
        
        // Receive download completed
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didCompleteFandeckDownload),
            name: NSNotification.Name(rawValue: "DidCompleteFandeckDownloadNotification"),
            object: nil)
        
        // Disconnect Notification
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didDisconnect),
            name: NSNotification.Name(rawValue: "DisconnectFromDeviceNotification"),
            object: nil)
        
        
        // hide the info panels
        timePanel.isHidden = false
        timeLabel.text = "-"
        colorsPanel.isHidden = false
        colorsLabel.text = "-"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(CaranController.instance.connectedDevice == nil){
            forceFail()
        }
    }
    

    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: METHODS
    // MARK:
    //--------------------------------------------------------------------------------
    
    
    func didStartFandeckDownload(notification: NSNotification) {
        
        print("UploadProgressViewController received DidStartFandeckDownloadNotification")
        
        // Get the parameters
        let userInfo = notification.userInfo as! [String: Any]
        totalChunks = userInfo["totalChunks"] as! Int
        
    }
    
    func didFandeckDownload(notification: NSNotification) {
        
        print("UploadProgressViewController received DidFandeckDownloadNotification")
        
        // Get the parameters
        let userInfo = notification.userInfo as! [String: Any]
        totalChunks = userInfo["totalChunks"] as! Int
        thisChunk = userInfo["thisChunk"] as! Int
        transferredColors = userInfo["transferredColors"] as! Int
        totalColors = userInfo["totalColors"] as! Int
        
        updateProgress(total: totalChunks,progress: thisChunk)
        
    }
    
    func didCompleteFandeckDownload(notification: NSNotification) {
        
        print("UploadProgressViewController received DidCompleteFandeckDownloadNotification")
        
        // Get the parameters
        let userInfo = notification.userInfo as! [String: Any]
        success = userInfo["success"] as! Bool
        
        presentResultView()
    }
    
    func presentResultView(){
    
        NotificationCenter.default.removeObserver(self)
        
        let resultVC = self.storyboard!.instantiateViewController(withIdentifier: "UploadResult") as! UploadResultViewController
        resultVC.uploadSucceded = success
        self.navigationController?.pushViewController(resultVC, animated: true)
        
    }
    
    func updateProgress(total:Int,progress:Int){
    
        // get the download time for the first chunk
        if(progress == 1){
            time0 = Date()
        }
        if(progress == 2){
            time1 = Date()
            timeInterval = time1!.timeIntervalSince(time0!)
            CaranController.instance.setDownloadTimeForChunk(time: timeInterval!)
        }
       
        if(progress >= 2){
            displayRemainingTime(total: total, progress: progress)
        }
        
        if(total == 0){
            self.progressBar.setProgress(0, animated: false)
        }else{
            let progressBarProgress = (Float(progress) / Float(total)) as Float
//            print("total: " + total.description)
//            print("progress: " + progress.description)
//            print("progressBarProgress: " + progressBarProgress.description)
            self.progressBar.setProgress(progressBarProgress, animated: true)
        }
    
        
       colorsLabel.text = "\(transferredColors)/\(totalColors)"
        
    }
    
    func displayRemainingTime(total:Int,progress:Int){
        
        let remaining = total - progress
        let remainingTime = Double(remaining) * timeInterval!
        
        timeLabel.text = DCUtils.instance.stringFromTimeInterval(time: remainingTime)
        
    }
        
    
    func didDisconnect(notification: NSNotification) {
       print("UploadProgressViewController was notified of the disconnection")
        forceFail()
    }
    
    
    func forceFail(){
        success = false
        presentResultView()
    }
    
}
