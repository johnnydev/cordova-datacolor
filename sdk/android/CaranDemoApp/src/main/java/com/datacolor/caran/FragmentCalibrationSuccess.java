//
//  FragmentCalibrationSuccess.java
//  Caran Demo App
//
//  Copyright © 2017 Datacolor. All rights reserved.
//

package com.datacolor.caran;


import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;

public class FragmentCalibrationSuccess extends FragmentBase {
    interface FragmentCalibrationSuccessDelegate {
        void fragmentCalibrationSuccessDidRequestDismiss(FragmentCalibrationSuccess sender);
    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_calibration_success;
    }


    @Override
    protected void init() {
        this.setTitle("Calibration");
        this.enableCloseButton();

        if (getMainActivity().calibrationWarning) {
            ((TextView) getView().findViewById(R.id.calibrated)).setText("Your device is now calibrated and ready to measure (however: please make sure that your device is on the calibration tile)");
        }

        ((Button) getView().findViewById(R.id.button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }


    @Override
    protected void dismiss() {
        getMainActivity().fragmentCalibrationSuccessDidRequestDismiss(this);
    }
}