//
//  CRNFandeck.h
//  DCICaran
//
//  Created by David Miller on 11/10/16.
//  Copyright © 2016 Datacolor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CRNFandeckColor.h"

@interface CRNFandeck : NSObject

// Default initializer. One way of building a Fandeck object is to create one
// this way, fill in its properties, then create an array of FandeckColors and
// set that array into its fandeckColors property.

- (id)init;

// This initializer can be used to init the Fandeck object from CSV and it will
// create itelf, and all contents, based on parsing that CSV in a known format.
// See the sample CSVs for the expected format.

- (id)initWithCSV:(NSString *)theCSV;

@property (nonatomic) NSString* libName;        // Fandeck names should be 10 chars max. Anything longer will be truncated to the leftmost 10 chars during download to Caran.
@property (nonatomic) NSString* GUID;           // This should be a GUID up to 40 chars max.
@property (nonatomic) unsigned short version;   // An internal version number of your choice
@property (nonatomic) unsigned short numColors; // The total number of colors in this fandeck
@property (nonatomic) NSArray* fandeckColors;   // An array of FandeckColors


@end
