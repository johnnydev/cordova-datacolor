//
//  FragmentPeripheralList.java
//  Caran Demo App
//
//  Copyright © 2017 Datacolor. All rights reserved.
//

package com.datacolor.caran;


import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.datacolor.DCICaranLib;

import java.util.ArrayList;
import java.util.List;

public class FragmentPeripheralList extends FragmentBase {
    interface FragmentPeripheralListDelegate {
        ArrayList<BluetoothDevice> fragmentPeripheralListPeripherals(FragmentPeripheralList sender);
        void fragmentPeripheralListDidSelectPeripheral(FragmentPeripheralList sender, BluetoothDevice peripheral);
    }

    private ListView peripheralsListView = null;

    private class PeripheralItem {
        public BluetoothDevice peripheral;
        public String title;

        public PeripheralItem(BluetoothDevice peripheral) {
            this.peripheral = peripheral;
            this.title = peripheral.getName();
        }
    }

    private class ItemsAdapter extends ArrayAdapter<PeripheralItem> {
        public View view;
        public ItemsAdapter(Context context, List<PeripheralItem> objects) {
            super(context, R.layout.list_item_peripheral, objects);
        }

        @Override
        public View getView(int pos, View convertView, ViewGroup parent) {
            this.view = convertView;
            if (view == null) {
                view = getLayoutInflater().inflate(R.layout.list_item_peripheral, null);
            }

            final PeripheralItem item = getItem(pos);

            ((TextView) (view.findViewById(R.id.title))).setText(item.title);

            return view;
        }
    }


    private ArrayList<PeripheralItem> peripheralsList = null;
    private ItemsAdapter peripheralsAdapter = null;


    @Override
    protected int layoutId() {
        return R.layout.fragment_peripheral_list;
    }


    @Override
    protected void init() {
        this.setTitle("Devices");

        peripheralsListView = (ListView) getView().findViewById(R.id.list_view);

        peripheralsList = new ArrayList<PeripheralItem>();
        peripheralsAdapter = new ItemsAdapter(getMainActivity(), peripheralsList);

        peripheralsListView.setAdapter(peripheralsAdapter);

        peripheralsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PeripheralItem item = peripheralsList.get(position);
                getMainActivity().fragmentPeripheralListDidSelectPeripheral((FragmentPeripheralList) self, item.peripheral);
            }
        });

        this.reload();
    }


    @Override
    public void reload() {
        peripheralsList.clear();

        ArrayList<BluetoothDevice> peripherals = getMainActivity().fragmentPeripheralListPeripherals(this);
        for (BluetoothDevice peripheral : peripherals)
            peripheralsList.add(new PeripheralItem(peripheral));

        peripheralsListView.invalidateViews();
    }
}