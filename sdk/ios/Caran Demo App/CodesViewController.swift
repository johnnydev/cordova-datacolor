//
//  CodesViewController.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 07/12/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import UIKit

class CodesViewController: UIViewController, UITextFieldDelegate {

    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: PROPERTIES and OUTLETS
    // MARK:
    //--------------------------------------------------------------------------------
    var oemCode:String = ""
    var registrationCode:String = ""
    
    @IBOutlet weak var closeButton: UIBarButtonItem!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var oemTextField: UITextField!
    
    @IBOutlet weak var registrationTextField: UITextField!
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: OVERRIDES
    // MARK:
    //--------------------------------------------------------------------------------
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()


        // set up the fields
        oemTextField.delegate = self
        registrationTextField.delegate = self
        
        oemTextField.text = self.oemCode
        registrationTextField.text = self.registrationCode

        //
        errorLabel.isHidden = true
        
        // Disconnect Notification
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didDisconnect),
            name: NSNotification.Name(rawValue: "DisconnectFromDeviceNotification"),
            object: nil)
        
        // Receive Get Codes event
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didGetCodes),
            name: NSNotification.Name(rawValue: "DidGetCodes"),
            object: nil)
        
        // Receive Set Codes event
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didSetCodes),
            name: NSNotification.Name(rawValue: "DidSetCodes"),
            object: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: METHODS
    // MARK:
    //--------------------------------------------------------------------------------
    
    @IBAction func btnSaveCodePressed(_ sender: Any) {
        
        btnSave.isEnabled = false
        closeButton.isEnabled = false
        
        var newOemCode = ""
        var newRegistrationCode = ""
        
        if(registrationTextField.text != nil){
            newRegistrationCode = registrationTextField.text!
        }
        if(oemTextField.text != nil){
            newOemCode = oemTextField.text!
        }
        
        CaranController.instance.writeCodes(_oemCode: newOemCode, _registrationCode: newRegistrationCode)
        
    }
    
    func didDisconnect(notification: NSNotification) {
        if(CaranController.instance.connectedDevice == nil)
        {
            _ = self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
 
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: CARAN - CODES Protocol
    // MARK:
    //--------------------------------------------------------------------------------
    func didGetCodes(notification: NSNotification) {
        
        
        //
        btnSave.isEnabled = true
        closeButton.isEnabled = true
        
        // Get the parameter
        let userInfo = notification.userInfo as! [String: Any]
        let success = userInfo["success"] as! Bool?
        let oemCode = userInfo["oemCode"] as! String?
        let registrationCode = userInfo["registrationCode"] as! String?
        
        if(success!){
            print("didGetCodes: success")
            self.errorLabel.isHidden = true
            
            self.oemTextField.text = oemCode!
            self.registrationTextField.text = registrationCode!
        
            self.dismiss(animated: true, completion: nil)
        
        }
        else{
            print("didGetCodes: fail")
            self.errorLabel.text = "Failed to get the codes"
            self.errorLabel.isHidden = false
        }
    }
    
    func didSetCodes(notification: NSNotification) {
        
        // Get the parameter
        let userInfo = notification.userInfo as! [String: Any]
        let success = userInfo["success"] as! Bool?
        
        if(success!){
            print("didSetCodes: success")
            self.errorLabel.isHidden = true
            
            // if the write operation succeded, call the getCodes again
            CaranController.instance.getCodes()
        }
        else{
            print("didSetCodes: fail")
            self.errorLabel.text = "Failed to write the codes"
            self.errorLabel.isHidden = false
        }
    }
    
    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: TextField Protocol
    // MARK:
    //--------------------------------------------------------------------------------
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (textField.text?.characters.count)! >= 16 && range.length == 0 {
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
