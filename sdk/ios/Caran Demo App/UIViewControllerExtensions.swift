//
//  UIViewControllerExtensions.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 13/10/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func presentViewControllerFromVisibleViewController(viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)?) {
        if self is UINavigationController {
            let navigationController = self as! UINavigationController
            navigationController.topViewController?.presentViewControllerFromVisibleViewController(viewControllerToPresent: viewControllerToPresent, animated: true, completion: nil)
        } else if (presentedViewController != nil) {
            presentedViewController!.presentViewControllerFromVisibleViewController(viewControllerToPresent: viewControllerToPresent, animated: true, completion: nil)
        } else {
            present(viewControllerToPresent, animated: true, completion: nil)
        }
    }
}
