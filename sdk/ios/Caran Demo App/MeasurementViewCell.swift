//
//  MatchTableViewCell.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 05/10/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import UIKit

class MeasurementViewCell: UITableViewCell {

    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: PROPERTIES
    // MARK:
    //--------------------------------------------------------------------------------
    
    @IBOutlet weak var MeasureView: UIView!
    @IBOutlet weak var MeasureLabel: UILabel!
    
    
    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: OVERRIDES
    // MARK:
    //--------------------------------------------------------------------------------
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
                
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
