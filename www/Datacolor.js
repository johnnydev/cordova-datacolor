const exec = require('cordova/exec')
// const cordova = require('cordova')

function run(method) {
  return function (args) {
    console.log('[datacolor js] ' + method)
    return new Promise(function(res, rej) {
      if (args) {
        exec(function(ret) {
          console.log("PROMISE OK " + method)
          res(ret)
        }, function(e){ console.log("PROMISE NON OK " + method); rej(e)}, 'Datacolor', method, args)
      } else {
        exec(res, rej, 'Datacolor', method)
      }
    })
  } 
}

let initialized = false
let handlers = {}

function eventHandler(args) {
  const eventName = args.event
  if (handlers[eventName]) {
    handlers[eventName](args.args)
  } else {
    console.log('possibile che non ho trovato un evento')
  }
}

console.log('DENTRO DATACOLOR.js!')
const Datacolor = {
  initialize: () => {
    if (initialized) return false
    initialized = true
    exec(eventHandler, e => { console.log('[event handler error js] ', e)}, 'Datacolor', 'eventsHandler');
    // run('eventsHandler')().then(eventHandler).catch( e => {console.error(e)})
    return run('initialize')()
  },
  isBluetoothOn: run('isBlueToothPoweredOn'),
  sendFandeck: colors => run('sendFandeck')([colors]),
  scan: run('scanDevices'),
  calibrate: run('calibrateDevice'),
  connect: deviceName => run('connectToDevice')([deviceName]),
  measure: run('takeMeasure'),
  needsCalibration: run('needsCalibration'),
  on: function (event, cb) {
    console.log('dentro on di ', event)
    handlers[event] = cb
  }
}

module.exports = Datacolor
