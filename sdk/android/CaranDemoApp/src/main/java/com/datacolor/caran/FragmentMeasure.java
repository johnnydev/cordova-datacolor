//
//  FragmentMeasure.java
//  Caran Demo App
//
//  Copyright © 2017 Datacolor. All rights reserved.
//

package com.datacolor.caran;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.datacolor.DCICaranLib;
import com.datacolor.caran.model.LABMeasure;

import static android.support.v4.graphics.ColorUtils.LABToColor;

public class FragmentMeasure extends FragmentBase {
    interface FragmentMeasureDelegate {
        void fragmentMeasureDidRequestBack(FragmentMeasure sender);
        void fragmentMeasureDidRequestList(FragmentMeasure sender);
        void fragmentMeasureDidTouchMeasureButton(FragmentMeasure sender);
        LABMeasure fragmentMeasureMeasurement(FragmentMeasure sender);
    }

    // 4.18.17, we're going to support the button officially for regular Caran, too, in ColorReader PRO future
    // builds, so we'll start showing it in the Demo App, too. (bring over earlier change from 4.10.17
    // in the iOS version.
    private static final boolean alwaysShowMeasureButton = true;

    @Override
    protected int layoutId() {
        return R.layout.fragment_measure;
    }

    @Override
    protected void init() {
        if (getMainActivity().isConnectedToDevice() == false) {
            this.enableBackButton();
        }
        this.setTitle("Measure");
        this.enableListButton();

        DCICaranLib dciCaranLib = getDciCaranLib();
        // Check our flag so that we can start showing the soft Measure
        // button in the app, always.
        if (alwaysShowMeasureButton) {
            ((TextView)getView().findViewById(R.id.measure_caran_ready)).setText("Caran is ready!");
            ((TextView)getView().findViewById(R.id.match_this_option)).setText("Press the device button, or use the Measure button below, to measure");
            // Enable the buttons if they're going to be visible.
            ((Button) getView().findViewById(R.id.button3)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getMainActivity().fragmentMeasureDidTouchMeasureButton((FragmentMeasure) self);
                }
            });
            ((Button) getView().findViewById(R.id.button4)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getMainActivity().fragmentMeasureDidTouchMeasureButton((FragmentMeasure) self);
                }
            });
        }
        else {
            // If regular Caran, and if we want to hide the new Measure button, then
            // make both of the Measure buttons invisible.
            ((Button) (getView().findViewById(R.id.button3))).setVisibility(View.INVISIBLE);
            ((Button) (getView().findViewById(R.id.button4))).setVisibility(View.INVISIBLE);
        }

    }

    @Override
    public void reload() {
        LABMeasure measurement = getMainActivity().fragmentMeasureMeasurement((FragmentMeasure)self);
        if (measurement == null) {
            // If no stored measurements, nothing to display. Show the "empty" layout and get rid of the other. (Using GONE
            // for that takes it out of the layout stack and then the "empty" view gets the entire screen.
            getView().findViewById(R.id.no_matching_data).setVisibility(View.VISIBLE);
            getView().findViewById(R.id.measurement_data).setVisibility(View.GONE);
        } else {
            // If we already have a measurement, get rid of the "empty" layout and we'll just be showing the measurement layout instead.
            getView().findViewById(R.id.no_matching_data).setVisibility(View.GONE);
            getView().findViewById(R.id.measurement_data).setVisibility(View.VISIBLE);

            (getView().findViewById(R.id.color)).setBackgroundColor(measurement.getColor());

            ((TextView)getView().findViewById(R.id.color_lab_l)).setText(String.format("%.2f", measurement.getLabL()));
            ((TextView)getView().findViewById(R.id.color_lab_a)).setText(String.format("%.2f", measurement.getLabA()));
            ((TextView)getView().findViewById(R.id.color_lab_b)).setText(String.format("%.2f", measurement.getLabB()));

            ((TextView)getView().findViewById(R.id.color_rgb_r)).setText(String.format("%d", measurement.getRgbR()));
            ((TextView)getView().findViewById(R.id.color_rgb_g)).setText(String.format("%d", measurement.getRgbG()));
            ((TextView)getView().findViewById(R.id.color_rgb_b)).setText(String.format("%d", measurement.getRgbB()));

            ((TextView)getView().findViewById(R.id.color_hex)).setText(measurement.getHex());

        }
    }


    @Override
    protected void navigateBack() {
        getMainActivity().fragmentMeasureDidRequestBack(this);
    }


    @Override
    protected void requestList() {
        getMainActivity().fragmentMeasureDidRequestList(this);
    }


}