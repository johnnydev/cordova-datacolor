//
//  CRNFandeckColor.h
//  DCICaran
//
//  Created by David Miller on 11/10/16.
//  Copyright © 2016 Datacolor. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CRNFandeckColor : NSObject

// Default initializer. You can simply create a FandeckColor and fill in its properties.
// Create many of these, put them in an NSArray and then set that into a Fandeck object.

- (id)init;

// This initializer can be used to pass the FandeckColor object a name and Lab value,
// a more convenient shorthand vs. using the default initializer and filling these in
// afterward.

- (id)initWithName:(NSString *)theName andL:(float)L andA:(float)a andB:(float)b;

@property (nonatomic) NSString* name;   // Color names should be 11 chars max. Anything longer will be truncated to the rightmost 11 chars during download to Caran.
@property (nonatomic) float L;          // Measured Lab of the color.
@property (nonatomic) float a;
@property (nonatomic) float b;


@end
