//
//  FragmentCalibrate.java
//  Caran Demo App
//
//  Copyright © 2017 Datacolor. All rights reserved.
//

package com.datacolor.caran;


import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class FragmentCalibrate extends FragmentBase {
    interface FragmentCalibrateDelegate {
        void fragmentCalibrateDidRequestDismiss(FragmentCalibrate sender);
        void fragmentCalibrateDidRequestCalibrate(FragmentCalibrate sender);
    }

    @Override
    protected int layoutId() {
        return R.layout.fragment_calibrate;
    }


    @Override
    protected void init() {
        this.setTitle("Calibration");
        this.enableCloseButton();

        ((Button) getView().findViewById(R.id.button)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getMainActivity().fragmentCalibrateDidRequestCalibrate((FragmentCalibrate) self);
            }
        });
    }


    @Override
    protected void dismiss() {
        getMainActivity().fragmentCalibrateDidRequestDismiss(this);
    }
}