//
//  MeasureViewController.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 29/09/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import UIKit

class MeasureViewController: UIViewController {

    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: PROPERTIES
    // MARK:
    //--------------------------------------------------------------------------------
    
    @IBOutlet weak var PreviewView: UIView!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var itemLabL: LabelAndValueView!
    @IBOutlet weak var itemLabA: LabelAndValueView!
    @IBOutlet weak var itemLabB: LabelAndValueView!
    @IBOutlet weak var itemRgbR: LabelAndValueView!
    @IBOutlet weak var itemRgbG: LabelAndValueView!
    @IBOutlet weak var itemRgbB: LabelAndValueView!
    @IBOutlet weak var itemHex: LabelAndValueView!
    @IBOutlet weak var MeasureButton: UIButton! // Our new outlet for the new Measure button on the screen
    
    private var displayedMeasure : LABMeasure?

    // 4.10.17, we're going to support the button officially for regular Caran, too, in ColorReader PRO future
    // builds, so we'll start showing it in the Demo App, too.
    private var alwaysShowMeasureButton : Bool = true;
    
    
    @IBOutlet weak var labelEmptyStateTitle: UILabel!
    @IBOutlet weak var labelEmptyStateText: UILabel!
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: OVERRIDES
    // MARK:
    //--------------------------------------------------------------------------------
    
    
    // Remove the observers
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        self.tabBarController?.title = "Measure"
        
        // Receive measure
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didReceiveMeasurement),
            name: NSNotification.Name(rawValue: "MeasureFromDeviceNotification"),
            object: nil)
               
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.tabBarController?.title = "Measure"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "List", style: .plain, target: self, action: #selector(listTapped))
        
        // restore the navigation title
        self.tabBarController?.navigationItem.titleView = nil
        self.tabBarController?.navigationItem.title = "Measure"
        
        displayMeasureOrEmptyState()
        
    }

    // This will be our action for the new Measure button at the bottom of the Measure screen
    @IBAction func measureButtonPressed(_ sender: Any) {
        
        // Send the take measurement command.
        CaranController.instance.takeMeasurement()

    }
    
    
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: METHODS
    // MARK:
    //--------------------------------------------------------------------------------
    
    func displayMeasureOrEmptyState(){
        
        // change the empty state text if no device is connected
        if(CaranController.instance.connectedDevice == nil){
            labelEmptyStateTitle.text = "No device connected!"
            labelEmptyStateText.text = "Hit the List button to browse the stored measurements"
        }else{
            labelEmptyStateTitle.text = "Caran is ready!"
            labelEmptyStateText.text = "Press the device button, or use the Measure button below, to measure"
            MeasureButton.isHidden = !alwaysShowMeasureButton;
       }
        
        // disable matches view if no matches are available
        if(CaranController.instance.lastMeasure == nil){
            emptyView.isHidden = false
        }
        else {
            populateWithMeasure(aMeasure: CaranController.instance.lastMeasure!)
            emptyView.isHidden = true
        }

    }
    
    func listTapped() {
    
        let modalViewController = self.storyboard!.instantiateViewController(withIdentifier: "MeasurementsView")
        modalViewController.modalPresentationStyle = .fullScreen
        self.present(modalViewController, animated: true, completion: nil)
    
    }
    
    
    func didReceiveMeasurement(notification: NSNotification) {
        
        print("MeasureViewController was notified of a measurement")
        
        // Get the parameter
        let userInfo = notification.userInfo as! [String: Any]
        let labMeasure = userInfo["LABMeasure"] as! LABMeasure?
        
        populateWithMeasure(aMeasure: labMeasure!)
        
    }
    
    func populateWithMeasure(aMeasure : LABMeasure){
    
        if(displayedMeasure == aMeasure) {
            return;
        }
        
        displayedMeasure = aMeasure
        
        // update the view
        let measureUtils = LABMeasureUtils(_labMeasure: aMeasure)
        
        PreviewView.backgroundColor = measureUtils.uicolor
        
        (itemLabL as LabelAndValueView).setValue(_value: measureUtils.lab_l_string)
        (itemLabA as LabelAndValueView).setValue(_value: measureUtils.lab_a_string)
        (itemLabB as LabelAndValueView).setValue(_value: measureUtils.lab_b_string)
        (itemRgbR as LabelAndValueView).setValue(_value: measureUtils.rgb_r_string)
        (itemRgbG as LabelAndValueView).setValue(_value: measureUtils.rgb_g_string)
        (itemRgbB as LabelAndValueView).setValue(_value: measureUtils.rgb_b_string)
        (itemHex as LabelAndValueView).setValue(_value: (measureUtils.hex))
        
        
        // animate
        Animations.ZoomIn(viewToAnimate: self.PreviewView)
        
        
        // hide the empty view
        emptyView.isHidden = true

        
    }

}
