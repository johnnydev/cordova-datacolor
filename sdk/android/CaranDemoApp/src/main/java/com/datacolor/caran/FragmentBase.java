//
//  FragmentBase.java
//  Caran Demo App
//
//  Copyright © 2017 Datacolor. All rights reserved.
//

package com.datacolor.caran;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.datacolor.DCICaranLib;

import org.w3c.dom.Text;

public abstract class FragmentBase extends Fragment {
    private MainActivity _mainActivity = null;
    private View _view = null;
    protected FragmentBase self = null;

    protected MainActivity getMainActivity() {
        if (_mainActivity != null)
            return _mainActivity;
        return (MainActivity) getActivity();
    }
    protected LayoutInflater getLayoutInflater() {
        return LayoutInflater.from(getMainActivity());
    }
    protected DCICaranLib getDciCaranLib () {
        return getMainActivity().getDciCaranLib();
    }
    public View getView() {
        return _view;
    }
    public void reload() {}
    protected void navigateBack() {}
    protected void dismiss() {}
    protected void requestList() {}

    protected void setTitle(String title) {
        View navigationBar = getView().findViewById(R.id.navigation_bar);
        if (navigationBar != null) {
            ((TextView)(navigationBar.findViewById(R.id.title))).setText(title);
        }
    }

    protected void enableBackButton() {
        View navigationBar = getView().findViewById(R.id.navigation_bar);
        if (navigationBar != null) {
            Button backButton = (Button) navigationBar.findViewById(R.id.back);
            if (backButton == null) {
                ViewGroup accessoryLeft = (ViewGroup) navigationBar.findViewById(R.id.accessory_left);
                accessoryLeft.removeAllViews();
                getLayoutInflater().inflate(R.layout.nav_button_back, accessoryLeft, true);
                backButton = (Button) accessoryLeft.findViewById(R.id.back);
                backButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        navigateBack();
                    }
                });
            }
        }
    }

    protected void enableCloseButton() {
        View navigationBar = getView().findViewById(R.id.navigation_bar);
        if (navigationBar != null) {
            Button closeButton = (Button) navigationBar.findViewById(R.id.close);
            if (closeButton == null) {
                ViewGroup accessoryRight = (ViewGroup) navigationBar.findViewById(R.id.accessory_right);
                accessoryRight.removeAllViews();
                getLayoutInflater().inflate(R.layout.nav_button_close, accessoryRight, true);
                closeButton = (Button) accessoryRight.findViewById(R.id.close);
                closeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                });
            }
        }
    }

    protected void enableListButton() {
        View navigationBar = getView().findViewById(R.id.navigation_bar);
        if (navigationBar != null) {
            Button listButton = (Button) navigationBar.findViewById(R.id.list);
            if (listButton == null) {
                ViewGroup accessoryRight = (ViewGroup) navigationBar.findViewById(R.id.accessory_right);
                accessoryRight.removeAllViews();
                getLayoutInflater().inflate(R.layout.nav_button_list, accessoryRight, true);
                listButton = (Button) accessoryRight.findViewById(R.id.list);
                listButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestList();
                    }
                });
            }
        }
    }

    protected abstract int layoutId();
    protected abstract void init();


    public FragmentBase init(MainActivity mainActivity, ViewGroup container) {
        this._mainActivity = mainActivity;
        View view = LayoutInflater.from(_mainActivity).inflate(this.layoutId(), container, false);
        this.onViewCreated(view, null);
        container.addView(_view);
        this.self = this;
        return this;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(this.layoutId(), container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        this._view = view;
        _view.setBackgroundColor(Color.WHITE);
        _view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
        this.init();
    }
}