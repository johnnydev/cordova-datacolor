//
//  Polygons.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 06/10/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import Foundation
import UIKit

class Polygons
{
    
    // Singleton
    class var instance: Polygons {
        struct Singleton {
            static let instance = Polygons()
        }
        return Singleton.instance
    }
    
    func polygonPointArray(sides:Int,x:CGFloat,y:CGFloat,radius:CGFloat,offset:CGFloat)->[CGPoint] {
        let angle = (360/CGFloat(sides)).radians()
        let cx = x // x origin
        let cy = y // y origin
        let r = radius // radius of circle
        var i = 0
        var points = [CGPoint]()
        while i <= sides {
            let xpo = cx + r * cos(angle * CGFloat(i) - offset.radians())
            let ypo = cy + r * sin(angle * CGFloat(i) - offset.radians())
            points.append(CGPoint(x: xpo, y: ypo))
            i += 1
        }
        return points
    }
    
    func polygonPath(x:CGFloat, y:CGFloat, radius:CGFloat, sides:Int, offset: CGFloat) -> CGPath {
        let path = CGMutablePath()
        let points = polygonPointArray(sides: sides,x: x,y: y,radius: radius, offset: offset)
        let cpg = points[0]
        path.move(to: cpg)
        for p in points {
            path.addLine(to: p)
        }
        path.closeSubpath()
        return path
    }
    
    func drawPolygonBezier(x:CGFloat, y:CGFloat, radius:CGFloat, sides:Int, color:UIColor, offset:CGFloat) -> UIBezierPath {
        let path = polygonPath(x: x, y: y, radius: radius, sides: sides, offset: offset)
        let bez = UIBezierPath(cgPath: path)
        // no need to convert UIColor to CGColor when using UIBezierPath
        color.setFill()
        bez.fill()
        return bez
    }
    
    func drawPolygonUsingPath(ctx:CGContext, x:CGFloat, y:CGFloat, radius:CGFloat, sides:Int, color:UIColor, offset:CGFloat) {
        let path = polygonPath(x: x, y: y, radius: radius, sides: sides, offset: offset)
        ctx.addPath(path)
        let cgcolor = color.cgColor
        ctx.setFillColor(cgcolor)
        ctx.fillPath()
    }
    
    func drawPolygon(ctx:CGContext, x:CGFloat, y:CGFloat, radius:CGFloat, sides:Int, color:UIColor, offset:CGFloat) {
        
        let points = polygonPointArray(sides: sides,x: x,y: y,radius: radius, offset: offset)
        ctx.addLines(between: points)
        let cgcolor = color.cgColor
        ctx.setFillColor(cgcolor)
        ctx.fillPath()
    }
    func drawPolygonLayer(x:CGFloat, y:CGFloat, radius:CGFloat, sides:Int, color:UIColor, offset:CGFloat) -> CAShapeLayer {
        
        let shape = CAShapeLayer()
        shape.path = polygonPath(x: x, y: y, radius: radius, sides: sides, offset: offset)
        shape.fillColor = color.cgColor
        return shape
        
    }
    
    
}


extension CGFloat {
    func radians() -> CGFloat {
        let b = CGFloat(Double.pi) * (self/180)
        return b
    }
}
