//
//  TabsViewController.swift
//  Caran Demo App
//
//  Created by Gabriele Campi on 29/09/2016.
//  Copyright © 2016 Datacolor. All rights reserved.
//

import UIKit

class TabsViewController: UITabBarController {

    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: OVERRIDES
    // MARK:
    //--------------------------------------------------------------------------------
    
    // Remove the observers
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
               
        // set the tint for the tabbar
        self.tabBar.tintColor = DCUtils.MainColor
        
        
        // Receive uncalibrated event
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didReceiveUncalibratedNotification),
            name: NSNotification.Name(rawValue: "DeviceUncalibratedNotification"),
            object: nil)
        
        // Receive calibrated event
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didReceiveCalibratedNotification),
            name: NSNotification.Name(rawValue: "DeviceCalibratedNotification"),
            object: nil)
        
        checkCalibrationForNotifications()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: NAVIGATION
    // MARK:
    //--------------------------------------------------------------------------------
    override func viewWillAppear(_ animated: Bool) {
        
        // Navigation bar and tab bar settings
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        
        if(CaranController.instance.connectedDevice != nil){
            // hide back button if device is connected
            self.navigationItem.hidesBackButton = true
        }        
        
        //
        super.viewWillAppear(animated);
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
    
        //
        super.viewDidAppear(animated);
        
        

        
    }
    
    //--------------------------------------------------------------------------------
    // MARK:
    // MARK: METHODS
    // MARK:
    //--------------------------------------------------------------------------------
    
    
    func checkCalibrationForNotifications(){
        
        if(CaranController.instance.needsCalibration) {
            DCUtils.instance.sendLocalNotificationForNotCalibrated()
            self.tabBar.items?[2].badgeValue = "!"
        }
        else{
            // remove the badge from the app
            UIApplication.shared.applicationIconBadgeNumber = -1
        }
        
    }
    
   
    func didReceiveCalibratedNotification(notification: NSNotification) {
        
        print("TabsViewController was notified of the device calibrated")
        
        self.tabBar.items?[2].badgeValue = nil
        DCUtils.instance.sendLocalNotificationForCalibrated()
        
    }
    
    func didReceiveUncalibratedNotification(notification: NSNotification) {
        
        print("TabsViewController was notified of the device uncalibrated")
        
        checkCalibrationForNotifications()
        
    }
    
}
