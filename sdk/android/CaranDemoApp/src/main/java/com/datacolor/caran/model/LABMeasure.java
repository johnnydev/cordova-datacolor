//
//  LabMeasure.java
//  Caran Demo App
//
//  Copyright © 2017 Datacolor. All rights reserved.
//

package com.datacolor.caran.model;


import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.datacolor.DCICaranLib;

import java.nio.ByteBuffer;

import static android.support.v4.graphics.ColorUtils.LABToColor;

public class LABMeasure {
    double lab_l;
    double lab_a;
    double lab_b;

    int rgb_r;
    int rgb_g;
    int rgb_b;

    String hex;
    int color;

    DCICaranLib.FandeckMatch[] matches = null;


    private static DCICaranLib.FandeckMatch newFandeckMatchFromBuffer(ByteBuffer bb, int offset) {
        byte[] bytes = new byte[40];
        bb.position(offset);
        bb.get(bytes, 0, 40);
        return new DCICaranLib.FandeckMatch(bytes);
    }


    private void init(double l, double a, double b, DCICaranLib.FandeckMatch[] matches) {
        this.lab_l = l;
        this.lab_a = a;
        this.lab_b = b;

        if ((matches != null) && (matches.length == 3) && (matches[0] != null) && (matches[1] != null) && (matches[2] != null))
            this.matches = matches;

        color = LABToColor(l, a, b);
        rgb_r = (color >> 16) & 0xFF;
        rgb_g = (color >> 8) & 0xFF;
        rgb_b = (color >> 0) & 0xFF;

        hex = String.format("#%06X", (0xFFFFFF & color));
    }


    public LABMeasure(double l, double a, double b) {
        this.init(l, a, b, null);
    }

    public LABMeasure(double l, double a, double b, DCICaranLib.FandeckMatch[] matches) {
        this.init(l, a, b, matches);
    }

    public LABMeasure copy() {
        return new LABMeasure(this.toB64String());
    }

    public LABMeasure(String b64) {
        ByteBuffer bb = ByteBuffer.wrap(Base64.decode(b64, Base64.DEFAULT));
        int size = bb.limit();
        if (size == 24) {
            this.init(bb.getDouble(0), bb.getDouble(8), bb.getDouble(16), null);
        } else if (size == 144) {
            DCICaranLib.FandeckMatch m1 = LABMeasure.newFandeckMatchFromBuffer(bb, 24);
            DCICaranLib.FandeckMatch m2 = LABMeasure.newFandeckMatchFromBuffer(bb, 64);
            DCICaranLib.FandeckMatch m3 = LABMeasure.newFandeckMatchFromBuffer(bb, 104);
            this.init(bb.getDouble(0), bb.getDouble(8), bb.getDouble(16),
                    new DCICaranLib.FandeckMatch[]{ m1, m2, m3});
        } else {
            this.init(0.0, 0.0, 0.0, null);
        }
    }

    public double getLabL() {
        return lab_l;
    }
    public double getLabA() {
        return lab_a;
    }
    public double getLabB() {
        return lab_b;
    }
    public int getRgbR() {
        return rgb_r;
    }
    public int getRgbG() {
        return rgb_g;
    }
    public int getRgbB() {
        return rgb_b;
    }
    public String getHex() {
        return hex;
    }
    public int getColor() { return color; }
    public DCICaranLib.FandeckMatch[] getMatches() { return matches; }

    public String toB64String() {
        int size = (matches == null)? 24:144;
        ByteBuffer bb = ByteBuffer.allocate(size);
        bb.putDouble(lab_l);
        bb.putDouble(lab_a);
        bb.putDouble(lab_b);
        if (matches != null) {
            bb.put(matches[0].getBytes());
            bb.put(matches[1].getBytes());
            bb.put(matches[2].getBytes());
        }
        return Base64.encodeToString(bb.array(), Base64.DEFAULT);
    }
}